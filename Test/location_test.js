var request = require('superagent');
var expect = require('expect.js');
var app  = require('../server.js');
var port = 5000;

describe('Test the locationa API', function(){
	var location_id
	it('Add a location', function(done){
	request.post('http://localhost:5000/addlocations')
	  .send({ _id: 0, type: 'location',name: 'test', description: 'description', coord: [-31.936722,115.754979]
	  })
	  .end(function(e,res){

	    expect(e).to.eql(null)
	    expect(res.status).to.eql(200)
	    expect(res.body.id).not.to.equal(NaN)
	    expect(res.body.id).not.to.equal('undefined')
	    location_id = res.body.id
	    done()
	  })    
	})
	it('Get a location', function(done){
	request.get('http://localhost:5000/getlocation')
	  .query({ id: location_id})
	  .end(function(e,res){
	    expect(e).to.eql(null)
	    expect(res.body.type).to.eql('location')
	    expect(res.status).to.eql(200)
	    done()
	  })  
	})
	it('Get a location based on the GPS position', function(done){
	request.get('http://localhost:5000/getgeolocations')
	.query({ lat: -31.936722, lon:115.754979})
		.end(function(e,res){
	  	//console.log(res)
	    expect(e).to.eql(null)
	    //expect(res.body.type).to.eql('location')
	    expect(res.status).to.eql(200)
	    done()
	  })  
	})
	it('Get a the number of checkin at a spot', function(done){
	request.get('http://localhost:5000/getlocationcheckin')
	.query({ loc_id:"854e27ee72e074b2ea20983517dbd8fc"})
		.end(function(e,res){
	  	//console.log(res)
	    expect(e).to.eql(null)
	    //expect(res.body.type).to.eql('location')
	    expect(res.status).to.eql(200)
	    done()
	  })  
  })
});