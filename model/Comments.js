var mongoose = require('mongoose')
  , Schema = mongoose.Schema;
  
// Define MODEL COMMENTS ==============
var commentSchema = new mongoose.Schema({
  type: { type: String, default: "comment" },

  parentId: { type: Schema.Types.ObjectId, ref: 'Show' },
  text: String,
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  created: Number
});

commentSchema.pre('save', function(next) {
  next();
});

exports.Comment = mongoose.model('Comment', commentSchema);