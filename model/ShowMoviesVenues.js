var mongoose = require('mongoose')
  , Schema = mongoose.Schema;
  
//==================================================================================
// Define MODEL SHOW ==============
var showSchema = new mongoose.Schema({
  type: { type: String, default: "show" },
  idRef: { type: Number, unique: true },

  movieId: { type: Schema.Types.ObjectId, ref: 'Movie' },
  venueId: { type: Schema.Types.ObjectId, ref: 'Venue' },
  startDay: Date,
  endDay: Date,
  gateOpen: String,
  movieStart: String,

  language: String,
  subtitle: String
});

showSchema.pre('save', function(next) {
  next();
});

exports.Show = mongoose.model('Show', showSchema);

//==============================================================================
// Define MODEL VENUE ==============
var venueSchema = new mongoose.Schema({
  type: { type: String, default: "venue" },
  cinema: { type: String, default: "VenueDefault" },
  suburb: { type: String, default: "VenueDefault" },
  venueRef: { type: String, unique: true},
  tips: String,
  address: String,
  AddressTip: String,
  maps: String,
  urlVenue: String,
  urlAccess: String,
  description: String,
  latitude: Number,
  longitude: Number
});

venueSchema.pre('save', function(next) {

  next();
});

exports.Venue = mongoose.model('Venue', venueSchema);

//==============================================================================
// Define MODEL MOVIE ==============
var movieSchema = new mongoose.Schema({
  type: { type: String, default: "movie" },
  movieTitle: { type: String, default: "" },
  movieRef: { type: String, unique: true},
  parentalRating: String,
  altTitle: String,
  Title: String,
  Year: String,
  Rated: String ,
  Released: String,
  Runtime: String ,
  Genre: String ,
  Director: String,
  Writer: String,
  Actors: String,
  Plot: String,
  Language: String,
  Country: String,
  Awards: String,
  Poster: String,
  Metascore: String,
  imdbRating: String,
  imdbVotes: String,
  imdbID: String,
  Response: String,
  img: String,
  trailer: String
});

movieSchema.pre('save', function(next) {

  next();
});

movieSchema.set('toJSON', {
    transform: function(doc, movie, options) {
      //delete movie.Poster; //commented because Poster required to get image.
      //delete movie.imdbID;
      delete movie.Metascore;
      delete movie.imdbRating;
      delete movie.imdbVotes;
      delete movie.Type;
      delete movie.Response;
      
      return movie;
    }
});

exports.Movie = mongoose.model('Movie', movieSchema);
//////////////////////////////////////////////////////////////
// Collections are Cheap
// It turns out it isn’t hard to put the comments in their own collection. If you restructure your posts to look like this, everything falls into place:

// // db.posts
// { _id: "4f297e550b3e6d9e2b7aa58e"
// , title: "My First Blog Post"
// , content: "Here is my super long post ..." 
// }

// // db.comments
// { postId: "4f297e550b3e6d9e2b7aa58e"
// , text: "This post sucks!"
// , name: "seanhess"
// , created: 1328118162000 }
// Your client doesn’t have to be aware of the change, just add the comments with a second query. Now the comment-specific queries return only comments.

// function postWithAllComments(id) {
//     var post = db.posts.findOne({_id: id})
//     post.comments = db.comments.find({postId: id}).toArray()
//     return post
// }

// function addComment(postId, comment) {
//     comment.created = Date.now()
//     comment.postId = postId
//     db.comments.save(comment)
// }

// function commentsByUser(username) {
//     return db.comments.find({"name": username}).toArray()
// }

// function commentsBetweenTimes(start, end) {
//     return db.comments.find({"created": {$gte: start, $lt: end}).toArray()
// }