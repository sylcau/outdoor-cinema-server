var bcrypt = require('bcryptjs')
  , mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var reminderSchema = new mongoose.Schema({
  userId: String,
  show: { type: Schema.Types.ObjectId, ref: 'Show' },
  nbDays: Number,
  sentDate: Date
})
//exports.Reminder = mongoose.model('Reminder', reminderSchema);

// Define MODEL ==============
var userSchema = new mongoose.Schema({
  //email: { type: String, unique: true, lowercase: true },
  email: { type: String, unique: false, lowercase: true },
  //emailId: { type: String, unique: true, lowercase: true }, //use for Accoutn Identification

  password: { type: String, select: false },
  resetPasswordToken: String,
  resetPasswordExpires: Date,

  displayName: String,
  picture: String,
  
  facebook: String,
  foursquare: String,
  google: String,
  github: String,
  linkedin: String,
  live: String,
  yahoo: String,
  twitter: String,

  reminders: [reminderSchema] //arary of obj {show: xxxx, nbDays: x, sentDate: xx }  (number of days priro to event.) 
});

userSchema.pre('save', function(next) {
  var user = this;
  if (!user.isModified('password')) {
    return next();
  }
  bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash(user.password, salt, function(err, hash) {
      user.password = hash;
      next();
    });
  });
});

userSchema.methods.comparePassword = function(password, done) {
  bcrypt.compare(password, this.password, function(err, isMatch) {
    done(err, isMatch);
  });
};

//module.exports = mongoose.model('User', userSchema);
exports.User = mongoose.model('User', userSchema);
