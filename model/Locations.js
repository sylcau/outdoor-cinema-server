var Location = {
	fields: [
		{
			name: 'latitude',
			type: 'float'
		},
		{
			name: 'locationName',
			type: 'string'
		},
		{
			name: 'longitude',
			type: 'float'
		},
		{
			name: 'description',
			type: 'float'
		}
	]
};