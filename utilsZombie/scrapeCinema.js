#!/usr/bin/env node
//scrape Cinema website

var Browser = require("zombie");
var fs = require("fs");
var Promise = require('promise');
var async = require('async');
var request = require('request')
var nock = require('nock');
var mongoose = require('mongoose');

var path = require('path');

var check = require('./checksAndUploadJS/checkTools');

//var log = console.log.bind(console);
// var colors = require('colors');
// colors.setTheme({
//   silly: 'rainbow',
//   input: 'grey',
//   verbose: 'cyan',
//   prompt: 'grey',
//   info: 'green',
//   data: 'grey',
//   help: 'cyan',
//   warn: 'yellow',
//   debug: 'blue',
//   error: 'red'
// });

var log = {};
[
 [ 'warn',  '\x1b[33m' ], // Yellow
 [ 'info',  '\x1b[32m' ], // green
 [ 'error', '\x1b[31m' ], // red
 [ 'log',   '\x1b[0m'  ] //
].forEach(function(pair) {
  var method = pair[0], reset = '\x1b[0m', color = '\x1b[36m' + pair[1];
  log[method] = console[method].bind(console, color, method.toUpperCase(), reset);
});

// getProgram is a Promise, readHtml is a sync function, cinema is a string

(function(){
   var scraper = {};

   exports.startProcess = scraper.startProcess = function(getProgram, readHtml, cinema, browsers){
   	console.log('')
   	log.info('scrapeCinema.js: ' + cinema)


   	var appendFile = Promise.denodeify(fs.appendFile)
		//var write = Promise.denodeify(fs.writeFile)
		var logSummary =""

    	if (!browsers || !browsers.browser) { 
    		console.log("no browser -- creating browser")  ;      
    		var browser = new Browser({waitDuration: 10*1000}); 
    	}
    	else { 
    		var browser = browsers.browser
    	}
    	console.log('!!!!!! START PROCESS ' + cinema + '!!!')
    	var d = new Date()
		var today = d.getFullYear()+"-"+ (d.getMonth()+1) + "-"+  d.getDate();

		var sep = "";
		//var filePath = './scrapedWeb-JSON/Moonlight_' + today +'.json'
		var filePath = path.join(__dirname, 'scrapeJSON', 'scrape' + cinema + '.json' );
		var write = Promise.denodeify(fs.writeFile)	

		var promiseResult =  write(filePath, "[", 'utf8')
		.then(function(){ 
			console.log("Download Program for: " + cinema);
			return browser
		})
		.then(getProgram)
		.then(function(urls){
			console.log('nb of urls found: ' + urls.length)
			var obj = {"urls":urls, "i": 0}
			var sep = "";
			
			var prom = Promise.resolve(obj)
			.catch(function(e){
				console.log(e)
			})


			function extractData(toto){
				console.log("=== EXTRACT_DATA =================")		
				console.log("-->" + toto.i +" / " + toto.urls.length)

				var url = toto.urls[toto.i].url
				console.log("---> " + url)

				var show = toto.urls[toto.i]

				var p = new Promise(function(resolve,reject){
					browser.visit(url, function (e){
						if (e){
							console.error("loading err: " + e)
						}
						console.log("---->url Loaded " + url)
					    // Wait until page is loaded
					    function pageLoaded(window) {
					        return window.document.querySelector('h1 a');
					    }
					    function pageLoaded2(window) {
					        return window.document.querySelector();
					    }
						//browser.wait(pageLoaded, function(){
						browser.wait(5000, function(){

							var delay = 0 //number of seconds
							console.log("loaded1 - waiting " + delay +"s")

							setTimeout(function(){

								//browser.wait(pageLoaded2, function(){
								//console.log(browser.html())

								show = readHtml(show, browser) //return an oject or a string show
								

								var showString = "";
								if (typeof show !== "string"){ 
									var showString = JSON.stringify(show, null, 4) 
								} else { 
									showString = show 
								}
								
								fs.appendFile(filePath, sep + showString, function(err){
                                 //console.log('File successfully written! - Check your project directory for the log.json file');
                            		if (!sep) sep = ",\n";     
							      	toto.i = toto.i + 1
									resolve(toto)                    
								});

							}, delay*1000)


							
							//})
						})
					})
				});

				return p
				//return toto
			}

					
			for (var i = 0; i < urls.length; i++) {
			//for (var i = 0; i < 1; i++) {
				//console.log(i)
				prom = prom.then(extractData)
			}
			return prom
		})
		.then(function (res){
			if (res.urls[0]) console.log("example of url: " + JSON.stringify(res.urls[0]))
			
			logSummary = logSummary + " number of urls scraped: " + res.urls.length 
		   
		   return appendFile(filePath, "]")	
		})
		.then(function(res){
			console.log('scrapeCinema.js: scrape completed')
			return {cinema: cinema, jsonFile: 'scrape' + cinema, log: logSummary}
		})
		.catch(function(e){
			log.error(e)
		});

		return promiseResult

   };
		
   if (!module.parent) {
     	///scraper.startProcess(process.argv[2]);
     	var bro = new Browser({waitDuration: 10*1000});

      // read arguments.
      if (process.argv.length < 3 ) {
        	console.log('missing last arguments. require cinema Name for functions cinema specific. Choices are:')
        	var files = fs.readdirSync('./cinemaJS')
        	files.forEach(function(val, index){
        		console.log(index + " " + val.slice(0,-3))
        	})
        	return
      } 
        
      if (process.argv.length == 3 ) {
        	var cinema = process.argv[2]
        	var dir = './cinemaJS/' + cinema + '.js'
        	console.log('loading ' + dir)  
        	var cinemaFunctions = require(dir)

	      scraper.startProcess(cinemaFunctions.getProgram, cinemaFunctions.readHtml, cinemaFunctions.cinema, bro)
         .then(function(){
        		console.log("end of process")
        	 	process.exit()
         })
         .catch(function(err){
        		console.log(err)
         })
	   }
   }
})();



