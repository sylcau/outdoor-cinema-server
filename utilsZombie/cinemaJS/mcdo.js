#!/usr/bin/env node
var Promise = require('promise');
var request = require('request');
var fs = require("fs");

var check = require('../checksAndUploadJS/checkTools');


(function(){
    var cinemaFn = {};

    exports.cinema = cinemaFn.cinema = "McDo"

    exports.getProgram = cinemaFn.getProgram = function(browser){
    	var p = new Promise(function (resolve, reject) {
			console.log("getProgram");

				function getUrls(url, callback){
				
					browser.visit(url, function (e){
						console.log("visit: "+ url)
						
						var BURSWOOD = [];
					    var POPUP = [];
					    var MURDOCH = [];
					    var BASSENDEAN = [];
					    var MANDURAH = [];


						//BURSWOOD
						BURSWOOD = browser.queryAll('div .table-responsive:nth-child(4) tbody tr').map(function(elem){
							var year = ""
							var url = elem.querySelector("td:nth-child(3) a").getAttribute('href').slice(9) //remove the ../Movie/ from the url
							
							var date = elem.querySelector("td:nth-child(2)").textContent.trim()
							if (date.length != 0){
								var year = check.checkYear(date)
								date = date + year
							}


							var movTitle = elem.querySelector("td:nth-child(3) a").textContent

							return { 	
										url : "https://www.communitycinemas.com.au/Movie/" + url, 
										venueId : "554ed6bcd617526c16816ceb",
										movtitle: movTitle,
										date : date
									}
						})


						//POPUP
						POPUP = browser.queryAll('div .table-responsive:nth-child(6) tbody tr').map(function(elem){
							var year = ""
							var url = elem.querySelector("td:nth-child(3) a").getAttribute('href').slice(9) //remove the ../Movie/ from the url
							
							var date = elem.querySelector("td:nth-child(2)").textContent.trim()
							if (date.length != 0){
								var year = check.checkYear(date)
								date = date + year
							}


							var movTitle = elem.querySelector("td:nth-child(3) a").textContent

							return { 	
										url : "https://www.communitycinemas.com.au/Movie/" + url, 
										venueId : "popvenue",
										movtitle: movTitle,
										date : date
									}
						})

        				//MURDOCH
						MURDOCH = browser.queryAll('div .table-responsive:nth-child(8) tbody tr').map(function(elem){
							var year = ""
							var url = elem.querySelector("td:nth-child(3) a").getAttribute('href').slice(9) //remove the ../Movie/ from the url
							
							var date = elem.querySelector("td:nth-child(2)").textContent.trim()
							if (date.length != 0){
								var year = check.checkYear(date)
								date = date + year
							}


							var movTitle = elem.querySelector("td:nth-child(3) a").textContent

							return { 	
										url : "https://www.communitycinemas.com.au/Movie/" + url, 
										venueId : "554ed6bdd617526c16816cee",
										movtitle: movTitle,
										date : date
									}
						})

       					//BASSENDEAN
						BASSENDEAN = browser.queryAll('div .table-responsive:nth-child(10) tbody tr').map(function(elem){
							var year = ""
							var url = elem.querySelector("td:nth-child(3) a").getAttribute('href').slice(9) //remove the ../Movie/ from the url
							
							var date = elem.querySelector("td:nth-child(2)").textContent.trim()
							if (date.length != 0){
								var year = check.checkYear(date)
								date = date + year
							}


							var movTitle = elem.querySelector("td:nth-child(3) a").textContent

							return { 	
										url : "https://www.communitycinemas.com.au/Movie/" + url, 
										venueId : "554ed6bcd617526c16816cea",
										movtitle: movTitle,
										date : date
									}
						})

       					//MANDURAH
						MANDURAH = browser.queryAll('div .table-responsive:nth-child(12) tbody tr').map(function(elem){
							var year = ""
							var url = elem.querySelector("td:nth-child(3) a").getAttribute('href').slice(9) //remove the ../Movie/ from the url
							
							var date = elem.querySelector("td:nth-child(2)").textContent.trim()
							if (date.length != 0){
								var year = check.checkYear(date)
								date = date + year
							}


							var movTitle = elem.querySelector("td:nth-child(3) a").textContent

							return { 	
										url : "https://www.communitycinemas.com.au/Movie/" + url, 
										venueId : "554ed6bdd617526c16816ced",
										movtitle: movTitle,
										date : date
									}
						})


						var elems = BURSWOOD.concat(POPUP, MURDOCH, BASSENDEAN, MANDURAH)

						console.log("Done Programme: "+ elems.length + " found")
						console.log("Done BURSWOOD: "+ BURSWOOD.length + " found")
						console.log("Done POPUP: "+ POPUP.length + " found")
						console.log("Done MURDOCH: "+ MURDOCH.length + " found")
						console.log("Done BASSENDEAN: "+ BASSENDEAN.length + " found")
						console.log("Done MANDURAH: "+ MANDURAH.length + " found")

						callback(elems);
					}); 
				};
				
				// HANDLE ERROR ??? TODO

				getUrls("http://www.communitycinemas.com.au/Page/Programme", function(data){
					var result = data

					// var datas = {"data": result};
					// var jsonStr = JSON.stringify(datas, null," ");
					// console.log(jsonStr)
					//fs.writeFile("testZombie.json", jsonStr)
					//if (err) reject(err);
					//else 
					resolve(result); 
				});
		});
		return p;
    };

    exports.readHtml = cinemaFn.readHtml = function(show, browser){

	    //Director
	    show.movieDirector = check.getText('div > dl > dd:nth-child(6)', 'director', browser)
        if (show.movieDirector === null) delete show.movieDirector; 


        var date = new Date(show.date)  
        show.movieStart = date.toLocaleTimeString('en-US', {hour: '2-digit', minute: '2-digit', hour12: true})

        show.gateOpen = "6:30 PM"
        
        show.language = ""
        show.subtitle = ""

        show.startDay = date;
        show.endDay = date;

        //console.log(JSON.stringify(h))
        show.parentalRating = check.checkRating(show.movtitle)

        var regExp = /([^(]+) \(/;
        show.movieTitle = regExp.exec(show.movtitle)[1] //find "(" and select what is before


    	return show
    };

})();