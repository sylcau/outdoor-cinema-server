#!/usr/bin/env node
var Promise = require('promise');
var request = require('request');
var fs = require("fs");

var check = require('../checksAndUploadJS/checkTools');


(function(){
    var camelot = {};

    exports.cinema = camelot.cinema = "Camelot"

    exports.getProgram = camelot.getProgram = function(dt){
    	var browser = dt
    	var p = new Promise(function (resolve, reject) {
			console.log("getProgram");

			function getUrls(url, callback){
				browser.visit(url, function (e){
					console.log("visit: "+ url)
					
					var elems = browser.queryAll('.programme-tile').map(function(elem){
						var url = elem.querySelector("a").getAttribute('href')
                        var date = elem.querySelector(".datetime .date").textContent.trim()
                        var year = check.checkYear(date)
                        date = date + year


                        var time = elem.querySelector(".datetime .time").textContent.trim()
                        var title = elem.querySelector("div .title a").textContent.trim()
                        var parentalrating = check.checkRating(title)

                        var regExp = /([^(]+)\(/;
                        var movieTitle = regExp.exec(title)[1].trim() //find "(" and select what is before

						return {
							url: "http://camelot.lunapalace.com.au/" + url,
                            venueId: "554ed6bbd617526c16816ce7",
                            date: date,
                            startDay: new Date(date),
                            endDay: new Date(date),
                            movieStart: time,
                            movTitle: title,
                            movieTitle: movieTitle,
                            parentalRating: parentalrating
						}
					})
					
					callback(elems);
				}); 
			};
			
			getUrls("http://camelot.lunapalace.com.au/", function(data){
				var datas = {"data": data};
				//var jsonStr = JSON.stringify(datas.data[0],null," ");
				//console.log(jsonStr)
				//fs.writeFile("testZOmbie.json", jsonStr)
				//if (err) reject(err);
				//else 
				resolve(data); 
			});
		});
		return p;
    };

    exports.readHtml = camelot.readHtml = function(show, browser){

        // DIRECTOR
        // var dir = check.getHtml('[id*="event"] > p', "Director", browser)
        //     var regExp = /Director: <\/b>(.*?)(?=<br>)/
        //     var re = regExp.exec(JSON.stringify(dir))
        //     if (re) {
        //         console.log("=> movieDirector: " + re[1].trim())
        //         show.movieDirector = re[1].trim()
        //     } else {
        //         console.log("=> movieDirector: undefined!")
        //     }

        //GateOpen
        var go = check.getText('[id*="doors_open"] div', "GateOpen", browser)
        	if (!go) {go = "6:00 PM" ; console.log("GateOpen is Manual!!")}
            show.gateOpen = go.trim()
               
        show.language = ""
        show.subtitle = ""

        //description
        //body > div.luna_body > div.content-body > div > div.content-left > table > tbody > tr > td > table > tbody > tr:nth-child(3) > td
        var description = check.getTexts('tr td[colspan="2"] p', "Description", browser)
        show.description = description

    	return show
    };

})();