#!/usr/bin/env node
var Promise = require('promise');
var request = require('request');
var fs = require("fs");

var check = require('../checksAndUploadJS/checkTools');


(function(){
    var cinemaFn = {};

    exports.cinema = cinemaFn.cinema = "Moonlight"

    exports.getProgram = cinemaFn.getProgram = function(browser){
    	var p = new Promise(function (resolve, reject) {
			console.log("getProgram");

				function getUrls(url, callback){
				
					browser.visit(url, function (e){
						console.log("visit: "+ url)
						
						var elems = browser.queryAll('div.span7.desc > ul > li > a').map(function(elem){
							var url = elem.getAttribute('href') 
							return {
								 "url": "" + url
							}
						})
						console.log("Done visit: "+ elems.length + " found")
						callback(elems);
					}); 
				};
				
				// HANDLE ERROR ??? TODO
				getUrls("https://www.moonlight.com.au/perth/program/", function(data){
					var result = data
					getUrls("https://www.moonlight.com.au/perth/program/page/2", function(data){
						result = result.concat(data)
						getUrls("https://www.moonlight.com.au/perth/program/page/3", function(data){
							result = result.concat(data)

							// var datas = {"data": result};
							// var jsonStr = JSON.stringify(datas,null," ");
							// console.log(jsonStr)
							//fs.writeFile("testZombie.json", jsonStr)
							//if (err) reject(err);
							//else 
							resolve(result); 
						});
					});
				});
		});
		return p;
    };

    exports.readHtml = cinemaFn.readHtml = function(show, browser){

		//Title
	    show.movieTitle = check.getText('#content > div.row-fluid > div.span10 > div.row-fluid > div.span7.content-area.movie-detail > h1' , "title", browser )

	    //Date
	    var dt = check.getText('#content div.span7.content-area.movie-detail div div dl dd', 'date', browser)
	        console.log("=>" + " WARNING YEAR IS MANUAL")
	        show.Date = dt
	        var year = check.checkYear(show.Date)
	        show.startDay = new Date(dt + year);
	        show.endDay = show.startDay;

	    //Rating
	    show.parentalRating = check.getText('#content dd .rating', 'rating', browser) 

	    //Director
	    show.movieDirector = check.getText('div.span7.content-area.movie-detail > div > div > dl > dd:nth-child(7)', 'director', browser)
        if (show.movieDirector === null) delete show.movieDirector; 


        if (new Date(show.startDay) > new Date("1 March 2016")){
            show.movieStart = "7:00 PM"
        } else {
            show.movieStart = "8:00 PM"
        }

        show.venueId = "554ed6bed617526c16816cef"

        show.gateOpen = "6:00 PM"
        
        show.language = ""
        show.subtitle = ""


    	return show
    };

})();