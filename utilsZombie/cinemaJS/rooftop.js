#!/usr/bin/env node
var Promise = require('promise');
var request = require('request');
var fs = require("fs");

var check = require('../checksAndUploadJS/checkTools');


(function(){
    var rooftopFn = {};

    exports.cinema = rooftopFn.cinema = "Rooftop"

    exports.getProgram = rooftopFn.getProgram = function(dt){
    	var browser = dt
    	var p = new Promise(function (resolve, reject) {
			console.log("getProgram");

			function getUrls(url, callback){
				browser.visit(url, function (e){
					console.log("visit: "+ url)
					
					var elems = browser.queryAll('.event_image').map(function(elem){
						var url = elem.getAttribute('href') 
						return {
							 "url": url.slice(0,84)
						}
						//url.slice(0,84)
					})
					
					callback(elems);
				}); 
			};
			
			getUrls("https://www.rooftopmovies.com.au/program/", function(data){
				// var datas = {"data": data};
				// var jsonStr = JSON.stringify(datas.data[0],null," ");
				// console.log(jsonStr)
				//fs.writeFile("testZOmbie.json", jsonStr)
				//if (err) reject(err);
				//else 
				resolve(data); 
			});
		});
		return p;
    };

    exports.readHtml = rooftopFn.readHtml = function(show, browser){
		//extract title:
		var txt = check.getText('h1 a', "title", browser )
			if (txt.search(/\(Encore Screening\)/i) > -1 ) {
	            txt = txt.slice(0,-18)
	            console.log("==> Title: " + txt)
	        } else if (txt.search(/\(Encore Screening [1-9]\)/i) > -1){
	            txt = txt.slice(0,-20)
	            console.log("==> Title: " + txt)
	        }
	        //toto.urls[toto.i].title = txt
        show.movieTitle = txt.trim()


        // Extract //Date
        var dt = check.getText('[id*="dates"]', "date", browser)
	        //console.log("=>" + text + " WARNING YEAR IS MANUAL")
            show.Date = JSON.stringify(dt)
            var year = check.checkYear(dt);
            console.log(year)
            show.startDay = new Date(dt + year);
            show.endDay = show.startDay;
           	console.log("=> " + show.startDay)


        //Rating
        var rt = check.getText('[id*="event"] > div > div.info.clearfix > div:nth-child(11)', "rating", browser)
        show.parentalRating = check.checkRating(rt); 


        // DIRECTOR
        var dir = check.getHtml('[id*="event"] > p', "Director", browser)
            var regExp = /Director: <\/b>(.*?)(?=<br>)/
            var re = regExp.exec(JSON.stringify(dir))
            if (re) {
                console.log("=> movieDirector: " + re[1].trim())
                show.movieDirector = re[1].trim()
            } else {
                console.log("=> movieDirector: undefined!")
            }

        //GateOpen
        var go = check.getText('[id*="doors_open"] div', "GateOpen", browser)
        	if (!go) {go = "6:00 PM" ; console.log("GateOpen is Manual!!")}
            show.gateOpen = go.trim()
        
        //SartTime
        var st = check.getText('[id*="time"]', "SartTime", browser)
            show.movieStart = st.trim()

        show.venueId = "554ed6bed617526c16816cf1"
        
        show.language = ""
        show.subtitle = ""
    	return show
    };

})();