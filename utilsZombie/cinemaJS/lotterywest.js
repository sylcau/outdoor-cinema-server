#!/usr/bin/env node
var Promise = require('promise');
var request = require('request');
var fs = require("fs");

var check = require('../checksAndUploadJS/checkTools');


(function(){
    var cinemaFn = {};

    exports.cinema = cinemaFn.cinema = "Lotterywest"

    exports.getProgram = cinemaFn.getProgram = function(browser){
    	var p = new Promise(function (resolve, reject) {
			console.log("getProgram");

				function getUrls(url, callback){
				
					browser.visit(url, function (e){
						console.log("visit: "+ url)
						
						var elems = browser.queryAll('#results a').map(function(elem){
							var url = elem.getAttribute('href') 
							return {
								 "url": "https://perthfestival.com.au" + url
							}
						})
						console.log("visit page1: "+ elems.length + " found")

						//click page 2
						var buttonPage2 = browser.link('2')
						browser.fire(buttonPage2, "click",  function(){
							var elems2 = browser.queryAll('#results a').map(function(elem){
								var url = elem.getAttribute('href') 
								return {
									 "url": "https://perthfestival.com.au" + url
								}
							})
							console.log("visit page2: "+ elems2.length + " found")

							elems = elems.concat(elems2)
							console.log("Done visit: "+ elems.length + " found")
							callback(elems);
						})
						
					}); 
				};
				
				// HANDLE ERROR ??? TODO
				getUrls("https://perthfestival.com.au/lotterywest-festival-films/whats-on/#/", function(data){
					var result = data

					//var datas = {"data": result};
					//var jsonStr = JSON.stringify(datas,null," ");
					//console.log(jsonStr)
					//fs.writeFile("testZombie.json", jsonStr)
					//if (err) reject(err);
					//else 
					resolve(result); 
				});
		});
		return p;
    };

    exports.readHtml = cinemaFn.readHtml = function(show, browser){

		//extract title:
		var txt = check.getText('.hero-card__title', "title", browser)
        show.movieTitle = txt


		// Extract Date
		var text = check.getText('.sticky-footer__event span:nth-of-type(1)', "date", browser)
		if( text && text.match("UWA Somerville")) {
			show.somerDates = text
		} else {
			show.joonDates = text
		}

		var text = check.getText('.sticky-footer__event span:nth-of-type(2)', "date", browser)
		if( text && text.match("UWA Somerville")) {
			show.somerDates = text
		} 
		else if (text && text.match("ECU Joondalup Pines")) {
			show.joonDates = text
		}
								
		// link
								
								
								
								
		// Rating
		var rt = check.getTextParent('.hero-card__notes text', "rating", browser)
		var regExp = /[^,]*/;
		show.parentalRating = regExp.exec(rt)[0]  


		// DIRECTOR html/body/div[2]/div[1]/div[2]/div[2]/p[5] /html/body/div[2]/div[1]/div[2]/div[2]/p[6]
		var text = ""; 
		var textTmp = "";
		for (var i = 1; i < 9; i++ ){
			var selector = 'body > div.teal > div.header-banner > div.hero-card.hero-card--event.teal.is-ready > div.hero-card__details > p:nth-child(' +i +')'
			textTmp = check.getText(selector, "director", browser)
			if (typeof textTmp == "string" && textTmp.search(/Director/i) > -1 ) { 
				text = textTmp
			}
		}

		//var regExp = /[^,]*/;
		console.log("=>" + text)
		show.movieDirector = text.slice(9) 

		// PROCESS
		var somerTemp = null;
		var joonTemp = null;
		if (typeof show.somerDates != "undefined"){
			var somerTemp = {};
	        somerTemp.somerDates = show.somerDates
	        somerTemp.movieTitle = show.movieTitle 
	        somerTemp.venueId = "554ed6bed617526c16816cf2" //"SOMERVILLE"
	        somerTemp.dates = show.somerDates.slice(15)

	        var reg = /[^–]+$/
	        somerTemp.startDay = new Date( reg.exec(somerTemp.dates)[0] )
	        somerTemp.endDay = new Date(reg.exec(somerTemp.dates)[0])
	        somerTemp.startDay.setDate(somerTemp.endDay.getDate()-6);
	        somerTemp.parentalRating = show.parentalRating
	        somerTemp.movieDirector = show.movieDirector

	        if (new Date(show.startDay) > new Date("6 March 2016")){
	            somerTemp.movieStart = "7:30 PM"
	        } else {
	            somerTemp.movieStart = "8:00 PM"
	        }

	        somerTemp.gateOpen = "6:00 PM"
	        
	        somerTemp.language = ""
	        somerTemp.subtitle = ""
	        //delete(show.somerDates)


	        //Door sales from 6pm nightly.
	        //Picnickers are welcome from 6pm but must be seated by 8pm (7.30pm from 7 March 2016)

	    }
        if (typeof show.joonDates != "undefined"){
            var joonTemp = {};
            joonTemp.somerDates = show.joonDates
            joonTemp.movieTitle = show.movieTitle 
            joonTemp.venueId = "554ed6bbd617526c16816ce8" //"JOONDALUP"
            joonTemp.dates = show.joonDates.slice(20)
            
            var reg = /[^–]+$/
            joonTemp.startDay = new Date( reg.exec(joonTemp.dates)[0] )
            joonTemp.endDay = new Date(reg.exec(joonTemp.dates)[0])
            joonTemp.startDay.setDate(joonTemp.endDay.getDate()-5);
            joonTemp.parentalRating = show.parentalRating
            joonTemp.movieDirector = show.movieDirector

            if (new Date(show.startDay) > new Date("6 March 2016")){
                joonTemp.movieStart = "7:30 PM"
            } else {
                joonTemp.movieStart = "8:00 PM"
            }

            joonTemp.gateOpen = "6:30 PM"
            
            joonTemp.language = ""
            joonTemp.subtitle = ""                            
        }
		var string = ""
		if (somerTemp) {
			string  = JSON.stringify(somerTemp, null, 4)
			if (joonTemp){ string = string + "," + JSON.stringify(joonTemp, null, 4) }
		} else if (joonTemp){
			string  = JSON.stringify(joonTemp, null, 4)
		}else {
		 // nothing
		}


    	return string
    };

})();