#!/usr/bin/env node
//scrape Luna website

var Browser = require("zombie");
var fs = require("fs");
var Promise = require('promise');
var async = require('async');
var request = require('request')
var nock = require('nock');

var check = require('./checkTools');

(function(){
    var scraper = {};

    exports.startProcess = scraper.startProcess = function(browsers){
    	if (!browsers.browser) { 
    		console.log("no browser")  ;      
    		var browser = new Browser({waitDuration: 10*1000}); 
    	}
    	else { var browser = browsers.browser}
    	console.log('!!!!!! START PROCESS Lotto!!!')
    	var d = new Date()
		var today = d.getFullYear()+"-"+ (d.getMonth()+1) + "-"+  d.getDate();

		var sep = "";
		var filePath = './scrapedWeb-JSON/lotterywest_' + today +'.json'
		fs.writeFile(filePath, "[", function(err){
		    //console.log('File successfully written! - Check your project directory for the log.json file');
		});


		function Program (resolve, reject) {
			console.log("Program");

			function getUrls(url, callback){
				browser.visit(url, function (e){
					console.log("visit: "+ url)
					
					var elems = browser.queryAll('#results a').map(function(elem){
						var url = elem.getAttribute('href') 
						return {
							 "url": "https://perthfestival.com.au" + url
						}
					})
					
					callback(elems);
				}); 
			};
			
			getUrls("https://perthfestival.com.au/lotterywest-festival-films/whats-on/#/", function(data){
				var datas = {"data": data};
				var jsonStr = JSON.stringify(datas,null," ");
				console.log(jsonStr)
				fs.writeFile("testZOmbie.json", jsonStr)
				//if (err) reject(err);
				//else 
				resolve(data); 
			});
		}


		var scrape = new Promise(Program)
		.then(function(urls){
			console.log('=>done ' + urls.length)
			//urls.i = 0
			var obj = {"urls":urls, "i": 0}
			var sep = "";
			
			var prom = Promise.resolve(obj)
			.catch(function(e){
				console.error(e)
			})


			function extractData(toto){
				console.log("=== EXTRACT_DATA =================")		
				console.log("-->" + toto.i +" / " + toto.urls.length)

				var url = toto.urls[toto.i].url
				console.log("---> " + url)

				var show = toto.urls[toto.i]

				var p = new Promise(function(resolve,reject){
					browser.visit(url, function (e){
						if (e){
							console.error("loading err: " + e)
						}
						console.log("---->url Loaded " + url)
					    // Wait until page is loaded
					    function pageLoaded(window) {
					        return window.document.querySelector('h1 a');
					    }
					    function pageLoaded2(window) {
					        return window.document.querySelector();
					    }
						//browser.wait(pageLoaded, function(){
						browser.wait(5000, function(){

							var delay = 0 //number of seconds
							console.log("loaded1 - waiting " + delay +"s")

							setTimeout(function(){

								//browser.wait(pageLoaded2, function(){
								//console.log(browser.html())
								

								function getText(selector, label){
									var nodes = browser.queryAll(selector)
									console.log(label + " nodes: " + nodes.length)
									var text = browser.text(nodes[0])
									console.log("=> " + label +": " + text)

									return text
								}
								function getTextParent(selector, label){
									var nodes = browser.queryAll(selector)
									console.log(label + " nodes: " + nodes.length)
									var text = browser.text(nodes[0].parentNode)
									console.log("=> " + label +": " + text)

									return text
								}
								function getHtml(selector, label){
									var nodes = browser.queryAll(selector)
									console.log(label + " nodes: " + nodes.length)
									var html = browser.html(nodes[0])
									console.log("=> " + label +": " + html)

									return html
								}

								//extract title:
								var txt = getText('.hero-card__title', "title")
						        show.title = txt


						      // Extract Date
						      var text = getText('.sticky-footer__event span:nth-of-type(1)', "date")
								if( text.match("UWA Somerville")) {show.somerDates = text} else {show.joonDates = text}

								var text = getText('.sticky-footer__event span:nth-of-type(2)', "date")
								if( text.match("UWA Somerville")) {show.somerDates = text} 
                        else if (text.match("ECU Joondalup Pines")) {show.joonDates = text}
								
								// link
								
								
								
								
			               // Rating
								var rt = getTextParent('.hero-card__notes text', "rating")
								var regExp = /[^,]*/;
								show.parentalRating = regExp.exec(rt)[0]  


			               // DIRECTOR html/body/div[2]/div[1]/div[2]/div[2]/p[5] /html/body/div[2]/div[1]/div[2]/div[2]/p[6]
								var text = getText('html/body/div[2]/div[1]/div[2]/div[2]/p[5] /html/body/div[2]/div[1]/div[2]/div[2]/p[6]', "rating")
								//var regExp = /[^,]*/;
								console.log("=>" + text)
                        show.movieDirector = text.slice(9) 

								// PROCESS
								var somerTemp = null;
								var joonTemp = null;
                        if (typeof show.somerDates != "undefined"){
									var somerTemp = {};
                            somerTmp.somerDates = show.somerDates
                            somerTemp.movieTitle = show.movieTitle 
                            somerTemp.venueId = "554ed6bed617526c16816cf2" //"SOMERVILLE"
                            somerTemp.dates = show.somerDates.slice(15)

                            var reg = /[^–]+$/
                            somerTemp.startDay = new Date( reg.exec(somerTemp.dates)[0] )
                            somerTemp.endDay = new Date(reg.exec(somerTemp.dates)[0])
                            somerTemp.startDay.setDate(somerTemp.endDay.getDate()-6);
                            somerTemp.parentalRating = show.parentalRating
                            somerTemp.movieDirector = show.movieDirector

                            if (new Date(show.startDay) > new Date("6 March 2016")){
                                somerTemp.movieStart = "7:30 PM"
                            } else {
                                somerTemp.movieStart = "8:00 PM"
                            }

                            somerTemp.gateOpen = "6:00 PM"
                            
                            somerTemp.language = ""
                            somerTemp.subtitle = ""
                            //delete(show.somerDates)


                            //Door sales from 6pm nightly.
                            //Picnickers are welcome from 6pm but must be seated by 8pm (7.30pm from 7 March 2016)

                        }
                        if (typeof show.joonDates != "undefined"){
                            var joonTemp = {};
                            joonTemp.somerDates = show.joonDates
                            joonTemp.movieTitle = show.movieTitle 
                            joonTemp.venueId = "554ed6bbd617526c16816ce8" //"JOONDALUP"
                            joonTemp.dates = show.joonDates.slice(20)
                            
                            var reg = /[^–]+$/
                            joonTemp.startDay = new Date( reg.exec(joonTemp.dates)[0] )
                            joonTemp.endDay = new Date(reg.exec(joonTemp.dates)[0])
                            joonTemp.startDay.setDate(joonTemp.endDay.getDate()-5);
                            joonTemp.parentalRating = show.parentalRating
                            joonTemp.movieDirector = show.movieDirector

                            if (new Date(show.startDay) > new Date("6 March 2016")){
                                joonTemp.movieStart = "7:30 PM"
                            } else {
                                joonTemp.movieStart = "8:00 PM"
                            }

                            joonTemp.gateOpen = "6:30 PM"
                            
                            joonTemp.language = ""
                            joonTemp.subtitle = ""                            
                        }
								var string = ""
								if (somerTemp) {
									string  = JSON.stringify(somerTemp, null, 4)
									if (joonTemp){ string = string + "," + JSON.stringify(joonTemp, null, 4) }
								} else if (joonTemp){
									string  = JSON.stringify(joonTemp, null, 4)
								}else {
								 // nothing
								}
								
								fs.appendFile(filePath, sep + string, function(err){
                                 //console.log('File successfully written! - Check your project directory for the log.json file');
                            if (!sep) sep = ",\n";     
							      toto.i = toto.i + 1
									resolve(toto)                    
								});

							}, delay*1000)


							
							//})
						})
					})
				});

				return p
				//return toto
			}

					
			for (var i = 0; i < urls.length; i++) {
			//for (var i = 0; i < 1; i++) {
				//console.log(i)
				prom = prom.then(extractData)
			}
			return prom
		})
		.then(function (res){
		    fs.appendFile(filePath, "]", function(err){
				console.log('done ')
				console.log(res.urls.length)
				console.log(JSON.stringify(res.urls[0]))    
			});
		})
		.catch(function(e){
				console.error(e)
		});
		
		return scrape
	};
		
    if (!module.parent) {
        ///scraper.startProcess(process.argv[2]);
        var bro = new Browser({waitDuration: 10*1000});
        scraper.startProcess(bro)
    }
})();