#!/usr/bin/env node
//scrape Luna website

var Browser = require("zombie");
var fs = require("fs");
var Promise = require('promise');
var async = require('async');
var request = require('request')
var nock = require('nock');

var check = require('./checkTools');

(function(){
    var scraper = {};

    exports.startProcess = scraper.startProcess = function(browsers){
    	if (!browsers.browser) { 
    		console.log("no browser")  ;      
    		var browser = new Browser({waitDuration: 10*1000}); 
    	}
    	else { var browser = browsers.browser}
    	console.log('!!!!!! START PROCESS Moonlight!!!')
    	var d = new Date()
		var today = d.getFullYear()+"-"+ (d.getMonth()+1) + "-"+  d.getDate();

		var sep = "";
		//var filePath = './scrapedWeb-JSON/Moonlight_' + today +'.json'
		var filePath = '../scrapeJSON/scrapeMoonlight.json'
		var write = Promise.denodeify(fs.writeFile)	

		return write(filePath, "[", 'utf8').then(function(){
			console.log("DownloadProgramMoonlight");
			function Program (resolve, reject) {
				console.log("Program");

				function getUrls(url, callback){
				
					browser.visit(url, function (e){
						console.log("visit: "+ url)
						
						var elems = browser.queryAll('div.span7.desc > ul > li > a').map(function(elem){
							var url = elem.getAttribute('href') 
							return {
								 "url": "https://perthfestival.com.au" + url
							}
						})
						console.log("Done visit: "+ elems.length + " found")
						callback(elems);
					}); 
				};
				
				// HANDLE ERROR ??? TODO
				getUrls("https://www.moonlight.com.au/perth/program/", function(data){
					var res = data
					getUrls("https://www.moonlight.com.au/perth/program/page/2", function(data){
						res = res.concat(data)
						getUrls("https://www.moonlight.com.au/perth/program/page/3", function(data){
							res = res.concat(data)

							var datas = {"data": res};
							var jsonStr = JSON.stringify(datas,null," ");
							console.log(jsonStr)
							fs.writeFile("testZOmbie.json", jsonStr)
							//if (err) reject(err);
							//else 
							resolve(res); 
						});
					});
				});
			}


			var scrape = new Promise(Program)
			.then(function(urls){
				console.log('=>done ' + urls.length)
				//urls.i = 0
				var obj = {"urls":urls, "i": 0}
				var sep = "";
				
				var prom = Promise.resolve(obj)
				.catch(function(e){
					console.error(e)
				})


				function extractData(toto){
					console.log("=== EXTRACT_DATA =================")		
					console.log("-->" + toto.i +" / " + toto.urls.length)

					var url = toto.urls[toto.i].url
					console.log("---> " + url)

					var show = toto.urls[toto.i]

					var p = new Promise(function(resolve,reject){
						browser.visit(url, function (e){
							if (e){
								console.error("loading err: " + e)
							}
							console.log("---->url Loaded " + url)
						    // Wait until page is loaded
						    function pageLoaded(window) {
						        return window.document.querySelector('h1 a');
						    }
						    function pageLoaded2(window) {
						        return window.document.querySelector();
						    }
							//browser.wait(pageLoaded, function(){
							browser.wait(5000, function(){

								var delay = 0 //number of seconds
								console.log("loaded1 - waiting " + delay +"s")

								setTimeout(function(){

									//browser.wait(pageLoaded2, function(){
									//console.log(browser.html())
									

									function getText(selector, label){
										var nodes = browser.queryAll(selector)
										console.log(label + " nodes: " + nodes.length)
										var text = browser.text(nodes[0])
										console.log("=> " + label +": " + text)

										return text
									}
									function getTextParent(selector, label){
										var nodes = browser.queryAll(selector)
										console.log(label + " nodes: " + nodes.length)
										var text = browser.text(nodes[0].parentNode)
										console.log("=> " + label +": " + text)

										return text
									}
									function getHtml(selector, label){
										var nodes = browser.queryAll(selector)
										console.log(label + " nodes: " + nodes.length)
										var html = browser.html(nodes[0])
										console.log("=> " + label +": " + html)

										return html
									}


									
									fs.appendFile(filePath, sep + JSON.stringify(show, null, 4), function(err){
	                                 //console.log('File successfully written! - Check your project directory for the log.json file');
	                            		if (!sep) sep = ",\n";     
								      	toto.i = toto.i + 1
										resolve(toto)                    
									});

								}, delay*1000)


								
								//})
							})
						})
					});

					return p
					//return toto
				}

						
				for (var i = 0; i < urls.length; i++) {
				//for (var i = 0; i < 1; i++) {
					//console.log(i)
					prom = prom.then(extractData)
				}
				return prom
			})
			.then(function (res){
			    fs.appendFile(filePath, "]", function(err){
					console.log('done ')
					console.log(res.urls.length)
					console.log(JSON.stringify(res.urls[0]))    
				});
			})
			.catch(function(e){
					console.error(e)
			});
			
			return scrape
		})
		.catch(function(e){
			console.log(e)
		});

    };
		
    if (!module.parent) {
        ///scraper.startProcess(process.argv[2]);
        var bro = new Browser({waitDuration: 10*1000});
        scraper.startProcess(bro)
    }
})();