#!/usr/bin/env node
//scrape Rooftop website

var Browser = require("zombie");
var fs = require("fs");
var Promise = require('promise');
var async = require('async');
var request = require('request')
var nock = require('nock');

var check = require('./checkTools');

// nock('https://tixframe.rooftopmovies.com.au/ticketing/')
//   //.get(/.*/)
//   .get('/SessionCapture.aspx')
//   .query(true)
//   .reply(200, 'ok')

(function(){
    var scraper = {};

    exports.startProcess = scraper.startProcess = function(browsers){
    	if (!browsers.browser) { 
    		console.log("no browser")  ;      
    		var browser = new Browser({waitDuration: 10*1000}); 
    	}
    	else { var browser = browsers.browser}
    	console.log('!!!!!! START PROCESS ROOFTOP!!!')
    	var d = new Date()
		var today = d.getFullYear()+"-"+ (d.getMonth()+1) + "-"+  d.getDate();

		var sep = "";
		//var filePath = '../scrapeJSON/scrapeRooftop_' + today +'.json'
		var filePath = '../scrapeJSON/scrapeRooftop.json'
		var write = Promise.denodeify(fs.writeFile)

		return write(filePath, "[", 'utf8').then(function(){
			var scrape = new Promise(function (resolve, reject) {
				console.log("DownloadProgramRooftop");

				function getUrls(url, callback){
					browser.visit(url, function (e){
						console.log("visit: "+ url)
						
						var elems = browser.queryAll('.event_image').map(function(elem){
							var url = elem.getAttribute('href') 
							return {
								 "url": url.slice(0,84)
							}
							//url.slice(0,84)
						})
						
						callback(elems);
					}); 
				};
				
				getUrls("https://www.rooftopmovies.com.au/program/", function(data){
					var datas = {"data": data};
					var jsonStr = JSON.stringify(datas.data[0],null," ");
					console.log(jsonStr)
					fs.writeFile("testZOmbie.json", jsonStr)
					//if (err) reject(err);
					//else 
					resolve(data); 
				});
			})
			.then(function(urls){
				console.log('=>done ' + urls.length)
				//urls.i = 0
				var obj = {"urls":urls, "i": 0}
				var sep = "";
				
				var prom = Promise.resolve(obj)
				.catch(function(e){
					console.log(e)
				})


				function extractData(toto){
					//console.log("---------------" + JSON.stringify(toto))
					console.log("=== EXTRACT_DATA "+ toto.i +" / " + toto.urls.length + " =================")		

					var url = toto.urls[toto.i].url
					console.log("---> " + url)

					var show = toto.urls[toto.i]

					var p = new Promise(function(resolve,reject){
						browser.visit(url, function (e){
							if (e){
								console.log("loading err: " + e)

							}
							console.log("----> " +url)
						    // Wait until page is loaded
						    function pageLoaded(window) {
						        return window.document.querySelector('h1 a');
						    }
						    function pageLoaded2(window) {
						        return window.document.querySelector();
						    }
							//browser.wait(pageLoaded, function(){
							browser.wait(5000, function(){

								var delay = 0 //number of seconds
								console.log("loaded1 - waiting " + delay +"s")

								setTimeout(function(){

									//browser.wait(pageLoaded2, function(){
									//console.log(browser.html())
									

									function getText(selector, label){
										var nodes = browser.queryAll(selector)
										console.log(label + " nodes: " + nodes.length)
										var text = browser.text(nodes[0])
										console.log("=> " + label +": " + text)

										return text
									}
									function getHtml(selector, label){
										var nodes = browser.queryAll(selector)
										console.log(label + " nodes: " + nodes.length)
										var html = browser.html(nodes[0])
										console.log("=> " + label +": " + html)

										return html
									}

									//extract title:
									var txt = getText('h1 a', "title")
										if (txt.search(/\(Encore Screening\)/i) > -1 ) {
								            txt = txt.slice(0,-18)
								            console.log("==> Title: " + txt)
								        } else if (txt.search(/\(Encore Screening [1-9]\)/i) > -1){
								            txt = txt.slice(0,-20)
								            console.log("==> Title: " + txt)
								        }
								        //toto.urls[toto.i].title = txt
							        show.movieTitle = txt.trim()


							        // Extract //Date
							        var dt = getText('[id*="dates"]', "date")
								        //console.log("=>" + text + " WARNING YEAR IS MANUAL")
					                    show.Date = JSON.stringify(dt)
					                    var year = check.checkYear(dt);
					                    console.log(year)
					                    show.startDay = new Date(dt + year);
					                    show.endDay = show.startDay;
					                   	console.log("=> " + show.startDay)


				                    //Rating
				                    var rt = getText('[id*="event"] > div > div.info.clearfix > div:nth-child(11)', "rating")
				                    show.parentalRating = check.checkRating(rt); 


				                    // DIRECTOR
				                    var dir = getHtml('[id*="event"] > p', "Director")
				                        var regExp = /Director: <\/b>(.*?)(?=<br>)/
				                        var re = regExp.exec(JSON.stringify(dir))
				                        if (re) {
				                            console.log("=> movieDirector: " + re[1].trim())
				                            show.movieDirector = re[1].trim()
				                        } else {
				                            console.log("=> movieDirector: undefined!")
				                        }

				                    //GateOpen
					                var go = getText('[id*="doors_open"] div', "GateOpen")
					                	if (!go) {go = "6:00 PM" ; console.log("GateOpen is Manual!!")}
				                        show.gateOpen = go.trim()
				                    
				                    //SartTime
					                var st = getText('[id*="time"]', "SartTime")
				                        show.movieStart = st.trim()

				                    show.venueId = "554ed6bed617526c16816cf1"
				                    
				                    show.language = ""
				                    show.subtitle = ""


				                    fs.appendFile(filePath, sep + JSON.stringify(show, null, 4), function(err){
					                    if (!sep) sep = ",\n";
								        
								        toto.i = toto.i + 1
										resolve(toto)                    
									});


								}, delay*1000)


								
								//})
							})
						})
					});

					return p
				}

						
				for (var i = 0; i < urls.length; i++) {
				//for (var i = 0; i < 1; i++) {
					//console.log(i)
					prom = prom.then(extractData)
				}
				return prom
			})
			.then(function (res){
			    fs.appendFile(filePath, "]", function(err){
					console.log('done ')
					console.log(res.urls.length)
					console.log(JSON.stringify(res.urls[0]))
					//return res    
				});
				return {"data":res, "browser": browser}
			})
			.catch(function(e){
				console.log(e)
			});

			return scrape
		})
		.catch(function(e){
			console.log(e)
		});

    };

    if (!module.parent) {
        ///scraper.startProcess(process.argv[2]);
        var bro = new Browser({waitDuration: 10*1000});
        scraper.startProcess(bro)
    }
})();