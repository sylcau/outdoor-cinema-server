#!/usr/bin/env node
//scrape Cinema website

var Browser = require("zombie");
var fs = require("fs");
var Promise = require('promise');
var async = require('async');
var request = require('request');
var nock = require('nock');
var mongoose = require('mongoose');
var path = require('path');


var check = require('./checksAndUploadJS/checkTools');

//** IMPORT CONSTANT 
var config = require('../config');

//var log = console.log.bind(console);
// var colors = require('colors');
// colors.setTheme({
//   silly: 'rainbow',
//   input: 'grey',
//   verbose: 'cyan',
//   prompt: 'grey',
//   info: 'green',
//   data: 'grey',
//   help: 'cyan',
//   warn: 'yellow',
//   debug: 'blue',
//   error: 'red'
// });

var log = {};
[
 [ 'warn',  '\x1b[33m' ], // Yellow
 [ 'info',  '\x1b[32m' ], // green
 [ 'error', '\x1b[31m' ], // red
 [ 'log',   '\x1b[0m'  ] //
].forEach(function(pair) {
  var method = pair[0], reset = '\x1b[0m', color = '\x1b[36m' + pair[1];
  log[method] = console[method].bind(console, color, method.toUpperCase(), reset);
});

// getProgram is a Promise, readHtml is a sync function, cinema is a string

var scrapeCinema = require('./scrapeCinema').startProcess;
var checkScrapedShows = require('./checksAndUploadJS/checkScrapedShows').checkScrapedShows;
var uploadNewMovies = require('./checksAndUploadJS/uploadNewMovies').uploadNewMovies;
var uploadNewShows = require('./checksAndUploadJS/uploadNewShows').uploadNewShows;
var getImage = require('./checksAndUploadJS/getImage').getImageNewMoviesAWS;
var getTrailer = require('./checksAndUploadJS/getTrailer').getTrailer;
var getShowsEndDate = require('./checksAndUploadJS/getShowsEndDate').getShowsEndDate;
var sendEmail = require('./checksAndUploadJS/sendEmail').sendEmail;


(function(){
   var zombieProcess = {};

   exports.startCycle = zombieProcess.startCycle = function(cinema){
    	
      console.log('')
      log.info('cycleCinema.js: ' + cinema)  

      var cinemafilename = path.join(__dirname, 'cinemaJS', cinema + '.js' );
     	console.log('loading ' + cinemafilename)  
     	
      var cinemaFunctions = require(cinemafilename)

      var mongooseState = mongoose.connection.readyState
      console.log("moongooseDB: " + mongooseState)
      
      if (mongooseState != 1 && mongooseState != 2){
         log.info("mongoDb not connected. starting connection....  | mongooseState = " + mongooseState + " " + typeof mongooseState )
         mongoose.connect(config.MONGO_URI,function(err){
            if (err){
               console.log('MongoDB Connection Error. Please make sure that MongoDB is running.' + err);
               throw new Error(err)
            } else{
               log.info('MongoDB Connected');
            }
         });      
      } else {
         console.log('MongoDB already Connected')
      }


    	var promiseResult = scrapeCinema(cinemaFunctions.getProgram, cinemaFunctions.readHtml, cinemaFunctions.cinema)
    	//var promiseResult = Promise.resolve({jsonFile: 'scrape' + cinema, cinema: cinema, log: ""}) //if already scrape.
    	.then(checkScrapedShows)
    	.then(function(result) {
    		if (result.movies2Upload > 0) {
    			return uploadNewMovies(result).then(checkScrapedShows)
    		}
    		else {
    			return result
    		}
    	})
    	.then(uploadNewShows)
    	.then(getImage)
    	.then(getTrailer)
      .then(getShowsEndDate)
    	.then(sendEmail)
		.catch(function(e){
			log.error(e)
		});

		return promiseResult

   };
		
   if (!module.parent) {

      // read arguments.
      if (process.argv.length < 3 ) {
        	console.log('missing last arguments. require cinema Name for functions cinema specific. Choices are:')
        	var files = fs.readdirSync('./cinemaJS')
        	files.forEach(function(val, index){
        		console.log(index + " " + val.slice(0,-3))
        	})
        	return
      } 
        
      if (process.argv.length == 3 ) {
        	var cinema = process.argv[2]
        	log.info("starting cycle for " + cinema)

         zombieProcess.startCycle(cinema)
         .then(function(){
            process.exit()
         })
         .catch(function(err){
            console.log(err)
         })
	   }
   }
})();



