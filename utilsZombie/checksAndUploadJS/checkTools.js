var Promise = require('promise');

module.exports = (function(){
    var obj = {};
    
    obj.checkRating = function (string){
        if (typeof string != "string") { return "Error"}
        if (string.search(/\(G/) > -1 ) { return "G"}
        if (string.search(/GENERAL ADMISSION/i) > -1 ) { return "G"}
        

        if (string.search(/\(PG/) > -1 ) { return "PG"}
        if (string.search(/PARENTAL GUIDENCE/i) > -1 ) { return "PG"}
        

        if (string.search(/\(M\)/) > -1 ) { return "M"}
        if (string.search(/\(CTC/) > -1 ) { return "CTC"}
        if (string.search(/\(MA15+/) > -1 ) { return "MA15+"}
        if (string.search(/\(R18+/) > -1 ) { return "R18+"}

        if (string.search("Private Screening") > -1 ) { return ("-")}

        return "CTC"
    };

    obj.checkYear = function (string){
        if (typeof string != "string") { return " Error - " + typeof string}
        if (string.search(/SEP/i) > -1 ) { return " 2015"}
        if (string.search(/OCT/i) > -1 ) { return " 2015"}
        if (string.search(/NOV/i) > -1 ) { return " 2015"}
        if (string.search(/DEC/i) > -1 ) { return " 2015"}
        if (string.search(/JAN/i) > -1 ) { return " 2016"}
        if (string.search(/FEB/i) > -1 ) { return " 2016"}
        if (string.search(/MAR/i) > -1 ) { return " 2016"}
        if (string.search(/APR/i) > -1 ) { return " 2016"}
        if (string.search(/MAY/i) > -1 ) { return " 2016"}
        if (string.search(/JUN/i) > -1 ) { return " 2016"}

        return " Error"
    };


    obj.getText = function(selector, label, browser){
        var nodes = browser.queryAll(selector)
        console.log(label + " nodes: " + nodes.length)
        if (nodes.length > 0){
            var text = browser.text(nodes[0])
            console.log("=> " + label +": " + text)
            return text        
        } 
        else { 
            return null
        }
    };
    
    obj.getTexts = function(selector, label, browser){
        var nodes = browser.queryAll(selector)
        console.log(label + " nodes: " + nodes.length)
        if (nodes.length > 0){
            var text = ""
            for (var i = 1; i< nodes.length; i++){
                if (browser.text(nodes[i])) {
                    text = text + browser.text(nodes[i]) + " "
                }
            }
            console.log("=> " + label +": " + text)
            return text        
        } 
        else { 
            return null
        }
    };

    obj.getTextParent = function(selector, label, browser){
        var nodes = browser.queryAll(selector)
        console.log(label + " nodes: " + nodes.length)
        var text = browser.text(nodes[0].parentNode)
        console.log("=> " + label +": " + text)

        return text
    };
    
    obj.getHtml = function(selector, label, browser){
        var nodes = browser.queryAll(selector)
        console.log(label + " nodes: " + nodes.length)
        var html = browser.html(nodes[0])
        console.log("=> " + label +": " + html)

        return html
    };

    obj.getAttribute = function(selector, label, browser){
        var nodes = browser.queryAll(selector)
        console.log(label + " nodes: " + nodes.length)
        
        if (nodes.length > 0 ) {
            var href = nodes[0].getAttribute('href')
            console.log("=> " + label +": " + href)
        } else {
            var href = null
        }

        return href
    };

    obj.movieDuplicate = function(movie, Movie){ // return res.bool and res.movie
        var res = {bool: false, movie: movie} //if duplicate find: bool = false.
        
        var pr = new Promise(function(resolve, reject){

            //check if duplicate!!!!
            var arr = [];
            if (typeof movie.imdbID != "undefined") { arr.push({imdbID: movie.imdbID}) }
            else if ((typeof movie.Title != "undefined") && (typeof movie.Year != "undefined"))  { arr.push({'Title': movie.Title, 'Year': movie.Year }) }

            if (arr.length == 0){
              console.log("issue: movie with not enough infos.")
              reject("issue: movie with not enough infos.")
            };
        
            Movie.find({"$or": arr})
            .exec(function (err, mv){
              if (err) {
                var mess = "Duplicate error: " + err
                //logf.push(mess)
                console.log(mess)
                reject(mess)

              } else if (mv.length == 0) {
                //search duplicate in MoviesDb
                var mess = "No Duplicate find for: " + JSON.stringify(arr)
                //logf.push(mess)
                console.log(mess)
                res.bool = true;
                resolve(res)

              } else {
                var mess = "find duplicate for " + mv[0].Title
                //logf.push(mess)
                console.log(mess)
                resolve(res)
              }
            });
        });

        return pr
    };

    obj.maxMovieRef = function(moviesArray){ //mvRef: "OC2001" --> return 2001
      moviesArray.sort(function(a, b){return parseInt(b.movieRef.slice(-4)) - parseInt(a.movieRef.slice(-4))});
      return parseInt(moviesArray[0].movieRef.slice(-4)) //first value as a number 
    };

    obj.maxShowRef = function(showArray){ //idRef: "101" --> return 101
      showArray.sort(function(a, b){ return parseInt(b.idRef) - parseInt(a.idRef) } );
      return parseInt(showArray[0].idRef) //first value as a number 
    };

    return obj

})();



