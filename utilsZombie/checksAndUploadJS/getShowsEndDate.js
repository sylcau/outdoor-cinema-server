#!/usr/bin/env node
//getShowsEndDate

// var Browser = require("zombie");
var fs = require("fs");
var Promise = require('promise');
var async = require('async');
var request = require('request')
// var nock = require('nock');

var mongoose = require('mongoose');
var rp = require('request-promise');

//var AWS = require('aws-sdk');

//** IMPORT CONSTANT 
var config = require('../../config');

var check = require('./checkTools');

// Import MODEL
var model = require('../../model/ShowMoviesVenues');

(function(){
	var log = {};
	[
	 [ 'warn',  '\x1b[33m' ], // Yellow
	 [ 'info',  '\x1b[32m' ], // green
	 [ 'error', '\x1b[31m' ], // red
	 [ 'log',   '\x1b[0m'  ] //
	].forEach(function(pair) {
	  var method = pair[0], reset = '\x1b[0m', color = '\x1b[36m' + pair[1];
	  log[method] = console[method].bind(console, color, method.toUpperCase(), reset);
	});

   var getShowsEndDate = {};

   exports.getShowsEndDate = getShowsEndDate.getShowsEndDate = function(param){
    	console.log("")
    	log.info("getShowsEndDate.js: get dates of last shows per venues");

    	var Show = model.Show;
    	var Venue = model.Venue;
		
    	var pathFolder = param.pathFolder || "./"
    	// if (param == null) {
    	// 	param.jsonFile = 
    	// }

    	var mongooseState = mongoose.connection.readyState
    	console.log("moongooseDB: " + mongooseState)

    	if (mongooseState != 1 && mongooseState != 2){
			log.info("mongoDb not connected. starting connection....  | mongooseState = " + mongooseState + " " + typeof mongooseState )
			mongoose.connect(config.MONGO_URI,function(err){
				if (err){
					console.log('MongoDB Connection Error. Please make sure that MongoDB is running.' + err);
					//throw new Error(err)
				} else{
					log.info('MongoDB Connected');
				}
			});    	
		} else {
			console.log('MongoDB already Connected')
		}

    	var promiseResult = new Promise(function(resolve, reject){

	   	//
			var startYear = new Date(2015, 8, 1);
			var endYear = new Date(2016, 8, 1);
			log.warn("year selection: "+ startYear.toDateString() + " to " + endYear.toDateString())

			Venue.find({})
			.exec()
			.then(function(venues){
				Show.find({startDay: {$gte: startYear , $lt: endYear}})
	    		.exec(function(err, shows){
		    		if (err) { return reject(err)}
		        	
		        	venues.forEach(function(val){
		        		val.shows = shows.filter(function(value){
		        			//filter shows by venues
		        			if (value.venueId == val._id.toString()) {
		        				return true
		        			} else {
		        				return false
		        			}
		        		})
		        		// sort by dates
		        		val.shows.sort(function (a, b) {
						  if (new Date(a.startDay) > new Date(b.startDay)) {
						    return -1;
						  }
						  if (new Date(a.startDay) < new Date(b.startDay)) {
						    return 1;
						  }
						  // a must be equal to b
						  return 0;
						});

		        	})

        			param.venues = venues


					var stringLog = ""

					param.venues.forEach(function(val){
						console.log("\n -- " + val.cinema + " " + val.suburb)
						stringLog = stringLog + " <br> \n <b>-- " + val.cinema + " " + val.suburb + ": </b>"
						if (typeof val.cinema == "undefined" || typeof val.suburb == "undefined" || typeof val.shows == "undefined") return
						
						if (val.shows.length > 0 && new Date(val.shows[0].startDay) < new Date()) {
							stringLog = stringLog + ' <span style="color:red;"> ToUpdate  </span><br> \n'
						} else {
							stringLog = stringLog + "<br> \n"
						}
						
						for (var i = 0; i < Math.min(val.shows.length, 3); i++){
							console.log(val.cinema + " " + val.suburb + ": " + val.shows[i].startDay)
							stringLog = stringLog + val.cinema + " " + val.suburb + ": " + val.shows[i].startDay + "<br> \n"
						}
						
					})

					param.log = param.log + "<br> \n <br> \n Shows End Date:" + stringLog 

	        		resolve(param)
	      	})
			})


			
	   })
	   .catch(function(e){
	    	log.error("error getShowsEndDate "  + e)
	   });

		return promiseResult
	};

		
   if (!module.parent) {

		getShowsEndDate.getShowsEndDate({pathFolder: "../", log: ""})
		.then(function(re){
			console.log("Done getShowsEndDate - nb of venues: " + re.venues.length)
			
			process.exit()
		})
		.catch(function(e){
			console.log("err " + e)
			process.exit()
		});

   }
})();




//Limitation of free API. 
// Change to series 


				// var trailerPromises = []
				// var trailerPromiseFactory = function(mov){
				// 	// get infos from TMDB
			 //      // if info save Movie
			 //      ////////////////////
				// 	var url = "http://api.themoviedb.org/3/movie/" + mov.imdbID + "/videos?external_source=imdb_id&api_key=ba604f19ae9c593275b33b242ac4bb10"

				// 	var prom = rp(url)
    //            .then(function(data){
	   //             console.log("=> " + url)
	   //             // if (typeof data.results == "undefined") { 
	   //             //   console.log(JSON.stringify(data))
	   //             //   return {status: "error"} 
	   //             // }
	   //             var dt = JSON.parse(data)
	   //             if (dt.results.length == 0) {
	   //                console.log("no trailer for: " + mov.imdbID  + " | " + mov.Title)
	   //                return {trailer: null, title: null, doc: mov} 
	   //             } 
	   //             var trailer = dt.results[0].key
	   //             var title = dt.results[0].name
	                
	   //             mov.trailer = dt.results[0].key
	   //             mov.save(function(){
	   //                console.log(trailer + " | " + title + " | " + mov._id + " | " + mov.imdbID)
	   //                //console.log("^^^^^^^^^")
	   //                return {trailer: trailer, title: title, doc: mov}
	   //             })
	   //          })
	   //          .catch(function(err){
	   //             console.error(err)
	   //             //console.log("^^^^^^^^^")
	   //             return {trailer: null, title: null, doc: mov}
	   //          })

				// 	return prom
				// }

				// for (var i = 0; i < movies.length; i++){
				// //for (var i = 0; i < 2; i++){
	   //      		trailerPromises.push(trailerPromiseFactory(movies[i]))
	   //      	}	        

				// Promise.all(trailerPromises)
				// .then(function(resu){
				// 	console.log("ok")
				// 	resolve(resu.length)
				// })
				// .catch(function(){
				// 	reject()
				// })


//
// EXAMPKE OF EQUENCE
// Start off with a promise that always resolves
// var sequence = Promise.resolve();

// // Loop through our chapter urls
// story.chapterUrls.forEach(function(chapterUrl) {
//   // Add these actions to the end of the sequence
//   sequence = sequence.then(function() {
//     return getJSON(chapterUrl);
//   }).then(function(chapter) {
//     addHtmlToPage(chapter.html);
//   });
// });

///

       