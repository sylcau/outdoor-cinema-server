#!/usr/bin/env node
//upload new Movies

// var Browser = require("zombie");
var fs = require("fs");
var Promise = require('promise');
var async = require('async');
// var request = require('request')
// var nock = require('nock');
var path = require('path');

var mongoose = require('mongoose');


var check = require('./checkTools');


//** IMPORT CONSTANT 
var config = require('../../config');

// Import MODEL
var model = require('../../model/ShowMoviesVenues');

(function(){
	var log = {};
	[
	 [ 'warn',  '\x1b[33m' ], // Yellow
	 [ 'info',  '\x1b[32m' ], // green
	 [ 'error', '\x1b[31m' ], // red
	 [ 'log',   '\x1b[0m'  ] //
	].forEach(function(pair) {
	  var method = pair[0], reset = '\x1b[0m', color = '\x1b[36m' + pair[1];
	  log[method] = console[method].bind(console, color, method.toUpperCase(), reset);
	});

   var upload = {};

   exports.uploadNewMovies = upload.uploadNewMovies = function(param){
    	console.log("")
    	log.info("upload new movies")
    	var Movie = model.Movie;

    	//var pathFolder = param.pathFolder || "./"
    	// if (param == null) {
    	// 	param.jsonFile = 
    	// }

    	var promiseResult = new Promise(function(resolve, reject){
	    	// read JSON - create table
		   var movieFile = JSON.parse(fs.readFileSync( path.join(__dirname, "../movies2Upload.json") , 'utf8'));
		   log.info("movie2Upload read: " + movieFile.length)
		    
		   var logf = [];
		   var nbMovie;

		    //for Each > check if duplicate // then save 

		   Movie.find({})
		   .select("movieRef")
		   .exec(function (err, movies){
		      if (err) {
		        log.error(err);
		        console.log(err)
		        reject("error")
		      } else {
		        nbMovie = check.maxMovieRef(movies);
		        console.log("nb movie in db: " + movies.length + " " + nbMovie)
		        resolve({nbMovie: nbMovie, movieFile: movieFile})
		      }
		   })
		})
		.then(function(resu){
			console.log("then resu")
			// function executeSequentially(movieFile) {
			//   	var result = new Promise.resolve();
			//   	promiseFactories.forEach(function (promiseFactory) {
			//     	result = result.then(promiseFactory);
			//  	});
			//   	return result;
			// }


	    	var pro = Promise.resolve(0)
	    	var nbuploaded = 0
	    	//.then(function(){
	    	resu.movieFile.forEach(function(movie, index){
	    		var pr2 = function(){
	    			console.log("ok " + movie.imdbID)
	    			return check.movieDuplicate(movie, Movie)
			        .then(function(res){
			        	if(res.bool){ //bool = true - movie must be saved

			                var movieObj = new Movie(res.movie)
			                resu.nbMovie = resu.nbMovie + 1

			                if (typeof movieObj.movieTitle == "undefined"){ movieObj.movieTitle = movieObj.Title };
			                if (typeof movieObj.movieRef == "undefined"){ 
			                	movieObj.movieRef = "OC" + resu.nbMovie 
			                };
			                
			                console.log('movie Model: ' + movieObj.movieRef + "  = " + movieObj.movieTitle + "  = " + movieObj.Title )

			                return new Promise(function(resolve,reject){
			                	movieObj.save(function (err, movieDoc) {
				                  	if (err) {
				                    	log.error('err saved' + err)
				                    	console.log("error saving movie: " + err)
				                    	reject(e)
				                  	} ;
				                  	log.info('==>movie saved: ' + movieDoc.movieRef + "  = " + movieDoc.movieTitle)
				                  	//return callback(movieDoc)
				                  	resolve(1)
				                })
				            })

			        	} else { // find duplicate
			        		//return callback()
			        		return 0
			        	}
			        })
			    }
	    		pro = pro.then(pr2).then(function(re){
	    			nbuploaded = nbuploaded + re
	    			console.log("  " + re +" | nbuploadedb of movie saved"+ nbuploaded)
					console.log(" *************** " + index)
					return nbuploaded
	    		}) 
	    	})		    		
	    	//})
		   //  	var pro = new Promise(function(resolve,reject){
		   //  		setTimeout(function(str1, str2) {
					//   console.log(str1 + " " + str2);
					//   resolve([200,2])
					// }, 3000, "Hello.", "How are you?");
		   //  	})
		    		// setTimeout(() => {
	       //      		resolve(value + 1);
	       //  		}, 0);



	    	return pro
	   })
	   .then(function(results){
			log.info("uploadNewMovies end: " + results + "\n")
			//if (results) console.info("nb results: "+ results.length)
	    	//console.log("done")
	    	param.log = param.log + "<br> \n" + 
      		" *** SUMMARY Upload New Movies *** " + "<br> \n" +
				"number of New Movies uploaded : " + results 
      		
	    	return param
	   })
	   .catch(function(e){
	    	log.error("error Movie.find "  + e)
	   });

		return promiseResult
	};
		
   if (!module.parent) {

		// CONNECT DB 
		mongoose.connect(config.MONGO_URI,function(err){
			if (err){
				console.error('MongoDB Connection Error. Please make sure that MongoDB is running.');
			} else{
				console.log('MongoDB Connected');
				upload.uploadNewMovies({pathFolder: "../", log: ""})
				.then(function(re){

				})
				.catch(function(e){
					console.log("err " + e)
				});

			}
		});

     //    //var bro = new Browser({waitDuration: 10*1000});

     //    // read arguments.
     //    if (process.argv.length < 3 ) {
     //    	console.log('missing last arguments. require cinema Name for functions cinema specific. Choices are:')
     //    	var files = fs.readdirSync('./cinemaJS')
     //    	files.forEach(function(val, index){
     //    		console.log(index + " " + val.slice(0,-3))
     //    	})
     //    	return
     //    } 
        
     //    if (process.argv.length == 3 ) {
     //    	// var cinema = process.argv[2]
     //    	// var dir = './cinemaJS/' + cinema + '.js'
     //    	// console.log('loading ' + dir)  
     //    	// var cinemaFunctions = require(dir)

	    //     // scraper.startProcess(bro, cinemaFunctions.getProgram, cinemaFunctions.readHtml, cinemaFunctions.cinema )
	    // }
   }
})();






       