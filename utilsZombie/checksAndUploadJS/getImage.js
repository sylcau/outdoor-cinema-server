#!/usr/bin/env node
//getImage new Movies

// var Browser = require("zombie");
var fs = require("fs");
var Promise = require('promise');
var async = require('async');
var request = require('request')
// var nock = require('nock');
var colors = require('colors');
var mongoose = require('mongoose');

var AWS = require('aws-sdk');

//** IMPORT CONSTANT 
var config = require('../../config');

var check = require('./checkTools');

// Import MODEL
var model = require('../../model/ShowMoviesVenues');

(function(){
	var log = {}, c= {}
	var Tag = [
	 [ 'warn',  '\x1b[33m', 'yellow' ], // Yellow
	 [ 'info',  '\x1b[32m', 'yellow' ], // green
	 [ 'error', '\x1b[31m', 'yellow' ] // red
	 //[ 'log',   '\x1b[0m', 'yellow'  ] //
	];
	Tag.forEach(function(pair) {
	  var method = pair[0], reset = '\x1b[0m', color = '\x1b[36m' + pair[1];
	  log[method] = console[method].bind(console, color, method.toUpperCase(), reset);
	  c[method] = function (toto) {
	  		console.log(colors.yellow('%s ', method.toUpperCase()), '' , toto);
	  }
	});

   var getImage = {};


 	//   exports.getImageNewMovies = getImage.getImageNewMovies = function(param){
 //    	console.log("getImage new movies")
 //    	var Movie = model.Movie;

 //    	var pathFolder = param.pathFolder || "./"
 //    	// if (param == null) {
 //    	// 	param.jsonFile = 
 //    	// }
 //    	var mongooseState = mongoose.connection.readyState
 //    	log.info("moongooseDB: " + mongooseState)

 //    	if (mongooseState != 1 || mongooseState != 2){
	// 		log.info("mongoDb not connected. starting connection....  | mongooseState = " + mongooseState + " " + typeof mongooseState )
	// 		mongoose.connect(config.MONGO_URI,function(err){
	// 			if (err){
	// 				console.log('MongoDB Connection Error. Please make sure that MongoDB is running.' + err);
	// 				//throw new Error(err)
	// 			} else{
	// 				log.info('MongoDB Connected');
	// 			}
	// 		});    	
	// 	}

 //    	var promiseResult = new Promise(function(resolve, reject){
	//     	// read JSON - create table
	// 	   //Download Images: (Need to have the Poster field filled. from OMDB)
	// 	   var downloadImg = function(uri, filename, title, callback){
	// 	      if (typeof uri == "undefined" || uri == "N/A") {
	// 	        console.log("error: "+ title)
	// 	        //uri = "./public/img/movies/" + mov.movieRef + ".jpg" 
	// 	        return callback
	// 	      }
	// 	      request.head(uri, function(err, res, body){
	// 		      if (err){ 
	// 		         console.log("error Dw: " + err)
	// 		         return callback
	// 		      }
		      
	// 	      	request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
	// 	      });
	// 	   };

	//     	//get list of movies:
	// 	   Movie.find({})
	// 	   .select('Poster movieTitle img movieRef') 
	// 	   .exec(function(err, movies) {
	// 	   	if (err) { return reject(err)}
	//         	console.log("got movies")

	//         //create array of promises
	// 			var imgPromises = []
	// 			var imgPromiseFactory = function(obj){
	// 				var prom = new Promise(function(resolve,reject){
	// 		         var dest = "../../../OutdoorCine/app/img/movies/" + obj.movieRef + ".jpg"
	// 		         if (typeof obj.Poster != "undefined" && obj.Poster != "N/A" && obj.Poster != "undefined"){
	// 		            // check if file already exist:
	// 		            fs.access(dest, fs.F_OK, function(err){
	// 		              	if (!err) {
	// 			               //console.log(mov.Poster + " : already exist")
	// 			               //if (obj.img != "./img/movies/" + obj.movieRef + ".jpg"){
	// 			               if (obj.img != "http://s3-ap-southeast-2.amazonaws.com/my-outdoor-cinema/img/movies/" + obj.movieRef + ".jpg"){	
	// 			               	obj.img = "http://s3-ap-southeast-2.amazonaws.com/my-outdoor-cinema/img/movies/" + obj.movieRef + ".jpg"
	// 			               	obj.save(function(){

	// 										return resolve()
	// 				            	})	
	// 			               }
	// 			               return resolve()

	// 		              	} else {
	// 		                // download new file
	// 		                downloadImg(obj.Poster, dest, obj.movieTitle, function(){
	// 		                  console.log('done: ' + obj.movieRef + "| " + obj.movieTitle);
	// 		                  obj.img = "http://s3-ap-southeast-2.amazonaws.com/my-outdoor-cinema/img/movies/" + obj.movieRef + ".jpg"
	// 		                  obj.save(function(){
	// 		                    return resolve()
	// 		                  })
	// 		                });   
	// 		              };

	// 		            });
	// 		         } else {
	// 		            console.log('No URI Infos for: ' + obj.movieTitle)
	// 		            return resolve()
	// 		         }

	// 				})
	// 				return prom
	// 			}
	// 			for (var i = 0; i < movies.length; i++){
	//         		imgPromises.push(imgPromiseFactory(movies[i]))
	//         	}	        

	// 			Promise.all(imgPromises)
	// 			.then(function(resu){
	// 				console.log("ok")
	// 				resolve(resu.length)
	// 			})
	// 			.catch(function(){
	// 				reject()
	// 			})

	// 	   });
	// 	})
	//    .catch(function(e){
	//     	log.error("error Movie.find "  + e)
	//    });

	// 	return promiseResult
	// };

   exports.getImageNewMoviesAWS = getImage.getImageNewMoviesAWS = function(param){
    	console.log("\n \n")
    	log.info("getImage new movies")
    	log.warn("getImage new movies")
    	log.error("getImage new movies")

    	c.info("c work!!!")
    	var Movie = model.Movie;
		
		var s3Conf = new AWS.Config({
		  	accessKeyId: config.AWS_accessKeyId,          
		  	secretAccessKey: config.AWS_secretAccessKey 
		});
		var s3 = new AWS.S3(s3Conf);

    	var pathFolder = param.pathFolder || "./"
    	// if (param == null) {
    	// 	param.jsonFile = 
    	// }
    	var mongooseState = mongoose.connection.readyState
    	console.log("moongooseDB: " + mongooseState)

    	if (mongooseState != 1 && mongooseState != 2){
			log.info("mongoDb not connected. starting connection....  | mongooseState = " + mongooseState + " " + typeof mongooseState )
			mongoose.connect(config.MONGO_URI,function(err){
				if (err){
					console.log('MongoDB Connection Error. Please make sure that MongoDB is running.' + err);
					//throw new Error(err)
				} else{
					log.info('MongoDB Connected');
				}
			});    	
		} else {
			console.log('MongoDB already Connected')
		}

    	var promiseResult = new Promise(function(resolve, reject){
	    	// read JSON - create table
		   
		   //Download Images: (Need to have the Poster field filled. from OMDB)
		   var downloadImg = function(uri, filename, title, callback){
		      if (typeof uri == "undefined" || uri == "N/A") {
		        console.log("error: "+ title)
		        //uri = "./public/img/movies/" + mov.movieRef + ".jpg" 
		        return callback
		      }
		      request.head(uri, function(err, res, body){
			      if (err){ 
			         console.log("error Dw: " + err)
			         return callback
			      }
		      
		      	request(uri).pipe(fs.createWriteStream(path.join(__dirname, filename ))).on('close', callback);
		      });
		   };

		   var uploadImg = function(uri, filename, title, callback){
		      
		   };

	    	//get list of movies:
		   Movie.find({})
		   .select('Poster movieTitle img movieRef') 
		   .exec(function(err, movies) {
		   	if (err) { return reject(err)}
	        	console.log("got movies")
 
	        //create array of promises
				var imgPromises = []
				var imgPromiseFactory = function(obj){
					var prom = new Promise(function(resolve,reject){

			         //check if this movie has a corresponding file on s3
			         // - if not, check if obj.poster exist. is so downlload in .Tmp upload file.
			         // - if yes  
			         // check ref address 
			         // save file
			         ////////////////////
			         var file = "img/movies/" + obj.movieRef + ".jpg"
			         var params = {
			         	Bucket: "my-outdoor-cinema",
    						Key: file
			         }

						s3.headObject(params, function (err, metadata) {  
						  	if (err && err.code === 'NotFound') {
						  		var dest = "tmp/" + obj.movieRef + ".jpg"  
						    	// Handle no object on cloud here 
						    	if (typeof obj.Poster != "undefined" && obj.Poster != "N/A" && obj.Poster != "undefined"){
						    		console.log("Downloading Img")
									downloadImg(obj.Poster, dest, obj.movieTitle, function(){
				                  console.log('done: ' + obj.movieRef + "| " + obj.movieTitle);
				                  obj.img = "http://s3-ap-southeast-2.amazonaws.com/my-outdoor-cinema/img/movies/" + obj.movieRef + ".jpg"
				                  
				                  var stream = fs.createReadStream(path.join(__dirname, dest ))
				                  var params = {Bucket: "my-outdoor-cinema", Key: file, ACL: "public-read" , Body: stream};
										s3.upload(params, function(err, data) {
										  	if (err) {
										  		console.log(err)
										  		reject(err)
										  	}
										  	console.log(obj.movieRef + " upload complete");

										  	//fs.unlink(dest,)

										  	obj.save(function(){
					                    	return resolve({downloaded:1})
					                  })
										});

				                 
				               });   
						    	} else {
					            console.log('No URI Infos for: ' + obj.movieTitle)
					            return resolve({NoUri:1})
					         }

						  	} else {  
						    	// object find - check ref address and save
						    	//console.log(obj.movieRef + " - find corresponding file ")

						    	if (obj.img != "http://s3-ap-southeast-2.amazonaws.com/my-outdoor-cinema/img/movies/" + obj.movieRef + ".jpg") {	
				               	obj.img = "http://s3-ap-southeast-2.amazonaws.com/my-outdoor-cinema/img/movies/" + obj.movieRef + ".jpg"
				               	obj.save(function(){
											return resolve(0)
					            	})	
				               }
				            return resolve()
						  	}
						});			            
					})
					return prom
				}
				for (var i = 0; i < movies.length; i++){
				//for (var i = 0; i < 2; i++){
	        		imgPromises.push(imgPromiseFactory(movies[i]))
	        	}	        

				Promise.all(imgPromises)
				.then(function(resu){
					console.log("getImage.js: all promises done")
					var nbDownloaded = resu.filter(function(val){
						if (typeof val == "object" && 'downloaded' in val) {return true}
						else {return false}
					} ).length;
					var noUri = resu.filter(function(val){
						if (typeof val == "object" && 'NoUri' in val) {return true}
						else {return false}
					} ).length;
					
					param.log = param.log + "<br> \n <br> \n" + 
									"Processed getImage: " + resu.length + "<br> \n" + 
									"nb of Images downloaded: " + nbDownloaded + "<br> \n" +
									"nb of Images with no Uri: " + noUri
					param.getImage = resu.length
					resolve(param)
				})
				.catch(function(err){
					reject(err)
				})

		   });
		})
	   .catch(function(e){
	    	log.error("error Movie.find "  + e)
	   });

		return promiseResult
	};

		
   if (!module.parent) {

		getImage.getImageNewMoviesAWS({pathFolder: "../", log:""})
		.then(function(re){
			log.info("Done getImage: " + re.getImage)
			process.exit()
		})
		.catch(function(e){
			console.log("err " + e)
		});

   }
})();






       