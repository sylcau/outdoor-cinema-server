#!/usr/bin/env node
//upload new Shows

// var Browser = require("zombie");
var fs = require("fs");
var Promise = require('promise');
var async = require('async');
// var request = require('request')
// var nock = require('nock');
var path = require('path');

var mongoose = require('mongoose');


var check = require('./checkTools');


//** IMPORT CONSTANT 
var config = require('../../config');

// Import MODEL
var model = require('../../model/ShowMoviesVenues');

(function(){
	var log = {};
	[
	 [ 'warn',  '\x1b[33m' ], // Yellow
	 [ 'info',  '\x1b[32m' ], // green
	 [ 'error', '\x1b[31m' ], // red
	 [ 'log',   '\x1b[0m'  ] //
	].forEach(function(pair) {
	  var method = pair[0], reset = '\x1b[0m', color = '\x1b[36m' + pair[1];
	  log[method] = console[method].bind(console, color, method.toUpperCase(), reset);
	});

   var upload = {};

   exports.uploadNewShows = upload.uploadNewShows = function(param){
    	console.log("")
    	log.info("upload new shows")
    	var Movie = model.Movie;
		var Show = model.Show;
		var write = Promise.denodeify(fs.writeFile)

   	var pathFolder = param.pathFolder || "./"

    	var promiseResult = new Promise(function(resolve, reject){

		   // read JSON of SHOWS to Import
		   var shows2Upload = JSON.parse(fs.readFileSync( path.join(__dirname, "../shows2Upload.json") , 'utf8'));
		   console.log("----------" + shows2Upload.length)

		   if (shows2Upload.length == 0 ) {
		    	console.log("shows2Upload is empty - end of process")
		   	return resolve(param)
		   }

		   // CHECK that movies and venues are defined correctly.
		   var Shows = [];
		   var log = [];
		   //var  = 0;
		   var maxIdRef;
		   console.log("show.find")
		   Show.find({}).select("idRef").exec()
			.then(function(data){
				//console.log(data)
				maxIdRef = check.maxShowRef(data)
				console.log("maxIdRef: " + maxIdRef)
				return (maxIdRef)
			})
			.then(function(maxIdRef){
				async.eachSeries(shows2Upload, function(show, callback){
					console.log("^^^^^^^^^^^^^^^^^^^^^^^")
					console.log("new SHow processed:" + show.startDay)

				  // Check if all details are there.
				  if (typeof show.movieId == "undefined" && show.movieId == "") {
				    var msg = "error movie id missing in show " + show.idRef
				    console.log(msg)
				    log.push(msg)
				    return callback()

				  }
				  if (typeof show.venueId == "undefined" && show.venueId == "") {
				    var msg = "error venue id missing in show " + show.idRef
				    console.log(msg)
				    log.push(msg)
				    return callback()
				  }

				  var showObj = new Show(show)
				  // Test if Duplicate SHows. Assume 1 movie per venue.
				  Show.find( {"$or": [ {startDay: showObj.startDay, venueId: showObj.venueId}, {idRef: showObj.idRef} ] })
				  .exec(function(err, res){
				    if (err) {
				      var msg = "error = " + err
				      log.push(msg)
				      console.log(msg)
				      callback()

				    } else if (res.length == 0) {
				      //Shows.push(show)
				      
				      maxIdRef = maxIdRef + 1
				      showObj.idRef = maxIdRef;
				      console.log("no duplicate - show added: " + show.startDay + " :" + show.movieTitle + " : " +showObj.idRef )

				      showObj.save(function (err, sh) {
				        if (err) {return console.error(err)};
				        console.log('==>> Show saved: ' + sh.startDay + " : " + sh.movieId)
				        callback();
				      });

				      //callback()

				    } else {
				      var msg = "Duplicate error: " + show.movieId + " - " + show.startDay
				      log.push(msg)
				      console.log(msg)
				      callback()

				    }
				  })
				}, function (){
				  
				  	if (log.length !== 0){
				    	fs.writeFile( path.join(__dirname, "../_log", "uploadShowsError.json"), JSON.stringify(log, null, 4), function(err){
				  			if (err) {
				  				return reject(err)
				  			}

				  			console.log("SHows uploaded - job done!!")
				  			param.log = param.log + "<br> \n" + 
				  			" *** SUMMARY uploadNewShows *** " + "<br> \n" +
							"new Shows Added: " + "oooo"

				  			resolve(param)
				  		});
				  	} else {
				  		resolve("SHows uploaded - job done!!")
				  	}
				});         
			});
		})
		.then(function(){
			console.log("done - End upload Show \n")
			return param
		})
		.catch(function(e){
	    	log.error("error Show.find "  + e)
	    });
			
		return promiseResult
	};
		
    if (!module.parent) {

		// CONNECT DB 
		mongoose.connect(config.MONGO_URI,function(err){
			if (err){
				console.error('MongoDB Connection Error. Please make sure that MongoDB is running.');
			} else{
				console.error('MongoDB Connected');
				upload.uploadNewShows({})
				.then(function(){
					console.log("done")
				})
				.catch(function(err){
					console.log(err)
				});

			}
		});

     //    //var bro = new Browser({waitDuration: 10*1000});

     //    // read arguments.
     //    if (process.argv.length < 3 ) {
     //    	console.log('missing last arguments. require cinema Name for functions cinema specific. Choices are:')
     //    	var files = fs.readdirSync('./cinemaJS')
     //    	files.forEach(function(val, index){
     //    		console.log(index + " " + val.slice(0,-3))
     //    	})
     //    	return
     //    } 
        
     //    if (process.argv.length == 3 ) {
     //    	// var cinema = process.argv[2]
     //    	// var dir = './cinemaJS/' + cinema + '.js'
     //    	// console.log('loading ' + dir)  
     //    	// var cinemaFunctions = require(dir)

	    //     // scraper.startProcess(bro, cinemaFunctions.getProgram, cinemaFunctions.readHtml, cinemaFunctions.cinema )
	    // }
    }
})();






       