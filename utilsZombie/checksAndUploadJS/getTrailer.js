#!/usr/bin/env node
//getTrailer new Movies

// var Browser = require("zombie");
var fs = require("fs");
var Promise = require('promise');
var async = require('async');
var request = require('request')
// var nock = require('nock');

var mongoose = require('mongoose');
var rp = require('request-promise');

//var AWS = require('aws-sdk');

//** IMPORT CONSTANT 
var config = require('../../config');

var check = require('./checkTools');

// Import MODEL
var model = require('../../model/ShowMoviesVenues');

(function(){
	var log = {};
	[
	 [ 'warn',  '\x1b[33m' ], // Yellow
	 [ 'info',  '\x1b[32m' ], // green
	 [ 'error', '\x1b[31m' ], // red
	 [ 'log',   '\x1b[0m'  ] //
	].forEach(function(pair) {
	  var method = pair[0], reset = '\x1b[0m', color = '\x1b[36m' + pair[1];
	  log[method] = console[method].bind(console, color, method.toUpperCase(), reset);
	});

   var getTrailer = {};

   exports.getTrailer = getTrailer.getTrailerTmdb = function(param){
    	console.log("")
    	log.info("getTrailer.js: findTrailerTmdb");

    	var Movie = model.Movie;
		
    	var pathFolder = param.pathFolder || "./"
    	// if (param == null) {
    	// 	param.jsonFile = 
    	// }

    	var mongooseState = mongoose.connection.readyState
    	console.log("moongooseDB: " + mongooseState)

    	if (mongooseState != 1 && mongooseState != 2){
			log.info("mongoDb not connected. starting connection....  | mongooseState = " + mongooseState + " " + typeof mongooseState )
			mongoose.connect(config.MONGO_URI,function(err){
				if (err){
					console.log('MongoDB Connection Error. Please make sure that MongoDB is running.' + err);
					//throw new Error(err)
				} else{
					log.info('MongoDB Connected');
				}
			});    	
		} else {
			console.log('MongoDB already Connected')
		}

    	var promiseResult = new Promise(function(resolve, reject){

	   	// find movie with no trailer and search tmdb and update movies.es
	    
	    	Movie.find({trailer: null})
	    	.select('imdbID Poster movieTitle img movieRef trailer')
	    	.exec(function(err,movies){
	    		if (err) { return reject(err)}
	        	log.warn("movies with no trailer: "+ movies.length)
	        	param.noTrailer = movies.length;
	        	var nbMovieAdded = 0;
	        	var trailermovieNotfind = 0;
	        	
	        	//limitation is 30 search per 10s 
	        	// add a 
	        	var tmdbRequest = function(mov, url){
	        		var promi = rp(url)
               .then(function(data){
	               console.log("=> " + url)
	               // if (typeof data.results == "undefined") { 
	               //   console.log(JSON.stringify(data))
	               //   return {status: "error"} 
	               // }
	               var dt = JSON.parse(data)
	               if (dt.results.length == 0) {
	                  console.log("no trailer for: " + mov.imdbID  + " | " + mov.movieTitle)
	                  return { trailer: null, title: null, doc: mov} 
	               } 
	               var trailer = dt.results[0].key
	               var title = dt.results[0].name
	                
	               mov.trailer = dt.results[0].key
	               mov.save(function(){
	                  console.log(trailer + " | " + title + " | " + mov._id + " | " + mov.imdbID)
	                  nbMovieAdded = nbMovieAdded + 1
	                  //console.log("^^^^^^^^^")
	                  return {trailer: trailer, title: title, doc: mov}
	               })
	            })
	            .then(null, function(err){
	            	console.log(err.message)
	            	return
	            })
	            // .catch(function(err){
	            //    console.error(err)
	            //    //console.log("^^^^^^^^^")
	            //    return {trailer: null, title: null, doc: mov}
	            // })

					return promi
				}

	        	var trailerPromiseFactory = function(mov){
	        		if (!mov.imdbID || mov.imdbID.slice(0,2) !="tt") {
						console.log("error mov.imdbID: "+ JSON.stringify(mov))
	        			return 
	        		}
	        		console.log("\n" + mov.movieTitle + " | " + mov.imdbID)
	        		var url = "http://api.themoviedb.org/3/movie/" + mov.imdbID + "/videos?external_source=imdb_id&api_key=ba604f19ae9c593275b33b242ac4bb10"	        		
	        		
	        		var wait = new Promise(function(resolve,reject){
	        			setTimeout(function(){ resolve()}, 1000);
	        		})

	        		return Promise.all([wait, tmdbRequest(mov, url)]) 
	        	};

	        	var prom = Promise.resolve()  //{arr : movies, results : []}
	        	movies.forEach(function(mov){
	        		prom = prom.then(function(){
	        			return trailerPromiseFactory(mov)
	        		})
	        	}) 
	        	prom = prom.then(function(res){
	        		param.getTrailer = nbMovieAdded
	        		param.log = param.log + "<br> \n" +
	        					"previously noTrailer" + param.noTrailer + "<br> \n" +  
	        					"nb Movie Trailer Added: "+ nbMovieAdded + "<br> \n" 

	        		return param
	        	});
	        	
	        	return resolve(prom)
	      })
	   })
	   .catch(function(e){
	    	log.error("error Movie.find "  + e)
	   });

		return promiseResult
	};

		
   if (!module.parent) {

		getTrailer.getTrailerTmdb({pathFolder: "../", log: ""})
		.then(function(re){
			console.log("Done getTrailer: " + re.getTrailer)
			process.exit()
		})
		.catch(function(e){
			console.log("err " + e)
			process.exit()
		});

   }
})();




//Limitation of free API. 
// Change to series 


				// var trailerPromises = []
				// var trailerPromiseFactory = function(mov){
				// 	// get infos from TMDB
			 //      // if info save Movie
			 //      ////////////////////
				// 	var url = "http://api.themoviedb.org/3/movie/" + mov.imdbID + "/videos?external_source=imdb_id&api_key=ba604f19ae9c593275b33b242ac4bb10"

				// 	var prom = rp(url)
    //            .then(function(data){
	   //             console.log("=> " + url)
	   //             // if (typeof data.results == "undefined") { 
	   //             //   console.log(JSON.stringify(data))
	   //             //   return {status: "error"} 
	   //             // }
	   //             var dt = JSON.parse(data)
	   //             if (dt.results.length == 0) {
	   //                console.log("no trailer for: " + mov.imdbID  + " | " + mov.Title)
	   //                return {trailer: null, title: null, doc: mov} 
	   //             } 
	   //             var trailer = dt.results[0].key
	   //             var title = dt.results[0].name
	                
	   //             mov.trailer = dt.results[0].key
	   //             mov.save(function(){
	   //                console.log(trailer + " | " + title + " | " + mov._id + " | " + mov.imdbID)
	   //                //console.log("^^^^^^^^^")
	   //                return {trailer: trailer, title: title, doc: mov}
	   //             })
	   //          })
	   //          .catch(function(err){
	   //             console.error(err)
	   //             //console.log("^^^^^^^^^")
	   //             return {trailer: null, title: null, doc: mov}
	   //          })

				// 	return prom
				// }

				// for (var i = 0; i < movies.length; i++){
				// //for (var i = 0; i < 2; i++){
	   //      		trailerPromises.push(trailerPromiseFactory(movies[i]))
	   //      	}	        

				// Promise.all(trailerPromises)
				// .then(function(resu){
				// 	console.log("ok")
				// 	resolve(resu.length)
				// })
				// .catch(function(){
				// 	reject()
				// })


//
// EXAMPKE OF EQUENCE
// Start off with a promise that always resolves
// var sequence = Promise.resolve();

// // Loop through our chapter urls
// story.chapterUrls.forEach(function(chapterUrl) {
//   // Add these actions to the end of the sequence
//   sequence = sequence.then(function() {
//     return getJSON(chapterUrl);
//   }).then(function(chapter) {
//     addHtmlToPage(chapter.html);
//   });
// });

///

       