#!/usr/bin/env node
//Check Scraped Shows

var fs = require("fs");
var Promise = require('promise');
var async = require('async');
var request = require('request')

var mongoose = require('mongoose');
var path = require('path');


var check = require('./checkTools');


//** IMPORT CONSTANT 
var config = require('../../config');

// Import MODEL
var model = require('../../model/ShowMoviesVenues');

(function(){
	var log = {};
	[
		[ 'warn',  '\x1b[33m' ], // Yellow
		[ 'info',  '\x1b[32m' ], // green
		[ 'error', '\x1b[31m' ], // red
		[ 'log',   '\x1b[0m'  ] //
	].forEach(function(pair) {
	  var method = pair[0], reset = '\x1b[0m', color = '\x1b[36m' + pair[1];
	  log[method] = console[method].bind(console, color, method.toUpperCase(), reset);
	});

   var checkShows = {};


   exports.checkScrapedShows = checkShows.checkScrapedShows = function(paramObj){
    	console.log("")
    	log.info("check Scraped Shows")
		var read = Promise.denodeify(fs.readFile)
		var write = Promise.denodeify(fs.writeFile)

    	var Movie = model.Movie;
    	var Show = model.Show;
    	
    	var jsonFile = paramObj.jsonFile
    	//var pathFolder = paramObj.pathFolder || "./"

    	var mongooseState = mongoose.connection.readyState
    	console.log("moongooseDB: " + mongooseState)
    	
    	if (mongooseState != 1 && mongooseState != 2){
			log.info("mongoDb not connected. starting connection....  | mongooseState = " + mongooseState + " " + typeof mongooseState )
			mongoose.connect(config.MONGO_URI,function(err){
				if (err){
					console.log('MongoDB Connection Error. Please make sure that MongoDB is running.' + err);
					//throw new Error(err)
				} else{
					log.info('MongoDB Connected');
				}
			});    	
		} else {
			console.log('MongoDB already Connected')
		} 

    	var promiseResult = new Promise(function(resolve, reject){

    		//CHECK if jsonFile is Valid
		   if (typeof jsonFile == "undefined") {
		      return reject("missing param file = ....json")
		   }

		   console.log("// try  reading json file")
		   var jsonFileFull = path.join(__dirname, '../scrapeJSON', jsonFile + '.json' ) ;

		   try {
			   fs.accessSync(jsonFileFull, fs.F_OK);
			   console.log("reading: " + jsonFileFull)
		    	var ShowsJson = JSON.parse(fs.readFileSync(jsonFileFull, 'utf8'));
			} catch (e) {
				console.log("error reading json file "  + jsonFileFull)
				return reject("error reading json file")
			}

		   // CHECK if jsonFile_extra exist
		   var addFile = false;
		   var addFileFull = path.join(__dirname, '../scrapeJSON', jsonFile + '_extra.json' ) ;
		   console.log("// try  reading extra file")
		   try {
			   fs.accessSync(jsonFileFull, fs.F_OK);
			   var extraDetails = JSON.parse(fs.readFileSync(addFileFull, 'utf8'));
		    	addFile = true;
		    	console.log("extraDetails Exist. reading " + addFileFull + " | length: " + extraDetails.length)
			} catch (e) {
			   // It isn't accessible
			   try{
			   	
			   	fs.writeFileSync(addFileFull, "[ ]")
			   	console.log("no extra file - creating file: file created")
			   } catch (e){
			   	throw new Error('cannot create ' + addFileFull)
			   }
			}

		   var shows2Upload = []
		   var movies2Upload = []
		   var logFile = {errors : [], duplicates: []};
		   var logExtraDetails = [];
		   logExtraDetails = extraDetails
		    //check if Movie.OMDB // Movie identify in the DB
		    // check if Movie.imdbId // movie identified manually.
		    // If Not, 
		    //   check for title in mongoose
		    //   if found => get movieId and add to Show
		    // else check for title in MovieDB
		    //   if found => get movie details and add to Show.movieIMDB + write in logFile
		    //   else write in Log file Not found.

		   function daydiff(first, second) {
		   	//define if 2 dates have the same day
		    	//first and second are dates in 
		      return Math.abs(Math.round((second-first)/(1000*60*60*24)));
		   }

		   //check if Show //Movie already in database
		   async.eachSeries(ShowsJson, function(show, callback){
		      console.log("^^^^^^^^^^^^^^^^^^^^^ ")
		      console.log("Check if show already in database, movie: " + show.movieTitle)

		      //test case for fucking -- F**king - replace * with --
		      show.movieTitle = show.movieTitle.replace(/\*/gi, '-')
		      show.movieTitle = show.movieTitle.trim();
		      
		      //search extra details for more infos:
		      if(addFile) {
		        	var index = extraDetails.map(function(e) { return e.movieTitle; }).indexOf(show.movieTitle)
		        	if ( index > -1 ) {
		          	show.movieId = extraDetails[index].movieId;
		          	show.movieIMDB = extraDetails[index].movieIMDB;
		          	console.log("added details : " + show.movieId + " | " + show.movieIMDB)
		        	};
		      } 

		      //check if show is already in Database:
		      var showObjTmp = new Show(show)
		      // Test if Duplicate SHows in dates. Assume 1 movie per venue.
		      showObjTmp.startDay.setHours(0)
		      showObjTmp.endDay = new Date(show.startDay)
		      showObjTmp.endDay.setHours(23)
		      console.log(" startDay: " + new Date(show.startDay))
		      console.log(" startDay: " + showObjTmp.startDay)
		      console.log("   endDay: " + showObjTmp.endDay)

		      Show.find({startDay: {"$gte": showObjTmp.startDay , "$lt": showObjTmp.endDay}, venueId: showObjTmp.venueId})
		     	.exec(function(err, res){
		        	if (err) {
		          	var msg = "error = " + err
		          	logFile.errors.push(msg)
		          	console.log(msg)
		          	return callback()

		        	} else if (res.length == 0) {
		          	console.log("=> Show is unique")
		        	} else {
			         var msg = "Duplicate Show: " + show.movieTitle + " - " + show.startDay
			         show.duplicate = true
			         logFile.duplicates.push(msg)
			         console.log(msg) 
		        	}
		      }).then(function(){
		        	if (show.duplicate) { 
		          	//console.log("duplicate")
		          	return callback() 
		        	}

		        	if (!show.movieId){
		        		console.log("// Search for Movie Id")
			         if (typeof show.movieDirector =="undefined") { show.movieDirector = "-" }
			         if (typeof show.parentalRating =="undefined") { show.parentalRating = "" }
			         if (typeof show.endDay =="undefined") { show.endDay = show.startDay }

		          	var criteria1 = new RegExp("^"+ show.movieTitle +"$","i")
		          	var criteria2 = "cantmacthAnything"; 
		          	if (typeof show.movieIMDB != "undefined" && show.movieIMDB != "") {criteria2 = show.movieIMDB };
		          	console.log("searh criteria " + criteria1 + "  " +  criteria2)
		          	Movie.find( {"$or": [ {Title: criteria1}, { movieTitle: criteria1}, {imdbID: criteria2} ] } )
		            .exec(function(err, mov){
							if (err){ 
							 	console.log(" -error with " + show.movieTitle + " => " + err)
							 	logFile.errors.push(" -error with " + show.movieTitle + " => " + err)
							 	logExtraDetails.push({movieTitle: show.movieTitle , movieIMDB: "", movieId: ""})              

							 	return callback();
							}
		              	if (mov.length == 1) { //find only one = good.!!
		                	show.movieId = mov[0]._id 
		                
		                	console.log(" -found: "+ mov[0].movieTitle + "  - " +  show.movieId)

		                	var first = new Date(show.startDay)
		                	var second = new Date(show.endDay)

		                	//Multiple Shows if start date diff de end date
		                	var diff = daydiff(first, second)
		                	console.log("nb of multiple show: "+ diff + " | " + first + " | "+ second)
		                	for (var i = 0; i <= diff; i++) {
			                  //var tmp = JSON.parse(JSON.stringify(show));
			                  var newfirst = new Date(first.toGMTString())
			                  //console.log("===*** "+ newfirst + "  " + first.getDate() + " " + i)
			                  newfirst.setDate(first.getDate() + i)
			                  newfirst.setHours(19)
			                  //console.log("===*** "+ newfirst + "  " + first.getDate() + " " + i)
			                  show.startDay = newfirst.toString()
			                  show.endDay = show.startDay
			                  console.log("==> Show generated: "+ show.startDay + " / "+ show.endDay)
			                  shows2Upload.push(JSON.parse(JSON.stringify(show)))
			               }


		               	return callback();

		              	} else if (mov.length > 1) { //find 2 possible issue
		                	console.log(" -Find Duplicate match for: " + show.movieTitle + " -- movie Director: " + show.movieDirector) 
		                	for (var i=0; i < mov.length; i++) {
		                  	if (i == 0) (logFile.duplicates.push( { "$oid": mov[i]._id }))
		                  	console.log(" --" + mov.length +  " | "+ i + " " + mov[i].movieTitle + " | " + mov[i]._id)
		                  	if (mov[i].Director == show.movieDirector){
		                    	console.log("identify with Director: " + mov[i]._id )
		                  	}

		                	};
		               	return callback();

		              } else { //didn't find any.              
		                	if (typeof show.movieIMDB != "undefined" && show.movieIMDB != "") { // manually identified.
			                  var url1 = 'http://www.omdbapi.com/?i=' + show.movieIMDB +'&y=&plot=full&r=json'
			               } else {
			                  var url1 = 'http://www.omdbapi.com/?t=' + encodeURIComponent(show.movieTitle)+'&y=&plot=full&r=json'
			               }
		                	request(url1 , function (error, response, json) {
			                  console.log('start request OMDBAPI: ' + url1);
			                  if (!error){
		                        var obj = JSON.parse(json)
		                        if (obj.Error){
			                        var msg = 'OMDB: error movie getting info for: '+ show.movieTitle
			                        console.log(msg)
			                        logFile.errors.push(msg)
			                        logExtraDetails.push({movieTitle: show.movieTitle.trim() , movieIMDB: "", movieId: ""})
			                        callback();

		                        } else {
		                        	if (typeof show.movieIMDB != "undefined" && show.movieIMDB != "") {
			                          	obj.movieTitle = show.movieTitle;
			                          	obj.parentalRating = show.parentalRating;
			                          	obj.trailer = show.trailer;
			                          	movies2Upload.push(obj)
			                          	console.log("OMDB done")
			                          	return callback();
		                        	
		                        	} else {
		                        		if	( (show.movieDirector.toLowerCase() !="-") &&
		                        			  (obj.Director.toLowerCase() != show.movieDirector.toLowerCase()) ) {
		                            	
			                            	var msg = "Warning Director don't match: " + obj.Director.toLowerCase()  + " | It should be: " + show.movieDirector.toLowerCase() + " | movie: " + show.movieTitle
			                            	console.log(msg)
			                            	logFile.errors.push(msg)
			                            	logExtraDetails.push({movieTitle: show.movieTitle.trim() , movieIMDB: "", movieId: ""})

			                            	if ( obj.Title.toLowerCase() != show.movieTitle.toLowerCase()) {
			                              	var msg = "title don't match " + obj.Title + ":  ||| " + show.movieTitle +":"
			                              	console.log(msg)
			                              	logFile.errors.push(msg)
			                              	logExtraDetails.push({movieTitle: show.movieTitle.trim() , movieIMDB: "", movieId: ""})
			                            	}
			                            	
			                            	return callback();
			                          	}
												else if ( (show.movieDirector.toLowerCase() !="-") &&
		                        			  (obj.Director.toLowerCase() == show.movieDirector.toLowerCase()) ) {
														
														console.log("find correct movie. Director match")
														obj.movieTitle = show.movieTitle;
					                          	obj.parentalRating = show.parentalRating;
					                          	obj.trailer = show.trailer;
					                          	movies2Upload.push(obj)
					                          	console.log("OMDB done")
					                          	return callback();
												}
			                          	else if(show.description && show.description != "undefined" ){
			                          		console.log("Director not present - searching description for match")

			                          		//director search
			                          		var re1 = new RegExp(obj.Director.replace(", ","|"),"i")
			                          		var search = show.description.search(re1)

			                          		if (search > -1) {
			                          			//find director name in description

														log.info("find director name in description")
														obj.movieTitle = show.movieTitle;
					                          	obj.parentalRating = show.parentalRating;
					                          	obj.trailer = show.trailer;
					                          	movies2Upload.push(obj)
					                          	console.log("OMDB done")
					                          	return callback();

			                          		}
			                          		// actors search
			                          		var re2 = new RegExp(obj.Actors.replace(", ","|"),"i")
			                          		var search2 = show.description.search(re2)
			                          		
			                          		if (search2 > -1) {
			                          			//find director name in description

														log.info("find Actors name in description")
														obj.movieTitle = show.movieTitle;
					                          	obj.parentalRating = show.parentalRating;
					                          	obj.trailer = show.trailer;
					                          	movies2Upload.push(obj)
					                          	console.log("OMDB done")
					                          	return callback();

			                          		}
			                          		
		                          			console.log("not sure: " + show.movieDirector + "\n" + show.description)
		                          			logExtraDetails.push({movieTitle: show.movieTitle.trim() , movieIMDB: "", movieId: ""})
		                          			return callback();
			                          		

			                          	} else {
			                          		console.log("not sure: " + show.movieDirector + "\n" + show.description)
			                    				logExtraDetails.push({movieTitle: show.movieTitle.trim() , movieIMDB: "", movieId: ""})
			                          		return callback();
			                          	}
		                        	}
		                     	}
			                  } else {
			                    console.log('OMDB: error movie getting info for: '+ show.movieTitle)
			                    logExtraDetails.push({movieTitle: show.movieTitle.trim() , movieIMDB: "", movieId: ""})
			                    callback();
			                  }
		                	});
		              	}
		            })
		        	} 
		        	else {
			         // create show
			         console.log("// Show has movieId: "+ show.movieId)

			         var first = new Date(show.startDay)
			         var second = new Date(show.endDay)

			         //Multiple Shows if start date diff de end date
			         var diff = daydiff(first, second)
			         console.log(diff + " | " + first + " | "+ second)
		          	for (var i = 0; i <= diff; i++) {
			            //var tmp = JSON.parse(JSON.stringify(show));
			            var newfirst = new Date(first.toGMTString())
			            //console.log("===*** "+ newfirst + "  " + first.getDate() + " " + i)
			            newfirst.setDate(first.getDate() + i)
			            newfirst.setHours(19)
			            //console.log("===*** "+ newfirst + "  " + first.getDate() + " " + i)
			            show.startDay = newfirst.toString()
			            show.endDay = show.startDay
			            console.log("==> "+ show.startDay + " / "+ show.endDay)
			            shows2Upload.push(JSON.parse(JSON.stringify(show)))
			         }
		          	callback();
		        	}
		      })

		   }, function(){

		      console.log( "\n *** SUMMARY *** " )
		      var msg = [];
		      msg.push('Error recorded (log.json): ' + logFile.errors.length);
		      msg.push('Duplicates (log.json): ' + logFile.duplicates.length);

		      msg.push('movies to Upload (movies2Upload.json): ' + movies2Upload.length)
		      msg.push('shows2Upload.json: ' + shows2Upload.length)
		      msg.push ("shows scraped: "+ ShowsJson.length)

		      // fs.writeFile('../log.json', JSON.stringify(log, null, 4), function(err){
		      //   console.log('Error recorded (log.json): ' + log.length);
		      // });
		      // fs.writeFile('../logExtraDetails.json', JSON.stringify(logExtraDetails, null, 4), function(err){
		      //   console.log('created logExtraDetails.json ' + logExtraDetails.length);
		      // });

		      // fs.writeFile('../movies2Upload.json', JSON.stringify(movies2Upload, null, 4), function(err){ 
		      //   console.log('movies to Upload (movies2Upload.json): ' + movies2Upload.length);
		      // });

		      // // write updated file.
		      // fs.writeFile('../shows2Upload.json', JSON.stringify(shows2Upload, null, 4), function(err){
		      //   console.log('shows2Upload.json: ' + shows2Upload.length );
		      // });
		      
		       
		      write( path.join(__dirname, '../_log', 'log.json'), JSON.stringify(logFile, null, 4))
		      .then(function(){
		      	console.log('Error recorded (log.json): ' + logFile.errors.length);
		      	console.log('Duplicates recorded (log.json): ' + logFile.duplicates.length);
		      	return write(addFileFull, JSON.stringify(logExtraDetails, null, 4))
		      })
		      .then(function(){
		      	console.log('created logExtraDetails.json ' + logExtraDetails.length);
		      	return write( path.join(__dirname, '../movies2Upload.json'), JSON.stringify(movies2Upload, null, 4))
		      })
				.then(function(){
					console.log('movies to Upload (../movies2Upload.json): ' + movies2Upload.length);
		      	return write(path.join(__dirname, '../shows2Upload.json'), JSON.stringify(shows2Upload, null, 4))
		      })
		      .then(function(){
		      	console.log('shows to Upload (shows2Upload.json): ' + shows2Upload.length );
		      	console.log('shows checked: '+ ShowsJson.length )
		      	paramObj.log = paramObj.log + "<br> \n" + 
		      		" *** SUMMARY checkScrapedShows *** " + "<br> \n" +
						"Error recorded (log.json): " + logFile.errors.length + "<br> \n" +
		      		"Duplicates recorded (log.json): " + logFile.duplicates.length + "<br> \n" +
		      		"movies to Upload (movies2Upload.json): " + movies2Upload.length + "<br> \n" +
		      		"shows to Upload (shows2Upload.json): " + shows2Upload.length + "<br> \n" +
		      		"shows scraped: " + ShowsJson.length

		      	resolve(paramObj)
		      })
				.catch(function(err){
		      	console.log("error writing file " + err)
					reject(err)
				})

		   }); //async
		});
				
		return promiseResult
	};
		
   if (!module.parent) {

	  	// read arguments.
	  	if (process.argv.length < 3 ) {
	  		console.log('missing last arguments. require jsonFile Name')
	  		return
	  	} 
	  
	  	if (process.argv.length == 3 ) {
	  		var jsonFile = process.argv[2]
	  		// CONNECT DB 
			mongoose.connect(config.MONGO_URI,function(err){
				if (err){
					console.error('MongoDB Connection Error. Please make sure that MongoDB is running.');
				} else{
					console.log('MongoDB Connected');
					checkShows.checkScrapedShows({jsonFile: jsonFile, pathFolder: "../", log:""}).then(function(re){
						console.log("done")
					})
					.catch(function(err){
						console.log("error "+ err)
					});
				}
			});
	 	}

	 	if (process.argv.length > 3 ) {
	 		console.log("too many args ; last arg is:" + process.argv[2] )
	 		return
	 	}
	}
})();






       