var Browser = require("zombie");
var fs = require("fs");
var Promise = require('promise');
var async = require('async');
var request = require('request')
//var nock = require('nock');
var path = require('path');

console.log("scraping: zombie")

// nock('https://tixframe.rooftopmovies.com.au/ticketing/')
//   //.get(/.*/)
//   .get('/SessionCapture.aspx')
//   .query(true)
//   .reply(200, 'ok')
 

var colors = require("colors");
 
// function makeColorConsole(fct, color){
//   return function(){
//     for (var i in arguments)
//       if (arguments[i] instanceof Object)
//         arguments[i] = sys.inspect(arguments[i]);
//     fct(Array.prototype.join.call(arguments," ")[color]);
//   };
// }
 
//console.warn = makeColorConsole(console.warn, "yellow");
//console.error = makeColorConsole(console.error, "yellow");

var log = {};
[
 [ 'warn',  '\x1b[33m' ], // Yellow
 [ 'info',  '\x1b[32m' ], // green
 [ 'error', '\x1b[31m' ], // red
 [ 'log',   '\x1b[0m'  ] //
].forEach(function(pair) {
  var method = pair[0], reset = '\x1b[0m', color = '\x1b[36m' + pair[1];
  log[method] = console[method].bind(console, color, method.toUpperCase(), reset);
});

var cycleCinema = require('./cycleCinema.js').startCycle

var d = new Date()
var today = d.getFullYear()+"-"+ (d.getMonth()+1) + "-"+  d.getDate()

var browser = new Browser({waitDuration: 10*1000});

//var cinema = "rooftop"

exports.zombieScrape = function(req, res){
	console.log('')
	log.info('zombie.js: ZombieScrape starting')
	
	//get params
	var params = req.query
	console.log("zombie params: " + JSON.stringify(params))
	
	var fileDir = path.join(__dirname, 'cinemaJS'); //console.log(fileDir)
	var files = fs.readdirSync(fileDir)

	// check param

	// test if parameter is missing
	if (Object.keys(params).length == 0) {
		console.log ("no cinema define. Please choose cinema as follow:")
		var st = ""
		files.forEach(function(val, index){
     		console.log(index + " " + val.slice(0,-3))
     		st = st + "<br>" + index + " " + val.slice(0,-3)
   	})
		return res.send("no cinema define. Please choose cinema as follow: <br>" + st)
	}
	else if (params.all){
		console.log('run all venues')
		var prom = Promise.resolve(1)

		files.forEach(function(val, index){
			var key = val.slice(0,-3)
			console.log("adding " + key + " to process")

    		var promiseWrap = function(){
    			return cycleCinema(key)
    		}

 			prom = prom.then(promiseWrap)

   	})
	} 
	else {
		var prom = Promise.resolve(1)
		for(var key in params) { 
			if (params.hasOwnProperty(key)) {
	       	//var cinema = params[key];
	       	if (files.indexOf(key+".js") != -1) {
	       		console.log("adding " + key + " to process")

	       		var promiseWrap = function(){
		    			return cycleCinema(key)
		    		}

	    			prom = prom.then(promiseWrap)
	       	} 
	       	else {
	       		console.log(key  +" does not exist - ignored")
	       	}
		   }
		}
	}
	
	if (prom) {
		res.send("zombie.js: processing cinemas "+ JSON.stringify(params))
		prom = prom.then(function(){
			console.log('\n zombie.js: done all cinema')
		})
		.catch(function(e){
			console.log(e)
			//res.send(e)
		})
	} else {
		console.log("zombie.je: not sure")
		res.send("not sure")
	}
}



// colors.setTheme({
//   silly: 'rainbow',
//   input: 'grey',
//   verbose: 'cyan',
//   prompt: 'grey',
//   info: 'green',
//   data: 'grey',
//   help: 'cyan',
//   warn: 'yellow',
//   debug: 'blue',
//   error: ['yellow', 'underline']
// });
 
// // outputs red text 
// console.log("this is an error".error);
 
// // outputs yellow text 
// console.log("this is a warning".warn);

// switch (cinema){
// 	case: "rooftop"
// 		scrapeRooftop.startProcess(browser)
// 		.then(function(res){
// 			console.log('end' + res.urls.length)
// 		})
// 		break;

// 	case: "moonlight"
// 		scrapeMoonlight.startProcess(browser)
// 		.then(function(res){
// 			console.log('end' + res.urls.length)
// 		})
// 		break;
	
// 	case: "moonlight"
// 		scrapeMoonlight.startProcess(browser)
// 		.then(function(res){
// 			console.log('end' + res.urls.length)
// 		})
// 		break;
	
// 	case: "moonlight"
// 		scrapeMoonlight.startProcess(browser)
// 		.then(function(res){
// 			console.log('end' + res.urls.length)
// 		})
// 		break;
	
// 	case: "moonlight"
// 		scrapeMoonlight.startProcess(browser)
// 		.then(function(res){
// 			console.log('end' + res.urls.length)
// 		})
// 		break;
	
// 	case: "moonlight"
// 		scrapeMoonlight.startProcess(browser)
// 		.then(function(res){
// 			console.log('end' + res.urls.length)
// 		})
// 		break;		
// }