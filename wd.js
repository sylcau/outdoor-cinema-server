var log = [];
var fs = require('fs'); 
var async = require('async')

var options = {
    desiredCapabilities: {
        browserName: 'firefox'
    }
};
var webdriverio = require('webdriverio');

var shows = [];
var Urls = [];



function checkRating(string){
    if (typeof string != "string") { return "Error"}
    if (string.search(/\(G/) > -1 ) { return "G"}
    if (string.search(/GENERAL ADMISSION/i) > -1 ) { return "G"}
    

    if (string.search(/\(PG/) > -1 ) { return "PG"}
    if (string.search(/PARENTAL GUIDENCE/i) > -1 ) { return "PG"}
    

    if (string.search(/\(M\)/) > -1 ) { return "M"}
    if (string.search(/\(CTC/) > -1 ) { return "CTC"}
    if (string.search(/\(MA15+/) > -1 ) { return "MA15+"}
    if (string.search(/\(R18+/) > -1 ) { return "R18+"}

    if (string.search("Private Screening") > -1 ) { return ("-")}

    return "CTC"
}

function checkYear(string){
    if (typeof string != "string") { return " Error - " + typeof string}
    if (string.search(/OCT/i) > -1 ) { return " 2015"}
    if (string.search(/NOV/i) > -1 ) { return " 2015"}
    if (string.search(/DEC/i) > -1 ) { return " 2015"}
    if (string.search(/JAN/i) > -1 ) { return " 2016"}
    if (string.search(/FEB/i) > -1 ) { return " 2016"}
    if (string.search(/MAR/i) > -1 ) { return " 2016"}

    return " Error"
}

var d = new Date()
var today = d.getFullYear()+"-"+ (d.getMonth()+1) + "-"+  d.getDate() 


var cinema = "lotterywest"

if (cinema == "lotterywest" ) {
    var sep = "";
    //var filePath = './utils/scrapedWeb-JSON/scrapeLotterywest_' + today +'.json'
    var filePath = './utils/scrapedWeb-JSON/scrapeLotterywest.json'

    fs.writeFile(filePath, "[", function(err){
        //console.log('File successfully written! - Check your project directory for the log.json file');
    });
    webdriverio
        .remote(options)
        .init()

        //option1
        .url('https://perthfestival.com.au/lotterywest-festival-films/whats-on/#/')
        //.getHTML('.teal')
        .getAttribute('#results a', 'href')
        .then(function(attr){
            console.log("attr.length "+ attr.length);
            //console.log(JSON.stringify(attr))
            Urls = attr
        })
        .end()
        .then(function(){
            //attr.slice(attr.length-1)
            //attr = ['https://perthfestival.com.au/lotterywest-festival-films/whats-on/2016/queen-and-country/']
            async.forEachSeries(Urls, function(url, callback){
                console.log("url "+ url)
                var show = {}           
                //var regExp;
                console.log("========")

                var client = webdriverio.remote(options)

                client.addCommand("getTextNoError", function(selector) {
                    return this.elements(selector).then(function(res) {

                        if(!res.value || res.value.length === 0) {
                            // throw NoSuchElement error if no element was found
                            //throw new ErrorHandler(7);
                            return ""
                        }

                        var self = this,
                            elementIdTextCommands = [];

                        res.value.forEach(function(elem) {
                            elementIdTextCommands.push(self.elementIdText(elem.ELEMENT));
                        });

                        return this.unify(elementIdTextCommands, {
                            extractValue: true
                        });

                    });
                });

                client
                    .init()
                    .url(url)

                    .getTextNoError('.hero-card__title').then(function(text){ //return Movie title 
                        console.log("=>" + text)
                        show.movieTitle = text 
                    })
                    .getTextNoError('.sticky-footer__event span:nth-of-type(1)').then(function(text){ //return dates 
                        console.log("=>" + text)
                        if( text.match("UWA Somerville")) {show.somerDates = text} else {show.joonDates = text}
                    })
                    .getTextNoError('.sticky-footer__event span:nth-of-type(2)').then(function(text){ //return dates  
                        console.log("=>" + text)
                        if( text.match("UWA Somerville")) {show.somerDates = text} 
                        else if (text.match("ECU Joondalup Pines")) {show.joonDates = text}
                    })
                    .getAttribute('/html/body/div[2]/div[1]/div[2]/div[2]/h1/a', 'href').then(function(text){
                        console.log("=>" + text)
                        show.link = text
                    })
                    .getTextNoError('.hero-card__notes*=,').then(function(text){ //return PRating 
                        console.log("=>" + text)
                        var regExp = /[^,]*/;
                        show.parentalRating = regExp.exec(text)[0] 
                    })
                    .getTextNoError('.hero-card__notes*=Director').then(function(text){ //return Director 
                        console.log("=>" + text)
                        show.movieDirector = text.slice(9) 
                    })   ///html/body/div[2]/div[1]/div[2]/div[2]/p[5] /html/body/div[2]/div[1]/div[2]/div[2]/p[6]

                    .then(function(){
                        var tmp = {};
                        var tmp1 = {}
                        if (typeof show.somerDates != "undefined"){
                            tmp.somerDates = show.somerDates
                            tmp.movieTitle = show.movieTitle 
                            tmp.venueId = "554ed6bed617526c16816cf2" //"SOMERVILLE"
                            tmp.dates = show.somerDates.slice(15)

                            var reg = /[^–]+$/
                            tmp.startDay = new Date( reg.exec(tmp.dates)[0] )
                            tmp.endDay = new Date(reg.exec(tmp.dates)[0])
                            tmp.startDay.setDate(tmp.endDay.getDate()-6);
                            tmp.parentalRating = show.parentalRating
                            tmp.movieDirector = show.movieDirector

                            
                            if (new Date(show.startDay) > new Date("6 March 2016")){
                                tmp.movieStart = "7:30 PM"
                            } else {
                                tmp.movieStart = "8:00 PM"
                            }

                            tmp.gateOpen = "6:00 PM"
                            
                            tmp.language = ""
                            tmp.subtitle = ""
                            //delete(show.somerDates)


                            //Door sales from 6pm nightly.
                            //Picnickers are welcome from 6pm but must be seated by 8pm (7.30pm from 7 March 2016)

                            shows.push(tmp)
                            fs.appendFile(filePath, sep + JSON.stringify(tmp, null, 4), function(err){
                                 //console.log('File successfully written! - Check your project directory for the log.json file');
                            });
                            if (!sep) sep = ",\n";
                        }
                        if (typeof show.joonDates != "undefined"){
                            
                            tmp1.somerDates = show.joonDates
                            tmp1.movieTitle = show.movieTitle 
                            tmp1.venueId = "554ed6bbd617526c16816ce8" //"JOONDALUP"
                            tmp1.dates = show.joonDates.slice(20)
                            
                            var reg = /[^–]+$/
                            tmp1.startDay = new Date( reg.exec(tmp1.dates)[0] )
                            tmp1.endDay = new Date(reg.exec(tmp1.dates)[0])
                            tmp1.startDay.setDate(tmp1.endDay.getDate()-5);
                            tmp1.parentalRating = show.parentalRating
                            tmp1.movieDirector = show.movieDirector

                            if (new Date(show.startDay) > new Date("6 March 2016")){
                                tmp1.movieStart = "7:30 PM"
                            } else {
                                tmp1.movieStart = "8:00 PM"
                            }

                            tmp1.gateOpen = "6:30 PM"
                            
                            tmp1.language = ""
                            tmp1.subtitle = ""
                            shows.push(tmp1)

                            fs.appendFile(filePath, sep + JSON.stringify(tmp1, null, 4), function(err){
                                 //console.log('File successfully written! - Check your project directory for the log.json file');
                            });
                            if (!sep) sep = ",\n";
                        }

                        callback()
                    })
                    .end()
                //callback()
            }, function(){
                console.log(shows.length + " ================= " + filePath)
                fs.appendFile(filePath,"]", function(err){
                            //console.log('File successfully written! - Check your project directory for the log.json file');
                });
            })
        })
}

if (cinema == "moonlight" ) {
    console.log("Moonlight")
    var sep = "";
    var filePath = './utils/scrapedWeb-JSON/scrapeMoonlight_' + today +'.json'
    fs.writeFile(filePath, "[", function(err){
        //console.log('File successfully written! - Check your project directory for the log.json file');
    });


    webdriverio
        .remote(options)
        .init()

        //main program
        .url('https://www.moonlight.com.au/perth/program/')
        .getAttribute('div.span7.desc > ul > li > a', 'href')
        .then(function(attr){
            console.log("attr.length "+ attr.length);
            Urls = Urls.concat(attr)
            Urls.forEach(function(url){
                console.log(url)
            })
        })
        .url('https://www.moonlight.com.au/perth/program/page/2/')
        .getAttribute('div.span7.desc > ul > li > a', 'href')
        .then(function(attr){
            console.log("attr.length "+ attr.length);
            Urls = Urls.concat(attr)
            Urls.forEach(function(url){
                console.log(url)
            })
        })
        .url('https://www.moonlight.com.au/perth/program/page/3/')
        .getAttribute('div.span7.desc > ul > li > a', 'href')
        .then(function(attr){
            console.log("attr.length "+ attr.length);
            Urls = Urls.concat(attr)
            Urls.forEach(function(url){
                console.log(url)
            })
        })
        .end()
        .then(function(){
            console.log(" ==> nb Urls: " + Urls.length)
            //attr.slice(attr.length-1)
            //Urls = ['https://www.moonlight.com.au/perth/movie/the-intern-3/', 'https://www.moonlight.com.au/perth/movie/suffragette-3/']
            async.forEachSeries(Urls, function(url, callback){
                console.log("========")
                console.log("url "+ url)
                var show = {}           

                var client = webdriverio.remote(options)

                client.addCommand("getTextNoError", function(selector) {
                    return this.elements(selector).then(function(res) {

                        if(!res.value || res.value.length === 0) {
                            // throw NoSuchElement error if no element was found
                            //throw new ErrorHandler(7);
                            return ""
                        }

                        var self = this,
                            elementIdTextCommands = [];

                        res.value.forEach(function(elem) {
                            elementIdTextCommands.push(self.elementIdText(elem.ELEMENT));
                        });

                        return this.unify(elementIdTextCommands, {
                            extractValue: true
                        });

                    });
                });

                client
                    .init()
                    .url(url)

                    //Title
                    .getTextNoError('#content > div.row-fluid > div.span10 > div.row-fluid > div.span7.content-area.movie-detail > h1').then(function(text){ //return Movie title 
                        console.log("=>" + text)
                        show.movieTitle = text 
                    })

                    //Date
                    .getTextNoError('#content div.span7.content-area.movie-detail div div dl dd').then(function(text){ //return dates 
                        console.log("=>" + text[0] + " WARNING YEAR IS MANUAL")
                        show.Date = text[0]
                        var year = checkYear(show.Date)
                        show.startDay = new Date(text[0] + year);
                        show.endDay = show.startDay;

                    })

                    //Rating
                    .getTextNoError('#content dd .rating').then(function(text){ //return PRating 
                        console.log("=>" + text)
                        //var regExp = /[^,]*/;
                        show.parentalRating = text; 
                    })

                    //Director
                    .getTextNoError('//*[@id="content"]/div[1]/div[2]/div[2]/div[1]/div/div/dl/dd[3]').then(function(text){ //return Director 
                        console.log("=>" + text)
                        show.movieDirector = text
                    })   ///html/body/div[2]/div[1]/div[2]/div[2]/p[5] /html/body/div[2]/div[1]/div[2]/div[2]/p[6]

                    
                    .then(function(){
                            
                        if (new Date(show.startDay) > new Date("1 March 2016")){
                            show.movieStart = "7:00 PM"
                        } else {
                            show.movieStart = "8:00 PM"
                        }

                        show.venueId = "554ed6bed617526c16816cef"

                        show.gateOpen = "6:00 PM"
                        
                        show.language = ""
                        show.subtitle = ""

                        shows.push(show)

                        fs.appendFile(filePath, sep + JSON.stringify(show, null, 4), function(err){
                            //console.log('File successfully written! - Check your project directory for the log.json file');
                        });
                        if (!sep) sep = ",\n";

                        callback()
                    })
                    .end()
                //callback()
            }, function(){
                console.log("================= " + shows.length + " shows")
                console.log("CHECK FOR YEAR ON DATES!!!! ")

                fs.appendFile(filePath,"]", function(err){
                            //console.log('File successfully written! - Check your project directory for the log.json file');
                });
            })
        })
}

if (cinema == "mcdo" ) {
    var BURSWOOD = [];
    var POPUP = [];
    var MURDOCH = [];
    var BASSENDEAN = [];
    var MANDURAH = [];

    var sep = "";
    var filePath = './utils/scrapedWeb-JSON/scrapeMcdo.json'
    fs.writeFile(filePath, "[", function(err){
        //console.log('File successfully written! - Check your project directory for the log.json file');
    });
    var wd = webdriverio.remote(options)
        
    wd.addCommand("getTextNoError", function(selector) {
            return this.elements(selector).then(function(res) {

                if(!res.value || res.value.length === 0) {
                    // throw NoSuchElement error if no element was found
                    //throw new ErrorHandler(7);
                    console.log("Error getText: " + selector)
                    return []
                }

                var self = this,
                    elementIdTextCommands = [];

                res.value.forEach(function(elem) {
                    elementIdTextCommands.push(self.elementIdText(elem.ELEMENT));
                });

                return this.unify(elementIdTextCommands, {
                    extractValue: true
                });

            });
    });
    wd.addCommand("getAttributeNoError", function(selector, attributeName) {

                /*!
                 * parameter check
                 */
                if(typeof attributeName !== 'string') {
                    //throw new ErrorHandler.CommandError('number or type of arguments don\'t agree with getAttribute command');
                        console.log("Error getAttributeNoError attribute is not a string: " + selector)
                        return []
                }

                return this.elements(selector).then(function(res) {

                    if(!res.value || res.value.length === 0) {
                        // throw NoSuchElement error if no element was found
                        //throw new ErrorHandler(7);
                        console.log("Error getAttributeNoError: " + selector)
                        return []
                    }

                    var self = this,
                        elementIdAttributeCommands = [];

                    res.value.forEach(function(elem) {
                        elementIdAttributeCommands.push(self.elementIdAttribute(elem.ELEMENT, attributeName));
                    });

                    return this.unify(elementIdAttributeCommands, {
                        extractValue: true
                    });

                })
                //.catch(staleElementRetry.bind(this, 'getAttributeNoError', arguments));
    });
        
    wd.init()

        //main program
        .url('http://www.communitycinemas.com.au/Page/Programme')

        //BURSWOOD
        .getAttributeNoError('div .table-responsive:nth-child(4) tbody tr td:nth-child(3) a', 'href').then(function(attr){
            console.log("attr.length "+ attr.length);
            if (attr.length == 0) return
            attr.forEach(function(url,index){
                BURSWOOD[index]={url:"", venueId: "554ed6bcd617526c16816ceb" }
                BURSWOOD[index].url=url

                console.log(BURSWOOD[index].url)
            })
            return
        })
        .getTextNoError('div .table-responsive:nth-child(4) tbody tr td:nth-child(2)').then(function(text){
            console.log(text.length)
            if (text.length == 0) return
            text.forEach(function(txt, index){
                var year = checkYear(txt)
                BURSWOOD[index].date = txt + year
            })

            return
        })
        .getTextNoError('div .table-responsive:nth-child(4) tbody tr td:nth-child(3)').then(function(text){
            console.log(text.length)
            if (text.length == 0) return
            text.forEach(function(txt, index){
                BURSWOOD[index].movtitle = txt
            })
            console.log("BURSWOOD: " + BURSWOOD.length)
            console.log("=================================================")
            return
        })

        //POPUP
        .getAttributeNoError('div .table-responsive:nth-child(6) tbody tr td:nth-child(3) a', 'href').then(function(attr){
            console.log("attr.length "+ attr.length);             
            if (attr.length == 0) return

            attr.forEach(function(url,index){
                POPUP[index]={url:"", venueId: "popvenue" }
                POPUP[index].url=url

                console.log(POPUP[index].url)
            })
            return
        })
        .getTextNoError('div .table-responsive:nth-child(6) tbody tr td:nth-child(2)').then(function(text){
            console.log(text.length)
            if (text.length == 0) return

            text.forEach(function(txt, index){
                var year = checkYear(txt)
                POPUP[index].date = txt + year
            })

            return
        })
        .getTextNoError('div .table-responsive:nth-child(6) tbody tr td:nth-child(3)').then(function(text){
            console.log(text.length)
            if (text.length == 0) return
            text.forEach(function(txt, index){
                POPUP[index].movtitle = txt
            })
            console.log("POPUP: " + POPUP.length)
            console.log("=================================================")
            return
        })

        //MURDOCH
        .getAttributeNoError('div .table-responsive:nth-child(8) tbody tr td:nth-child(3) a', 'href').then(function(attr){
            console.log("attr.length "+ attr.length);
                        if (attr.length == 0) return

            attr.forEach(function(url,index){
                MURDOCH[index]={url:"", venueId: "554ed6bdd617526c16816cee" }
                MURDOCH[index].url=url
                console.log(MURDOCH[index].url)
            })
            return
        })
        .getTextNoError('div .table-responsive:nth-child(8) tbody tr td:nth-child(2)').then(function(text){
            console.log(text.length)
                        if (text.length == 0) return

            text.forEach(function(txt, index){
                var year = checkYear(txt)
                MURDOCH[index].date = txt + year
            })

            return
        })
        .getTextNoError('div .table-responsive:nth-child(8) tbody tr td:nth-child(3)').then(function(text){
            console.log(text.length)
            if (text.length == 0) return
            text.forEach(function(txt, index){
                MURDOCH[index].movtitle = txt
            })
            console.log("MURDOCH: " + MURDOCH.length)
            console.log("=================================================")
            return
        })

        //BASSENDEAN
        .getAttributeNoError('div .table-responsive:nth-child(10) tbody tr td:nth-child(3) a', 'href').then(function(attr){
            console.log("attr.length "+ attr.length);
                        if (attr.length == 0) return

            attr.forEach(function(url,index){
                BASSENDEAN[index]={url:"", venueId: "554ed6bcd617526c16816cea" }
                BASSENDEAN[index].url=url

                console.log(BASSENDEAN[index].url)
            })
            console.log(BASSENDEAN.length)
        })
        .getTextNoError('div .table-responsive:nth-child(10) tbody tr td:nth-child(2)').then(function(text){
            console.log(text.length)
            if (text.length == 0) return

            text.forEach(function(txt, index){
                var year = checkYear(txt)
                BASSENDEAN[index].date = txt + year
            })
        })
        .getTextNoError('div .table-responsive:nth-child(10) tbody tr td:nth-child(3)').then(function(text){
            console.log(text.length)
            if (text.length == 0) return
            text.forEach(function(txt, index){
                BASSENDEAN[index].movtitle = txt
            })
            console.log("BASSENDEAN: " + BASSENDEAN.length)
            console.log("=================================================")
            return
        })

        //MANDURAH
        .getAttributeNoError('div .table-responsive:nth-child(12) tbody tr td:nth-child(3) a', 'href').then(function(attr){
            console.log("attr.length "+ attr.length);
                        if (attr.length == 0) return

            attr.forEach(function(url,index){
                MANDURAH[index]={url:"", venueId: "554ed6bdd617526c16816ced" }
                MANDURAH[index].url=url

                console.log(MANDURAH[index].url)
            })
            console.log(MANDURAH.length)
        })
        .getTextNoError('div .table-responsive:nth-child(12) tbody tr td:nth-child(2)').then(function(text){
            console.log(text.length)
                        if (text.length == 0) return

             text.forEach(function(txt, index){
                var year = checkYear(txt)
                MANDURAH[index].date = txt + year
            })
        })
        .getTextNoError('div .table-responsive:nth-child(12) tbody tr td:nth-child(3)').then(function(text){
            console.log(text.length)
            if (text.length == 0) return
            text.forEach(function(txt, index){
                MANDURAH[index].movtitle = txt
            })
            console.log("MANDURAH: " + MANDURAH.length)
            console.log("=================================================")
            return
        })

        .end()
        .then(function(){
            //attr.slice(attr.length-1)
            var shows = []
            shows = shows.concat(BURSWOOD,POPUP,MURDOCH,MANDURAH,BASSENDEAN)
            console.log("=================Shows " + shows.length)

            // shows = [{
            //         url: "http://www.communitycinemas.com.au/Movie/Unindian",
            //         date: "Saturday 28 Nov 7:45 PM",
            //         venueId: "toto"
            // }]
            var k = 0



            

            async.forEachSeries(shows, function(sh, callback){
                k = k + 1                   
                console.log("======== " + k + " / " + shows.length )
                console.log("url "+ sh.url)
                var show = sh
                //var regExp;

                var client = webdriverio.remote(options)

                client.addCommand("getTextNoError", function(selector) {
                    return this.elements(selector).then(function(res) {

                        if(!res.value || res.value.length === 0) {
                            // throw NoSuchElement error if no element was found
                            //throw new ErrorHandler(7);
                            return ""
                        }

                        var self = this,
                            elementIdTextCommands = [];

                        res.value.forEach(function(elem) {
                            elementIdTextCommands.push(self.elementIdText(elem.ELEMENT));
                        });

                        return this.unify(elementIdTextCommands, {
                            extractValue: true
                        });

                    });
                });

                client.init()
                    .url(sh.url)

                    // //Title
                    // .getTextNoError('#content > div.row-fluid > div.span10 > div.row-fluid > div.span7.content-area.movie-detail > h1').then(function(text){ //return Movie title 
                    //     console.log("=>" + text)
                    //     show.movieTitle = text 
                    // })

                    // //Date
                    // .getTextNoError('#content div.span7.content-area.movie-detail div div dl dd').then(function(text){ //return dates 
                    //     console.log("=>" + text[0] + " WARNING YEAR IS MANUAL")
                    //     show.Date = text[0]
                    //     show.startDay = new Date(text[0] +" 2015");
                    //     show.endDay = show.startDay;

                    // })

                    // //Rating
                    // .getTextNoError('#content dd .rating').then(function(text){ //return PRating 
                    //     console.log("=>" + text)
                    //     //var regExp = /[^,]*/;
                    //     show.parentalRating = text; 
                    // })

                    //Director
                    .getTextNoError('div > dl > dd:nth-child(6)').then(function(text){ //return Director 
                        console.log("=>" + text)
                        show.movieDirector = text
                    })   ///html/body/div[2]/div[1]/div[2]/div[2]/p[5] /html/body/div[2]/div[1]/div[2]/div[2]/p[6]

                    
                    .then(function(){
                        console.log("create final show")

                        var date = new Date(sh.date)  
                        show.movieStart = date.toLocaleTimeString('en-US', {hour: '2-digit', minute: '2-digit', hour12: true})

                        show.gateOpen = "6:30 PM"
                        
                        show.language = ""
                        show.subtitle = ""

                        show.startDay = new Date(sh.date);
                        show.endDay = new Date(sh.date);

                        console.log(JSON.stringify(sh))
                        show.parentalRating = checkRating(sh.movtitle)

                        var regExp = /([^(]+) \(/;
                        show.movieTitle = regExp.exec(sh.movtitle)[1] //find "(" and select what is before

                        fs.appendFile(filePath, sep + JSON.stringify(show, null, 4), function(err){
                            //console.log('File successfully written! - Check your project directory for the log.json file');
                        });
                        if (!sep) sep = ",\n";
                        callback()
                    })
                    .end()
                //callback()
            }, function(){
                console.log("=================BURSWOOD " + BURSWOOD.length)
                console.log("=================POPUP " + POPUP.length)
                console.log("=================MURDOCH " + MURDOCH.length)
                console.log("=================BASSENDEAN " + BASSENDEAN.length)
                console.log("=================MANDURAH " + MANDURAH.length)
                fs.appendFile(filePath,"]", function(err){
                            //console.log('File successfully written! - Check your project directory for the log.json file');
                });
             })
        })
}

if (cinema == "rooftop" ) {
    var sep = "";
    var filePath = './utils/scrapedWeb-JSON/scrapeRooftop_' + today +'.json'
    fs.writeFile(filePath, "[", function(err){
        //console.log('File successfully written! - Check your project directory for the log.json file');
    });


    webdriverio
        .remote(options)
        .init()

        //main program
        .url('https://www.rooftopmovies.com.au/program/')


        //.waitForVisible('body', 2000)
        .getAttribute('.event_image', 'href')
        .then(function(attr){
            console.log("attr.length "+ attr.length);
            var Urls = attr
            Urls.forEach(function(url){
                console.log(url)
            })
            return attr
        })
        .then(function(arr){
            //attr.slice(attr.length-1)
            //arr = ['https://www.rooftopmovies.com.au/program/event/51d44b3e-2a91-4c6b-ad36-69b2076d26b1/session/16a61645-470d-4e67-9db0-a9a1159a902e/']
            //Urls = ['https://www.rooftopmovies.com.au/program/event/810b5612-5911-48fd-b25e-a8faedc7c771/session/ec0e849d-1600-4ded-b310-be1cb1d6c46f/']
            async.forEachSeries(arr, function(url, callback){
                console.log("========")
                console.log("url "+ url)
                var show = {}           
                //var regExp;

                var client = webdriverio.remote(options)

                client.addCommand("getTextNoError", function(selector) {
                    return this.elements(selector).then(function(res) {

                        if(!res.value || res.value.length === 0) {
                            // throw NoSuchElement error if no element was found
                            //throw new ErrorHandler(7);
                            return ""
                        }

                        var self = this,
                            elementIdTextCommands = [];

                        res.value.forEach(function(elem) {
                            elementIdTextCommands.push(self.elementIdText(elem.ELEMENT));
                        });

                        return this.unify(elementIdTextCommands, {
                            extractValue: true
                        });

                    });
                });

                client
                    .init()
                    .url(url)

                    //Title
                    .getTextNoError('h1 a').then(function(text){ //return Movie title 
                        console.log("=> Title: " + text)
                        if (text.search(/\(Encore Screening\)/i) > -1 ) {
                            text = text.slice(0,-18)
                            console.log("=> Title: " + text)
                        } else if (text.search(/\(Encore Screening 2\)/i) > -1){
                            text = text.slice(0,-20)
                            console.log("=> Title: " + text)
                        }
                        show.movieTitle = text.trim() 
                    })

                    //Date
                    .getTextNoError('[id*="dates"]').then(function(text){ //return dates 
                        console.log("=>" + text + " WARNING YEAR IS MANUAL")
                        show.Date = JSON.stringify(text)
                        var year = checkYear(text);
                        show.startDay = new Date(text + year);
                        console.log("=> " + show.startDay)
                        show.endDay = show.startDay;

                    })

                    //Rating
                    .getTextNoError('[id*="event"] > div > div.info.clearfix > div:nth-child(11)').then(function(text){ //return PRating 
                        console.log("=> parental: " + text)
                        //var regExp = /[^,]*/;
                        show.parentalRating = checkRating(text); 
                    })

                    //Director
                    .getTextNoError('[id*="event"] > p').then(function(text){ //return Director
                        
                            var regExp = /\:(.*?)\\/
                            var toto = regExp.exec(JSON.stringify(text))
                        if (toto) {
                            console.log("=> movieDirector: " + toto[1].trim())
                            show.movieDirector = toto[1].trim()
                        } else {
                            console.log("=> movieDirector: undefined!")
                        }
                    })   ///html/body/div[2]/div[1]/div[2]/div[2]/p[5] /html/body/div[2]/div[1]/div[2]/div[2]/p[6]

                    //GateOpen
                    .getTextNoError('[id*="doors_open"]').then(function(text){ //return Director 
                        console.log("=> gateOpen: " + text[1].trim())
                        show.gateOpen = text[1].trim()
                    })
                    
                    //SartTime
                    .getTextNoError('[id*="time"]').then(function(text){ //return Director 
                        console.log("=> startTime: " + text)
                        show.movieStart = text
                    })


                    .then(function(){
                            
                        // if (new Date(show.startDay) > new Date("1 March 2016")){
                        //     show.movieStart = "7:00 PM"
                        // } else {
                        //     show.movieStart = "8:00 PM"
                        // }

                        show.venueId = "554ed6bed617526c16816cf1"

                        //show.gateOpen = "6:00 PM"
                        
                        show.language = ""
                        show.subtitle = ""

                        shows.push(show)

                        fs.appendFile(filePath, sep + JSON.stringify(show, null, 4), function(err){
                            //console.log('File successfully written! - Check your project directory for the log.json file');
                        });
                        if (!sep) sep = ",\n";

                        callback()
                    })
                    .end()
                //callback()
            }, function(){
                console.log("================= " + shows.length + " shows")

                fs.appendFile(filePath,"]", function(err){
                            //console.log('File successfully written! - Check your project directory for the log.json file');
                });
            })
        })
        .end()
}

console.log("end==========")












    // //working
    // .url('https://perthfestival.com.au/lotterywest-festival-films/whats-on/2016/a-perfect-day/')
    // .waitForVisible('body', 2000)
    // .getHTML('.sticky-footer__event')
    // .then(function(res){
    //     log.push(res)
    // })
    // .then(function(){
    //     fs.writeFile('./nw/reports/log.json', JSON.stringify(log, null, 4), function(err){
    //         console.log('File successfully written! - Check your project directory for the log.json file');
    //     });
    // })
    // .end();

 
// webdriverio
//     .remote(options)
//     .init()
//     .url('https://www.rooftopmovies.com.au/program/')
//     .waitForVisible('body', 2000)
//     .getAttribute('.event_image', 'href')
//     .then(function(res){
//         async.eachSeries(res, function(val, callback){
//             console.log('toto')
//             callback();

//             // webdriverio.url(val).getTitle(function(err, res) {
//             //     console.log('Title was: ' + res.value);
//             //     callback();
//             // })
//         },function(){
//             console.log("done")
//         })
//         //console.log(res)
//         log.push(res)
//     })
//     .title(function(err, res) {
//         console.log('Title was: ' + res.value);
//     })
//     .then(function(){
//         fs.writeFile('./nw/reports/log.json', JSON.stringify(log, null, 4), function(err){
//             console.log('File successfully written! - Check your project directory for the log.json file');
//         });
//     })
//     .end();