var weatherKey = '85ee21624159a74b5a287ba91b447';
var requestUrl = 'https://api.worldweatheronline.com/free/v2/weather.ashx?q=perth&num_of_days=2&key='+weatherKey+'&format=json';

var request = require("request");

function dayOfWeekAsString(dayIndex) {
    return ["Mon","Tue","Wed","Thu","Fri","Sat","Sun"][dayIndex];
}


exports.getWeather = function(req, res) {
  request(requestUrl, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      res.send(body);
    } else {
      console.log(error, response.statusCode, body); 
      res.end("");       
    };
  });
};
// app.get('/weather', function (req, res) {
//     request(requestUrl, function (error, response, body) {
//         if (!error && response.statusCode == 200) {

//             // parse the json result
//             var result = JSON.parse(body);

//            // generate a HTML table
//            var html = '<table style="font-size: 10px; font-family: Arial, Helvetica, sans-serif">';

//            // loop through each row
//            for (var i = 0; i < 3; i++) {
//                html += "<tr>";

//                result.data.weather.forEach(function(weather) {
//                    html += "<td>";
//                    switch (i) {
//                        case 0:
//                            html += dayOfWeekAsString(new Date(weather.date).getDay());
//                            break;
//                        case 1:
//                            html += weather.hourly[0].weatherDesc[0].value;
//                            break;
//                        case 2:
//                            var imgSrc = weather.hourly[0].weatherIconUrl[0].value;
//                            html += '<img src="'+ imgSrc + '" alt="" />';
//                            break;
//                   }
//                   html += "</td>";
//               });
//               html += "</tr>";
//           }

//           res.send(html);
//         } else {
//            console.log(error, response.statusCode, body);
//         }
//         res.end("");
//     });
// });