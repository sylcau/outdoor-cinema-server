"use strict";
var request = require("request");
var http = require('http');
var nodemailer = require("nodemailer");
var async = require('async');
var mongoose = require('mongoose')
//var readline = require('readline');
//var child_process = require('child_process');

var csv = require('csv');
var fs = require('fs');

//*********************

var getShowsEndDate = require('../utilsZombie/checksAndUploadJS/getShowsEndDate').getShowsEndDate;


//global variable.
var allMovies = null;
var allShows = null;
var allVenues = null;

var allMovies_Reduced = {rows:[]};
var allShows_Reduced = {rows:[]};

// Import MODEL
var modelUser = require('../model/User');
var User = modelUser.User;

var model = require('../model/ShowMoviesVenues');
var Venue = model.Venue;
var Movie = model.Movie;
var Show = model.Show;

function copyFile(source, target, cb) {
  var cbCalled = false;

  var rd = fs.createReadStream(source);
  rd.on("error", function(err) {
    done(err);
  });
  var wr = fs.createWriteStream(target);
  wr.on("error", function(err) {
    done(err);
  });
  wr.on("close", function(ex) {
    done();
  });
  rd.pipe(wr);

  function done(err) {
    if (!cbCalled) {
      cb(err);
      cbCalled = true;
    }
  }
}
////////////////////////////////////////////////////////////////////

var previousWeek = new Date();
previousWeek.setDate( previousWeek.getDate() - 7);
//previousWeek = new Date(2015,0,8)
//============================================================================
// Get All Docs
//=============================================================================

var getAllMovies = function(req, res) {
	console.log('getAllMovies ' );
	Movie.find({},function(err, results){
        if (!err){ 
        	res.send({data: results});
        	console.log("done!!")
        } else {
        	res.send(err)
        	console.log("err: " + JSON.stringify(err));
        };
     });
};

var getAllVenues = function(req, res) {
	console.log('getAllVenues');
	Venue.find({},function(err, results){
        if (!err){ 
        	res.send({data: results});
        	console.log("done!!")
        } else {
        	res.send(err)
        	console.log("err: " + JSON.stringify(err));
        };
     });
};
var getAllShows = function(req, res) {
	function max(d){
		d.sort(function(a, b){return parseInt(b.idRef) - parseInt(a.idRef)});
		return d
    };
	console.log('getAllShows');
	Show.find({},function(err, results){
        if (!err){ 
        	res.send({data: results});
        	console.log("done!!")
        } else {
        	res.send(err)
        	console.log("err: " + JSON.stringify(err));
        };
     });
};

exports.getAllMovies = getAllMovies;
exports.getAllVenues = getAllVenues;
exports.getAllShows = getAllShows;
/////////////////////////////////////////////////////////////////////////////////////
// Get movies from Cache
exports.getMovies = function(req, res) {
	
	Show.find(
		{ "endDay": {
        	"$gte": previousWeek
    		}
    	}, function(err, results){
        if (!err){
        	var shows = [] 
        	results.forEach(function(show){
        		shows.push(show.movieRef)
        	});
        	Movie.find({"movieRef":{"$in":shows}},function(err, results){
        		res.send({data: results})
        	});

        	console.log("done!!")
        } else {
        	res.send(err)
        	console.log("err: " + JSON.stringify(err));
        };
    });

	//console.log('getMovies ' + allMovies_Reduced.length );
	// allMovies_Reduced = null
	// if (allMovies_Reduced != null ) { //
	// 	//console.log('cacheMov')
	// 	res.send(allMovies_Reduced)
	// } else {
	// 	//======= CURENTLY USELESS ...
	// 	getAllMovies(req, res);
	// }
};

exports.getMovieById = function(req, res) {
	var param = req.query.mvId;
	console.log("Get1Movie: param " + param)
	//console.log(JSON.stringify(req))
	//console.log(req)

	Movie.findById(param,'-Poster')
	.exec(function(err, result){
        if (!err){
        	res.send({data: result})
        	console.log("done!! get1Movie ")
        } else {
        	res.send(err)
        	console.log("err: " + JSON.stringify(err));
        };
    });
};

exports.getMovieByRef = function(req, res) {
	var param = req.query.mvRef;
	console.log("Get1Movie: param " + param)

	Movie.find({movieRef: param})
	.select('-Poster')
	.exec(function(err, result){
        if (!err){
        	res.send({data:result})
        	console.log("done!! get1Movie ")
        } else {
        	res.send(err)
        	console.log("err: " + JSON.stringify(err));
        };
    });
};

exports.getMoviesBySeason = function(req, res) {
	console.log("test")
	var startYear = new Date(2015, 8, 1);
	var endYear = new Date(2016, 8, 1);

	Show.find({startDay: {$gte: startYear , $lt: endYear}}).distinct('movieId').exec(function(err, result){
		console.log(result.length)

		Movie.find({'_id': { $in: result}})
			.select('movieRef Genre Runtime Title img movieTitle')
			.exec(function(err, movies){
				res.send({data: movies})
			})
	})
};

exports.getVenues = function(req, res) {
	console.log('getVenues');
		allVenues = null

	if (allVenues != null ) { //
		res.send({data: allVenues})
	} else {
	//=======
		getAllVenues(req, res);
	}
};

exports.getShows = function(req, res) {
	console.log('getShows');
	
	var fromDate = previousWeek
	if (req.query.today){
		var d = new Date(JSON.stringify(req.query.today))
		d.setDate( d.getDate() - 14);
		//console.log(d + ' rrrr getShows '+ req.query.today)

		fromDate = d
		console.log(' getShows '+ fromDate)

	}

	Show.find( { "endDay": {"$gte": fromDate} } )
		.populate({path: 'movieId', model: 'Movie', select: 'movieRef movieTitle img Genre Runtime parentalRating'})
		.exec(function(err, results){
	        if (!err){ 
	        	res.send({data: results});
	        	console.log("nb of shows send: " + results.length )
	        } else {
	        	res.send(err)
	        	console.log("err: " + JSON.stringify(err));
	        };
	    });


	// allShows_Reduced = null
	// if (allShows_Reduced != null) { //
	// 	res.send(allShows_Reduced)
	// } else {
	// 	//======= CURENTLY USELESS ...
	// 	getAllShows(req, res);
	// }
};


// the next 2 do almost the same.
exports.getLastShowsDate = function(req,res){
	Venue.find({})
		.select('_id cinema suburb')
		.exec()
		.then(function(ids){
			var promises = []
			ids.forEach(function(val){

				promises.push(Show.find( { "venueId": val._id , "endDay": {"$gte": new Date(2016,0,1)} } )
					.populate({path: 'venueId', model: 'Venue', select: 'cinema suburb'})
					//.select('startDay')
					.exec()
					.then(function(results){
				        if (typeof results == 'array') {console.log(results[0].venueId.cinema)	}			        	
			        	function findMax(array){
			        		array.sort(function(a, b){ return new Date(b.startDay) - new Date(a.startDay) });
					  		return array[0]
			        	}
			        	//find the latest
			        	var max = findMax(results)
			        	
			        	console.log("done!!" + JSON.stringify(max))
			        	return max
				    })
				)
				    // .catch(function(e){
				    //     	//res.send(err)
			     //    	console.log("err: " + JSON.stringify(err));
			     //    	return err
				    // });

			});

			return Promise.all(promises).then(function(data){
				var finalres = []
				data.forEach(function(val){
					if (val && typeof val.startDay != "undefined" ) {
						var date = new Date(val.startDay)
						finalres.push([val.venueId.cinema + " | " + val.venueId.suburb, date.toDateString()  ])
					}
				})
				//sort res
				finalres.sort(function(a, b){ return new Date(a[1]) - new Date(b[1]) })
				var d = new Date
				res.send({date: d.toDateString(), data: finalres})
			})
			.catch(function(e){
				console.log(e)
				res.send(e)
			})
		})
};

exports.getShowsEndDate = function(req,res){
	console.log("getShowsEndDate")
	getShowsEndDate({log: ""})
	.then(function(re){
		console.log("Done getShowsEndDate - nb of venues: " + re.venues.length)
			
		res.send(re.log)
	})
	.catch(function(e){
		console.log("err " + e)
		res.send(e)	
	});

};

//
exports.initCache = function(req, res){
	initCache(function(result){
		res.send('result: ' + result)
	});
	//res.send('Init Cache server...')
};

exports.initCacheRestart = function(){
	initCache(function(result){
		console.log('result: ' + result)
	});
	//res.send('Init Cache server...')
};


function initCache(callback){
	var d = new Date();
	var callbackStatus = [];
	var total_callbackStatus = 3; //nombre de request
	//reset Reduced collection
	allMovies_Reduced = {rows:[]};
	allShows_Reduced = {rows:[]};
	
	//Request Shows that are in the future:
	ocdb.view('shows', 'all', function(err, body) {
		if (!err) {
			allShows = body
			console.log('cacheShows: ' + d)

			//select shows in the future:  // Should be done at the database level ... TODO
			allShows.rows.forEach(function(show){
				var showEndDay = new Date(show.value.endDay)
				showEndDay.setDate(showEndDay.getDate() + 7)
				//console.log(d + ' ==== ' + show.value.endDay)
				if (showEndDay > d){
					allShows_Reduced.rows.push(show)
				}
			});
			console.log('allShows_Reduced.rows.length' + allShows_Reduced.rows.length)

			callbackStatus.push({status: "OK - allShows_Reduced.rows.length", bdy : '' })
			final();
		} else {
			//res.send('error with db.get : ' + err)
		}
	});

	//request all movies
	ocdb.view('movies', 'byTitle', function(err, body) {
		if (!err) {
			allMovies = body;
			console.log('cacheMovies: ' + d);
			callbackStatus.push({status: "OK", bdy : '' })
			final();
		} else {
			//res.send('error with db.get : ' + err)
		}
	});

	//request all venues
	ocdb.view('venues', 'byRef', function(err, body) {
		if (!err) {
		    allVenues = body;
			console.log('cacheVenues: ' + d);
			callbackStatus.push({status: "OK", bdy : '' })
			final();
		} else {
			//res.send('error with db.get : ' + err)
		}
	});

	function final(err){  // NOK status is not treated ......
			// console
	  // 		//check if one is not ok
			// var obj = callbackStatus.filter(function ( obj ) {
			//     return obj.status === "NOK";
			// })[0];

			// if (obj == undefined) {
			// 	final(callbackStatus)
			// } else{
			// 	console.log(index)
			// }
		if (!err && (callbackStatus.length == total_callbackStatus) ){
			console.log('ok')

			//setup allMovies_Reduced
			allMovies_Reduced = {rows:[]};
			//go through shows and select movies:
			allShows_Reduced.rows.forEach(function(show){
				var movieRef = show.value.movieRef;
				//console.log('? ' +movieRef )
				//check if movie already in allMovies_Reduced collection
				var filt = allMovies_Reduced.rows.filter(function(e) { 
					return e.value.movieRef == movieRef; })[0];
				//console.log('?filt ' + filt )

				if (filt != undefined && filt.hasOwnProperty("value")){
					// already in the new collection // do nothing
				} else {
					// add to new database
					//console.log('add2databse: '+ movieRef)
					//find movie in allMovies
					var movie = allMovies.rows.filter(function(e) { 
						return e.value.movieRef == movieRef; })[0];

					//clean movie
					delete movie.Poster;
					delete movie.imdbID;
					delete movie.imdbRating;
					delete movie.imdbVotes;
					delete movie.Type;
					delete movie.Response;

					//and add movie to collection
					allMovies_Reduced.rows.push(movie) 
				}
			});
			console.log(allMovies_Reduced.rows.length)
			allMovies_Reduced.total_rows = allMovies_Reduced.rows.length;
			callback(allMovies_Reduced.rows.length);

		} else if (err){
			callback(err)
		} else {

		}
	};
};

exports.postReminder = function(req, res){
	var reminder = req.body.reminder;
	var userId = req.body.userId;
	console.log("postReminder: " + req.body.userId + " // " + req.body.reminder.nbDays )
	//console.log(JSON.stringify(req.body) )
	if (typeof userId == "undefined") { res.send("error"); console.log("postReminder: missing userId")}
	if (typeof reminder == "undefined") { res.send("error"); console.log("postReminder: missing reminder details")}

	//reminderData.userId = userId
	//var reminder = new Reminder(reminderData)

	User.findById(userId, function (err, doc) {
  		if (err) { return res.send(err)
  		} else {
  			if (typeof doc.reminders == "undefined"){
  				doc.reminders.push(reminder)
  			} else {
  				//find if existing reminder for this show and update the value \\ Need the toString because Show ref is na ObjectId
  				var indexShow = doc.reminders.map(function(e) { return e.show.toString(); }).indexOf(reminder.show)
  				//console.log(JSON.stringify(doc.reminders.map(function(e) { console.log(e.show.toString()); return e.show; })) + "|| "+ '"'+reminder.show+'"')
  				if (indexShow == -1) { //not find //or add new record
  					doc.reminders.push(reminder);
  				} else {
  					console.log('Find Duplicate => update value')
  					doc.reminders[indexShow].nbDays = reminder.nbDays
  					doc.markModified('reminders');
  				}
  				
  			};
			
	  		doc.save(function(err){
	  			if (err) {
					console.log("error")
					return res.send(err);
				} else {
			    	res.send('ok');	
				}
	  		});
  		}
	})
};

exports.removeReminders = function(req, res){
	var reminderId = req.query.reminderId;
	console.log("removeReminders: " + req.query.reminderId )
	if (typeof req.query.reminderId == "undefined") { 
		console.log("removeReminders: missing reminderId") 
		return res.send("error"); 
	} 

	console.log(mongoose.Types.ObjectId(reminderId))
	User.findOne({"reminders._id": mongoose.Types.ObjectId(reminderId)})
		.select('reminders')
		.populate('reminders.show')
		.exec(function (err, user) {
			//console.log(user)
	  		if (err) { return res.send(err) };
	  		
	  		user.reminders.id(reminderId).remove() 			//could investigate using $pull ///
	  		user.save(function(err){
				if (err) { 
					console.log("error saving document in removeReminder" + err);
					return res.send(err) 
				} else{

			    	Movie.populate(user.reminders,{ path: "show.movieId", select: 'movieTitle'}, function(err, reminders2) {
			    		//console.log("==== " + reminders2)
	    				if (err) { return res.send(err) }; // or do something		         
						
						return res.send({data: user.reminders})	
					});		
				}
			})	
		});
};


exports.getReminders = function(req, res){
	var userId = req.query.userId;
	console.log("getReminder: " + req.query.userId )
	
	if (typeof req.query.userId == "undefined") { 
		console.log("postReminder: missing userId"); 
		return res.send("error");
	} 

	User.findById(userId)
		.select('reminders')
		.populate('reminders.show')
		.exec(function (err, user) {
	  		if (err) { return res.send(err)};
	  		if (typeof user.reminders == "undefined" || user.reminders.length == 0) { return res.send({data: []})}
  			//
			Movie.populate(user.reminders,{ path: "show.movieId", select: 'movieTitle'}, function(err, reminders2) {
	    		if (err) { return res.send(err) }; // or do something		         
				return res.send({data: user.reminders})	
			});			
		});
};

exports.processReminders = function(req,res){
	console.log("processReminder")

	var reminder2send = [];
	var today = new Date("2015-01-08T16:00:00.000Z")
	var minus7Day = new Date("2015-01-01T16:00:00.000Z")

	User.find({})
		.select('displayName email reminders')
		.populate({
			path: 'reminders.show',
			select: '-language -subtitle',
			startDay: { $gte: minus7Day }
		})
		.exec(function (err, users) {
	  		if (err) { return res.send(err)};
	  		//if (typeof user.reminders == "undefined" || user.reminders.length == 0) {res.send({data: []})}
  			//
            async.waterfall([
            	function(callback){
            		var reduceUser = [];
            		users.forEach(function(val){
            			if ((typeof val.reminders != "undefined") && (val.reminders.length != 0)) {
            				reduceUser.push(val)
            			}
            		})
            		callback(null, reduceUser)
            	},
				function(results, callback){
					Movie.populate(results,{ path: "reminders.show.movieId", select: 'movieTitle'}, function(err, results) {
	    				if (err) { return res.send(err) }; // or do something		         
						//res.send({data: user.reminders})
						//console.log(JSON.stringify(results))
						callback(null, results)	
					});	
				},
				function(results, callback){
					Venue.populate(results, {path: 'reminders.show.venueId', select: 'cinema suburb' }, function(err, results){
						//console.log('Venue populate '  + JSON.stringify(results));
						//res.send(results1);
						callback(null, results);
					})
				}
				], function(err, results){
					console.log("---------------- end of waterfall");
					//console.log("---------------SHOW " + JSON.stringify(reminder2sendTmp[index].show ))
					//res.send(results)
					// SAve results in File .....
					// TODO!!!
					//

					// create reusable transport method (opens pool of SMTP connections)
	        		var smtpTransport = nodemailer.createTransport({
					    service: "Mailgun",
					    auth: {
					        user: "postmaster@sandboxefbd444a4c6c467c8c06c5b70faf2511.mailgun.org",
					        pass: "90b0a509a29a6970a23a7f88afa7fc63"
					    }
					});

					// //create emails
	        		async.forEachSeries(results, function(user, callback){
	        			console.log("email: " + user.email + " | reminders: " +user.reminders.length)
	        			var showList = ""
	        			user.reminders.forEach(function(val){
	        				//console.log("-----------  "+JSON.stringify(val.show))
	        				showList = showList + "<li> <span style=\"color: #83207d;\">" + 
	        				val.show.movieId.movieTitle +"</span> on " + val.show.startDay + " at " + 
	        				val.show.venueId.cinema +" in " + val.show.venueId.suburb +" </li>"
	        			})
	        			// Create HTML email data
	        			var htmlEmail = "Hey "+ user.displayName + 
	        					"! <br> <br> You are receiving this email because you have selected a some shows on http://www.myoutdoorcinema.com.au/ <br> <br>" + 
					    		"<b> Show coming: </b> <br>" +
					    		showList +
					    		"<br>" + 
					    		"Thanks for using myoutdoorcinema! <br>" +
					    		"<br> to change your reminders settings, please visit http://www.myoutdoorcinema.com.au/profile " +
					    		"" // html body

	        			// setup e-mail data with unicode symbols
						var mailOptions = {
						    from: "noreply@myoutdoorcinema.com.au", // sender address
						    to: user.email,
						    subject: "Show reminder - my Outdoor Cinema", // Subject line
						    //text: "test........"
						    html: htmlEmail	
						}

						// send mail with defined transport object
						smtpTransport.sendMail(mailOptions, function(error, info){
						    if(error){ console.log(error);
						    	callback();
						        //res.send('{"error":1, "message":"Sorry there was an issue. please try again later"}');
						    } else {
					        	console.log("Message sent"); //JSON.stringify(info));
					    		//res.send('{"error":0, "message":"ok"}');
					    		//res.send('{"error":0}');
					    		callback();

					    	}
				       	})
				    }, function(err){
				    	console.log( "done emails")
				    	smtpTransport.close();
				    	res.json("done")
				    });
					}
			);

					
		});
};
