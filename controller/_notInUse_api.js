
//git@heroku.com:lottosc.git
"use strict";
var request = require("request");
var http = require('http');
var nodemailer = require("nodemailer");
//var readline = require('readline');
//var child_process = require('child_process');

var csv = require('csv');
var fs = require('fs');

//*********************
//Define Database address
var nano = require('nano')('https://sylvain.cloudant.com/');
//var nano = require('nano')('https://oundkedshoughtentilyingb:oundkedshoughtentilyingb@cloudant.com/');
var ocdb = nano.db.use('outcine3');

// sylvain / cloudantpass;

//API KEY for Reader
// Key: hationsidefungstudgetrab
// Password: 5mg07gXgIfBPjokbyduv0eVG
//**************************************
// https://hationsidefungstudgetrab:5mg07gXgIfBPjokbyduv0eVG@hationsidefungstudgetrab.cloudant.com 

// Docs ref
//var allShows = 'a8728bd232da3c29a883267a943d1554';
var docTrend = '';
var docLast = '';

//global variable.
var allMovies = null;
var allShows = null;
var allVenues = null;

var allMovies_Reduced = {rows:[]};
var allShows_Reduced = {rows:[]};
// function checkCache(){
// 	var currentTime = new Date() 
// 	if allMovies.timestamp - currentTime > hour
// 	return true;

// }

function newMovie(obj){
	this.type = 'movie';
};


function show2(array){
	this.type = 'show';
	this.idRef = array[0];
	this.movieRef = array[1];
	this.venueRef = array[2];
	this.startDay = array[3];
	this.endDay = array[4];
	this.gateOpen = array[5];
	this.movieStart = array[6];
	this.language = array[7];
	this.subtitle = array[8];
};


function venue2(array){
	this.type = 'venue';
	this.cinema = array[0];
	this.suburb = array[1];
	this.venueRef = array[2];
	this.url = array[3];

	if (array.length > 4) {
		this.gps = array[4];
		this.tips = array[5];
		this.food = array[6];
	} else {
		this.gps = '';
		this.tips = '';
		this.food = '';
	}

};
///////////////////////////////////
// UNIT FUNCTION
/////////////////////////////////////////////////////////////////////
//upload to Database
function insert2Database(arg1, arg2, callback){
	ocdb.insert(arg1, arg2, function(err, body) {
	 	if (!err) {
	 		callback();
		}
	 	else {
	 		console.log('issue inserting : ' + err);
	 		callback(err);
	 	}
	});
};

//update Doc:
function updateDoc(doc, id, callback){
	ocdb.insert(doc, id, function(err, body) {
	 	if (!err) {
	 		callback('{}');
		}
	 	else {
	 		console.log('issue inserting : ' + err);
	 		callback(err);
	 	}
	});
};

//Delete Doc details
function deleteDoc(id, rev, callback){
	ocdb.destroy(id, rev, function(err, body) {
	 	if (!err) {
	 		callback();
		}
	 	else {
	 		console.log('issue deleting : ' + err);
	 		callback(err);
	 	}
	});
};

function copyFile(source, target, cb) {
  var cbCalled = false;

  var rd = fs.createReadStream(source);
  rd.on("error", function(err) {
    done(err);
  });
  var wr = fs.createWriteStream(target);
  wr.on("error", function(err) {
    done(err);
  });
  wr.on("close", function(ex) {
    done();
  });
  rd.pipe(wr);

  function done(err) {
    if (!cbCalled) {
      cb(err);
      cbCalled = true;
    }
  }
}
////////////////////////////////////////////////////////////////////

// UPLOAD ALL Shows by reading CSV. and movie Ref json (mvref) to find details.
// Get Info from OMDB and update Db document (TO REVIEW -- FOR THE ALL DATABASE)
exports.uploadCsvV2 = function(req, res) {
	console.log('uploadCsv')
	var results = [];
	
	//read mvref to get movies reference 
	var mvref = JSON.parse(fs.readFileSync('./utils/mvref.json', 'utf8'));

	//get the info fo the movies
	function getInfoforMovie(){
		
		var k = 0

		function final1(array, k){
			//var completed = array.filter(function(e) { return e.imdbID != undefined; })
			console.log(k + "|" + array.length)
			if ( k == array.length){

				fs.createReadStream("./MovieData.csv").pipe(parser);
			}
		};

		mvref.forEach(function(mv){
			var title = ""
			if (mv.altTitle != "0" ){
				title = mv.altTitle
			} else {
				title = mv.movie
			}

			var url1 = 'http://www.omdbapi.com/?t=' + encodeURIComponent(title)+'&y=&plot=full&r=json'

			request(url1 , function (error, response, json) {
				console.log('start request OMDBAPI: ' + url1);
		  		//res.set("Content-Type", "text/javascript");
				if (!error){
							var obj = JSON.parse(json)
							for (var attrname in obj) { mv[attrname] = obj[attrname]; }
							mv.type = "movie";
							k = k + 1
							final1(mvref, k);  
				} else {
					console.log('error movie getting info for: '+ mov.title)
				}
			});
		});
	};

	//parse CSV with Shows 
	var parser = csv.parse({delimiter: ','}, function(err, data){
		if (err) { console.log(err.message)
		} 
		else {
			console.log('parser callback');
			console.log(JSON.stringify(data.slice(1,5)));

			//results = data.slice(1,999)
 
			var movies = mvref; //collection of movies
			var venues = []; //collection of venues
			var shows = []; //collection of show
			//
			var headers = data[0] // set Headers
			data = data.slice(1); // remove headers
			data.forEach(function(item,index){

				var obj = {}
				item.forEach(function(val, index){
					obj[headers[index]] = val
				});

 				//Add show to collection:
  				var show = new show2([obj.idRef, obj.movieRef, obj.venueRef,obj.startDate,obj.endDate,obj.gateOpen,obj.movieStart, obj.language, obj.subtitle])

 				if ( index<3) { console.log("show " + JSON.stringify(show))}
 				shows.push(show)

				////check if movie is already in collection
				//var indexMovie = movies.map(function(e) { return e.Title; }).indexOf(obj.movie);

				////if not push new movie with show. if yes add show to SHOWS.
				// if (indexMovie == -1) {  //not exist create the movie
				// 	//var mov = new movie2(item.slice(1,7))
				// 	var mov = {movieRef: item[3], title: item[1], parentalRating: item[2]}
				// 	// get info from OMDBAPI.com

				// 	if ( index<3) { console.log("mov " + JSON.stringify(mov))}

				// 	movies.push(mov) 
				// 	indexMovie = movies.length - 1
				// } 

				//console.log(indexMovie + " indexMovie | " + movieTitle +  " movieTitle | " + movies[indexMovie].title);

				//check if venue exist. if not add venue to VENUES collection.
				var indexVenue = venues.map(function(e) { return e.venueRef; }).indexOf(obj.venueRef);
				if (indexVenue == -1) {  //not exist
					var ven = new venue2([obj.cinema, obj.suburb, obj.venueRef, obj.urlVenue ])
					venues.push(ven) 
				} else { //exist! all good
				}
			});
			console.log("venues: " + venues.length )
			console.log("movies: " + movies.length )
			console.log("shows: " + shows.length )
			
			// async status
			console.log("====Start ASYNC=======")

  			var collections = shows.concat(movies.concat(venues));

  			var callbackStatus = [];
  			for (var i = 0 ; i < collections.length  ; i++){
  				callbackStatus[i] = {status: "NOK", bdy : '' }
  			}
			//Insert all venues/movies/shows to database
			var name = undefined;
			collections.forEach(function(val, index){
				if (val.type == "venue") {
					name = val.venueRef
				} else if (val.type == "movie"){
					name = val.movieRef
				} else {
					name = undefined
				}
				insert2Database(val, name, function(result){
  					callbackStatus[index] = {status: "OK", bdy : result }

	  				//check if one is not ok
	  				var obj = callbackStatus.filter(function ( obj ) {
					    return obj.status === "NOK";
					})[0];

					if (obj == undefined) {
						final(callbackStatus)
					} else{
						console.log(index)
					}
  				});
			})

			//**End
	  	}
	});

	//fs.createReadStream("./MovieData.csv").pipe(parser);


	getInfoforMovie();


	//send response.
	function final(status){
		obj = JSON.stringify('status.a1bdy')
		res.send( obj  )
		console.log("End postUpdate(resultupdate)")
	}
};

// UPLOAD new Shows by reading CSV. and movie Ref json (mvref) to find details 
//   Get Info from OMDB and update Db document (TO REVIEW -- FOR THE ALL DATABASE)
exports.uploadCsvV3 = function(req, res) {
	console.log('uploadCsv')
	var results = [];
	
	// Get existing movies

	//read mvref to get movies reference 
	var mvref = JSON.parse(fs.readFileSync('./utils/mvref.json', 'utf8'));

	//get the info fo the movies
	function getInfoforMovie(){
		
		var k = 0

		function final1(array, k){
			//var completed = array.filter(function(e) { return e.imdbID != undefined; })
			console.log(k + "|" + array.length)
			if ( k == array.length){

				fs.createReadStream("./MovieData.csv").pipe(parser);
			}
		};

		if (mvref.length > 0 ){
			mvref.forEach(function(mv){
				var title = ""
				delete mv.key;
				if (mv.altTitle != "0" ){
					title = mv.altTitle
				} else {
					title = mv.movie
				}

				var url1 = 'http://www.omdbapi.com/?t=' + encodeURIComponent(title)+'&y=&plot=full&r=json'

				request(url1 , function (error, response, json) {
					console.log('start request OMDBAPI: ' + url1);
			  		//res.set("Content-Type", "text/javascript");
					if (!error){
								var obj = JSON.parse(json)
								for (var attrname in obj) { mv[attrname] = obj[attrname]; }
								mv.type = "movie";
								k = k + 1
								final1(mvref, k);  
					} else {
						console.log('error movie getting info for: '+ mov.title)
					}
				});
			});
		} else {
			fs.createReadStream("./MovieData.csv").pipe(parser);
		}

	};

	//parse CSV with Shows 
	var parser = csv.parse({delimiter: ','}, function(err, data){
		if (err) { console.log(err.message)
		} 
		else {
			console.log('parser callback');
			console.log(JSON.stringify(data.slice(1,5)));

			var movies = mvref; //collection of movies
			var shows = []; //collection of show

			//results = data.slice(1,999)
		
			//
			var headers = data[0] // set Headers
			data = data.slice(1); // remove headers
			data.forEach(function(item,index){

				var obj = {}
				item.forEach(function(val, index){
					obj[headers[index]] = val
				});

 				//Add show to collection:
  				var show = new show2([obj.idRef, obj.movieRef, obj.venueRef,obj.startDate,obj.endDate,obj.gateOpen,obj.movieStart, obj.language, obj.subtitle])

 				if ( index<3) { console.log("show " + JSON.stringify(show))}
 				shows.push(show)

				////check if movie is already in collection
				//var indexMovie = movies.map(function(e) { return e.Title; }).indexOf(obj.movie);

				////if not push new movie with show. if yes add show to SHOWS.
				// if (indexMovie == -1) {  //not exist create the movie
				// 	//var mov = new movie2(item.slice(1,7))
				// 	var mov = {movieRef: item[3], title: item[1], parentalRating: item[2]}
				// 	// get info from OMDBAPI.com

				// 	if ( index<3) { console.log("mov " + JSON.stringify(mov))}

				// 	movies.push(mov) 
				// 	indexMovie = movies.length - 1
				// } 

				//console.log(indexMovie + " indexMovie | " + movieTitle +  " movieTitle | " + movies[indexMovie].title);

			});
			console.log("movies: " + movies.length )
			console.log("shows: " + shows.length )
			
			// async status
			console.log("====Start ASYNC=======")

  			var collections = shows.concat(movies);

  			var callbackStatus = [];
  			for (var i = 0 ; i < collections.length  ; i++){
  				callbackStatus[i] = {status: "NOK", bdy : '' }
  			}
			//Insert all venues/movies/shows to database
			var name = undefined;
			collections.forEach(function(val, index){
				if (val.type == "venue") {
					name = val.venueRef
				} else if (val.type == "movie"){
					name = val.movieRef
				} else {
					name = undefined
				}
				insert2Database(val, name, function(result){
  					callbackStatus[index] = {status: "OK", bdy : result }

	  				//check if one is not ok
	  				var obj = callbackStatus.filter(function ( obj ) {
					    return obj.status === "NOK";
					})[0];

					if (obj == undefined) {
						final(callbackStatus)
					} else{
						console.log('Issue with:' + index)
					}
  				});
			})

			//**End
	  	}
	});

	//fs.createReadStream("./MovieData.csv").pipe(parser);


	getInfoforMovie();


	//send response.
	function final(status){
		obj = JSON.stringify('status.a1bdy')
		res.send( obj  )
		console.log("End postUpdate(resultupdate)")
		initCache()
	}
};

// =============================================================================
// Work with Json file
// ===========================================================================

// Create newShows.json (with new shows to upload) and newMovies.json 
// resulting from Scrape by reading JSON  +  Get Info from OMDB
//  next will be => update Db document
exports.newShowGenerator = function(req, res) {
	console.log('newShowGenerator')
	var mvref = [];
	var scrapeResults = [];
	var MovieCollectionAddition = [];
	var showsNumber = -1;

	// Get existing movies/////////////////////////////////////
	function GetMovieRef(callback){
		ocdb.view('movies', 'byRef', function(err, body) {
			if (!err) {
				//allMovies = body;
				console.log('allMovies :' + body.rows.length)
				callback(body.rows)
			} else {
				c('error with db.get : ' + err)
				callback()
			}
		});
	} 
	GetMovieRef(function(res){
		mvref = res;
		c("mvref : " + mvref.length + " : " + mvref[0]);
		NextStep()
	});

	// read scrape results put in array//////////////////////////////////////////
	scrapeResults = JSON.parse(fs.readFileSync('./utils/scrape.json', 'utf8'));
	c("scrapeResults : " + scrapeResults.length + " : " + scrapeResults[0])

	//get highest id number for shows (to be able to add increment number id)//////////////////////////
	;
	function GetShowsNumber(callback){
		function max(d){
			d.sort(function(a, b){return parseInt(b.key) - parseInt(a.key)});
			return d
    	};
		console.log('GetShowslength');
		ocdb.view('shows', 'byRef2', function(err, body) {
			if (!err) {
				//flter numbers

				max(body.rows);
				callback(body.rows[0].key);
			} else {
				console.log('error with db.get : ' + err)
				callback(-1);
			}
		});
	};

	GetShowsNumber(function(res){
		showsNumber = res;
		console.log("showsNumber : " + showsNumber)
		NextStep();
	});
	

	NextStep();
	function NextStep(){
		createNewShow(scrapeResults, mvref, MovieCollectionAddition, showsNumber, function(){
			getInfoforMovieArray(MovieCollectionAddition, function(){
				fs.writeFile('./utils/newMovies.json', JSON.stringify(MovieCollectionAddition, null, 4), function(err){
					console.log('File successfully written!');
					res.send('File successfully written!')
				});
			});
		});
	};

	// for each new show, 
	// create object show, check if movie exist. if not Increment Movie Ref, 
	// fill MovieCollectionAddition
	// writenewShows.json
	function createNewShow(ArrayScrapeShows, MovieCollection, MovieCollectionAddition, showsMaxNumber, callback){
		function maxRef(d){
			d.sort(function(a, b){return parseInt(b.key.slice(-4)) - parseInt(a.key.slice(-4))});
			return d
		};
		
		if (showsMaxNumber !== -1 && ArrayScrapeShows.length > 0 && MovieCollection.length > 0){
			var ArrayNewShows = [];
			//find max movieRef

			maxRef(MovieCollection);
			var maxMovieRef = parseInt(MovieCollection[0].key.slice(-4));
			console.log('maxMovieRef :' + maxMovieRef)

			ArrayScrapeShows.forEach(function(sh, index){
				var idRef = parseInt(showsMaxNumber) + index + 1

				//check if new movie 
				MovieCollection.forEach(function(mv){ mv.movie = mv.value.movie});
				var mvCollections = MovieCollection.concat(MovieCollectionAddition);
				// tolowerCase to search with insensitive case
				var indexMovie = mvCollections.map(function(e) { return e.movie.toLowerCase(); }).indexOf(sh.movie.toLowerCase());
				if (indexMovie == -1) {  //not exist create the movie
					var id = "OC" + (maxMovieRef + MovieCollectionAddition.length + 1);
					var trailer = sh.trailer || "";
					var mov = {movieRef: id, movie: sh.movie, parentalRating: sh.rating, trailer: trailer};
					MovieCollectionAddition.push(mov);
					sh.movieRef = id;
				} else {
					sh.movieRef = mvCollections[indexMovie].key || mvCollections[indexMovie].movieRef;
					console.log("find movie in database: " + sh.movieRef)
					
				}

				//create show
				var show = new show2([idRef.toString(), sh.movieRef, sh.venueRef,sh.startDate,sh.endDate,sh.gateOpen,sh.movieStart, sh.language, sh.subtitle]);
				ArrayNewShows.push(show)
			});

			fs.writeFile('./utils/newShows.json', JSON.stringify(ArrayNewShows, null, 4), function(err){
				console.log('File successfully written!');
				callback();
			});
		}
	}

	//get the info fo the movies
	//newMovieArray is [{mvref: , ...}, {mvref: , ...} ]
	function getInfoforMovieArray(newMovieArray, callback){
		var k = 0

		if (newMovieArray.length > 0 ){
			newMovieArray.forEach(function(mv){
				var title = ""
				//delete mv.key;
				if (mv.altTitle){
					title = mv.altTitle
				} else {
					title = mv.movie
				}

				var url1 = 'http://www.omdbapi.com/?t=' + encodeURIComponent(title) + '&y=&plot=full&r=json'

				request(url1 , function (error, response, json) {
					console.log('start request OMDBAPI: ' + url1);
			  		//res.set("Content-Type", "text/javascript");
					if (!error){
						var obj = JSON.parse(json)
						for (var attrname in obj) { mv[attrname] = obj[attrname]; }
						mv.type = "movie";
						k = k + 1
						final1(newMovieArray);
					} else {
						console.log('error movie getting info for: '+ mov.title)
					}
				});
			});
		} else {
			//fs.createReadStream("./MovieData.csv").pipe(parser);
		}

		function final1(array){
			//var completed = array.filter(function(e) { return e.imdbID != undefined; })
			console.log(k + "|" + array.length)
			if ( k == array.length){
				callback();
			}
		};
	};
};

// Upload to Database new documents from json file (newShows.json or newMovies.json)
exports.uploadJsonS = function(req,res){
	
	var Docs = []
	var file1 = './utils/newShows.json';
	var file2 = './utils/newMovies.json';
	var d = new Date();
	var file1Arch = "./utils/archive/newShows" + "_" + d.getMonth() + "-" + d.getDate() + "-" + d.getTime() + ".json";
	var file2Arch = "./utils/archive/newMovies" + "_" + d.getMonth() + "-" + d.getDate() + "-" + d.getTime() + ".json";

	console.log("files : "+ file1 + " & " + file2)

	// read jsonFile and put in array
	var array1 = JSON.parse(fs.readFileSync(file1, 'utf8'));
	var array2 = JSON.parse(fs.readFileSync(file2, 'utf8'));
	Docs = array1.concat(array2)
	console.log("Docs : " + Docs.length)

	var callbackStatus = [];
	for (var i = 0 ; i < Docs.length  ; i++){
		callbackStatus[i] = {status: "NOK", bdy : '' }
	}

	//Insert all venues/movies/shows to database
	var name = undefined;
	Docs.forEach(function(val, index){
		if (val.type == "venue") {
			name = val.venueRef
		} else if (val.type == "movie"){
			name = val.movieRef
		} else {
			name = undefined
		}
		insert2Database(val, name, function(result){
			if (result){
				callbackStatus[index] = {status: "Error", bdy : result }
				console.log(name + " | " + result.error)
			} else{
				callbackStatus[index] = {status: "OK", bdy : result }	
			}
			
			final(callbackStatus)
		});
	});
	
	//send response.
	function final(status){
		
		//check if one is not processed
		var proc = callbackStatus.filter(function ( obj ) {
	    	return obj.status === "NOK";
		})[0];

		if (proc == undefined) {
			copyFile(file1, file1Arch, function(err){ 
				if (!err) {
					console.log("Copy file1")
					fs.writeFile('./utils/newShows.json', '[]', function(err){ });
				}
			});
			copyFile(file2, file2Arch, function(err){ 
				if (!err) {
					console.log("Copy file2")
					fs.writeFile('./utils/newMovies.json', '[]', function(err){ });
				}
			});

			res.send( "End uploadJson"  )
			console.log("End uploadJson")

		}
		
		//initCache()
	}
}

//Download Images: (Need to have the Poster field filled. from OMDB)
exports.downloadImages = function(res,req){
	var download = function(uri, filename, callback){
	  request.head(uri, function(err, res, body){
	    //console.log('content-type:', res.headers['content-type']);
	    //console.log('content-length:', res.headers['content-length']);

	    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
	  });
	};
	function final(array, k){
		//var completed = array.filter(function(e) { return e.imdbID != undefined; })
		console.log(k + "|" + array.length)
		if ( k == array.length) {
			res.send(JSON.stringify(k))
		}
	};

	//get list of movies:
	ocdb.view('movies', 'byRef', function(err, body) {
		if (!err) {
			console.log("got movies")
			if (typeof body == "string") {
				var obj = JSON.parse(body)
			} else {
				var obj = body
			}
		   	var k = 0;
		   	obj.rows.forEach(function(mov){
		   		var dest = "./public/img/movies/" + mov.value.movieRef + ".jpg"
		   		console.log(mov.value.Poster + "  |  " + dest)
		   		if (mov.value.Poster != "N/A" && mov.value.Poster != undefined){
		   			// check if file already exist:
		   			fs.exists(dest, function(exists) {
					  	if (exists) {
					  		console.log(mov.value.Poster + ": already exist")
					  	} else {
					    	// download new file
						    download(mov.value.Poster, dest, function(){
								console.log('done: ' + mov.value.movieRef);
								k=k+1;
								final(obj.rows, k);
							});		
					  	}
					});
		   		} else{
		   			console.log('Skip: ' + mov.value.movie)
		   			k = k + 1;
		   			final(obj.rows, k);
		   		}
		   	})
			
		} else {
			res.send('error with db.get : ' + err)
		}
	});
};


//============================================================================
// Modify Already loaded Shows/Movies
//=============================================================================
//Amend movie details
exports.updateMovie = function(req, res){
	console.log('updateDocs')
	//get list of all docs
	ocdb.view('movies', 'byRef', function(err, bdy){
		if (!err) {
			console.log('all doc fetch')
			if (typeof bdy == "string"){
				var obj = JSON.parse(bdy)
			} else {
				var obj = bdy
			}
			//list of change:
			var changes = JSON.parse(fs.readFileSync('./utils/changesMovie.json', 'utf8'));

			var k = 0
			console.log("changes: " + changes.length)

			//find movie in collection
			changes.forEach(function(ch){
				var filt = obj.rows.filter(function(e) { 
					return e.value.movieRef == ch.movieRef; }
					//return e.value.idRef == ch.idRef; }
				)[0];

				if (filt.hasOwnProperty("value")){
					var movie = filt.value
					var name = ch.key
					//movie[name] = ch.val
					movie.trailer = ch.val
					movie.shows = [];
					console.log(JSON.stringify(movie.movieRef) + " ok")
					updateDoc(movie, movie._id, function(){
						k = k + 1
						final2(changes,k)
					})
				} else{
					console.log('Error: ' + JSON.stringify(e))
				};
			})
			
			//res.send(bdy)
		} else {
			res.send('error with db.get : ' + err)
		}
	});
	//Update SHOWS
	// ocdb.view('shows', 'byRef', function(err, bdy){
	// 	if (!err) {
	// 		console.log('all doc fetch')
	// 		if (typeof bdy == "string"){
	// 			var obj = JSON.parse(bdy)
	// 		} else {
	// 			var obj = bdy
	// 		}
	// 		//list of change:
	// 		var changes = JSON.parse(fs.readFileSync('./utils/Changes.json', 'utf8'));

	// 		var k = 0
	// 		console.log("changes: " + changes.length)
	// 		//find movie in collection
	// 		changes.forEach(function(ch){
	// 			//var filter = obj.rows.filter(function(e) { return e.value.movieRef == ch.movieRef; })

	// 			var filt = obj.rows.filter(function(e) { 
	// 				//return e.value.movieRef == ch.movieRef; }
	// 				return e.value.idRef == ch.idRef; }

	// 			)[0]
	// 			if (filt.hasOwnProperty("value")){
	// 				var movie = filt.value
	// 				var name = ch.key
	// 				movie[name] = ch.val

	// 				console.log(JSON.stringify(movie.idRef) + " ok")
	// 				updateDoc(movie, movie._id, function(){
	// 					k = k + 1
	// 					final2(changes,k)
	// 				})
	// 			} else{
	// 				console.log('Error: ' + JSON.stringify(e))
	// 			};
	// 		})
			
	// 		//res.send(bdy)
	// 	} else {
	// 		res.send('error with db.get : ' + err)
	// 	}
	// });
	function final2(array, k){
		//var completed = array.filter(function(e) { return e.imdbID != undefined; })
		console.log(k + "|" + array.length)
		if ( k == array.length) {
			res.send(JSON.stringify(k))
			console.log('Done....')
		}
	};
};

//add img key to movie
exports.updateMovieImg = function(req, res){
	console.log('updateDocs')
	//get list of all docs
	ocdb.view('movies', 'byRef', function(err, bdy){
		if (!err) {
			console.log('all doc fetch')
			if (typeof bdy == "string"){
				var obj = JSON.parse(bdy)
			} else {
				var obj = bdy
			}

			var movies = obj.rows 
			var k=0
			//find movie in collection
			movies.forEach(function(mv){
				var movie = mv.value
				movie.img = "./img/movies/"+ movie.movieRef +'.jpg'
				updateDoc(movie, movie._id, function(){
						k = k + 1
						final2(movies,k)
				 		console.log(JSON.stringify(movie.movieRef) + " ok")

				})
			})
			
			//res.send(bdy)
		} else {
			res.send('error with db.get : ' + err)
		}
	});

	function final2(array, k){
		//var completed = array.filter(function(e) { return e.imdbID != undefined; })
		console.log(k + "|" + array.length)
		if ( k == array.length) {
			res.send(JSON.stringify(k))
			console.log('Done....')
		}
	};
};

//add img key to movie
exports.updateMovieStart = function(req, res){
	console.log('updateDocs')
	//get list of all docs
	ocdb.view('shows', 'all', function(err, bdy){
		if (!err) {
			console.log('all doc fetch')
			if (typeof bdy == "string"){
				var obj = JSON.parse(bdy)
			} else {
				var obj = bdy
			}

			var shows = obj.rows 
			var k=0
			//find movie in collection
			shows.forEach(function(mv){
				var show = mv.value
				if (show.movieTime) { 
					show.movieStart = show.movieTime.slice(0); 
					console.log() 
					delete show.movieTime
					console.log(JSON.stringify(show.idRef) + " ok " + show.movieStart)
					updateDoc(show, show._id, function(){
						k = k + 1
						final2(shows,k)
				 		console.log(JSON.stringify(show.movieRef) + " ok")
					});
				} else if (!show.movieStart){
					console.log(show.idRef + " issue")
				};
				// if (show.gatesOpen){
				// 	show.gateOpen = show.gatesOpen.slice(0);
				// 	delete show.gatesOpen;
				// 	console.log(show.idRef + " " + show.gateOpen);
				// 	updateDoc(show, show._id, function(){
				// 		k = k + 1
				// 		final2(shows,k)
				//  		console.log(show.idRef + " " + show.venueRef + " ok")
				// 	});
				// }
			})
			
			//res.send(bdy)
		} else {
			res.send('error with db.get : ' + err)
		}
	});

	function final2(array, k){
		//var completed = array.filter(function(e) { return e.imdbID != undefined; })
		console.log(k + "|" + array.length)
		if ( k == array.length) {
			res.send(JSON.stringify(k))
			console.log('Done....')
		}
	};
};


//update venues
exports.updateVenues = function(req, res){
	console.log('updateDocs')
	//get list of all docs
	ocdb.view('venues', 'byRef', function(err, bdy){
		if (!err) {
			console.log('all doc fetch')
			if (typeof bdy == "string"){
				var obj = JSON.parse(bdy)
			} else {
				var obj = bdy
			}

			var venues = obj.rows 

			//list of change:
			var changes = JSON.parse(fs.readFileSync('./utils/changesVenues.json', 'utf8'));

			var k = 0
			console.log("changes: " + changes.length)

			//find venue in collection
			changes.forEach(function(ch){
				var filt = venues.filter(function(e) { return e.value.venueRef == ch.venueRef; })[0];
				var venue = filt.value
				for (var attrname in ch) { venue[attrname] = ch[attrname]; }
				delete venue.gps;
				delete venue.food;
				delete venue.url;
				//send to Db
				updateDoc(venue, venue._id, function(){
						k = k + 1
						final2(venues,k)
				 		console.log(JSON.stringify(venue.venueRef) + " ok")
				});
			});
			
			//res.send(bdy)
		} else {
			res.send('error with db.get : ' + err)
		}
	});

	function final2(array, k){
		//var completed = array.filter(function(e) { return e.imdbID != undefined; })
		console.log(k + "|" + array.length)
		if ( k == array.length) {
			res.send(JSON.stringify(k))
			console.log('Done....')
		}
	};
};

//Amend Show details
exports.updateShow = function(req, res){
	console.log('updateDocs')
	//get list of all docs
	ocdb.view('shows', 'byRef', function(err, bdy){
		if (!err) {
			console.log('all doc fetch')
			if (typeof bdy == "string"){
				var obj = JSON.parse(bdy)
			} else {
				var obj = bdy
			}
			//list of change:
			var changes = JSON.parse(fs.readFileSync('./utils/changesShow.json', 'utf8'));

			var k = 0
			console.log("changes: " + changes.length)

			//find movie in collection
			changes.forEach(function(ch){
				var filt = obj.rows.filter(function(e) { 
					//return e.value.movieRef == ch.movieRef; }
					return e.value.idRef == ch.idRef; }
				)[0];

				if (filt.hasOwnProperty("value")){
					var showObj = filt.value
					var name = ch.key
					showObj.startDay = ch.startDate
					showObj.endDay = ch.endDate
					//movie[name] = ch.val
					
					//console.log(JSON.stringify(movie.movieRef) + " ok")
					updateDoc(showObj, showObj._id, function(){
						k = k + 1
						final2(changes,k)
					})
				} else{
					console.log('Error: ' + JSON.stringify(e))
				};
			})
			
			//res.send(bdy)
		} else {
			res.send('error with db.get : ' + err)
		}
	});

	function final2(array, k){
		//var completed = array.filter(function(e) { return e.imdbID != undefined; })
		console.log(k + "|" + array.length)
		if ( k == array.length) {
			res.send(JSON.stringify(k))
			console.log('Done....')
		}
	};
};

//delete some Docs
exports.deleteDocs = function(req, res){
	console.log('deleteDocs')
	var array = ["1c9a5524060ff0833bb09db74bad0529", "bdfaffa1d652c7406df7f7e29e48f172",
	"c5101e4c7f3bae0abcad03c60a63534b", "a2f3851462a554eac70ad6ddf3e0c301",
	"bdfaffa1d652c7406df7f7e29ecaa739",
	"d5e563003c541fb250b67ed3b3bb5f3b",
	"bdfaffa1d652c7406df7f7e29e48f05d",
	"d5e563003c541fb250b67ed3b3bb6865",
	"4e582876ca2fd46a60bf3bf8074f16ca",
	"723d8c7d16e7fde20ae362a0aea991e7",
	"a2f3851462a554eac70ad6ddf3e0b459",
	"d5e563003c541fb250b67ed3b33a48a1"	,
	"c8173743736d2870f87c65f5d1251b21",
	"ae4e373ea91bab71796d15411358fff9",
	"aa424ac912ba8a60736403edfe1f23a5"
	]
	var k = 0;
	array.forEach(function(item,index){

		ocdb.get(item, { revs_info: true }, function(err, body){
			if (!err) {
				deleteDoc(item, body._rev , function(){
					k=k+1
					final2(array, k)
				});
			}
		});
	});

	function final2(array, k){
		
		console.log(k + "|" + array.length)
		if ( k == array.length - 1) {
			res.send(JSON.stringify(k))
		}
	};
};

//============================================================================
// Get All Docs
//=============================================================================
exports.getAllMovies = function(req, res) {
	console.log('getAllMovies ' + allMovies_Reduced.length );
	ocdb.view('movies', 'byTitle', function(err, body) {
		if (!err) {
			res.send(body)
		} else {
			res.send('error with db.get : ' + err)
		}
	});	
};
exports.getAllVenues = function(req, res) {
	console.log('getAllVenues');
	ocdb.view('venues', 'byRef', function(err, body) {
			if (!err) {
				res.send(body)
			} else {
				res.send('error with db.get : ' + err)
			}
	});
};
exports.getAllShows = function(req, res) {
	function max(d){
		d.sort(function(a, b){return parseInt(b.value.idRef) - parseInt(a.value.idRef)});
		return d
    };
	console.log('getAllShows');
	ocdb.view('shows', 'all', function(err, body) {
		if (!err) {
			max(body.rows);
			res.send(body)
		} else {
			res.send('error with db.get : ' + err)
		}
	});
};

/////////////////////////////////////////////////////////////////////////////////////
// Get movies from Cache
exports.getMovies = function(req, res) {
	console.log('getMovies ' + allMovies_Reduced.length );
	if (allMovies_Reduced != null ) { //
		//console.log('cacheMov')
		res.send(allMovies_Reduced)
	} else {
		//======= CURENTLY USELESS ...
		ocdb.view('movies', 'byTitle', function(err, body) {
			if (!err) {
				res.send(body)
			} else {
				res.send('error with db.get : ' + err)
			}
		});
	}
};
exports.getVenues = function(req, res) {
	console.log('getVenues');
	if (allVenues != null ) { //
		res.send(allVenues)
	} else {
	//=======
		ocdb.view('venues', 'byRef', function(err, body) {
			if (!err) {
				res.send(body)
			} else {
				res.send('error with db.get : ' + err)
			}
		});
	}
};
exports.getShows = function(req, res) {
	console.log('getShows');
	if (allShows_Reduced != null) { //
		res.send(allShows_Reduced)
	} else {
		//======= CURENTLY USELESS ...
		ocdb.view('shows', 'all', function(err, body) {
			if (!err) {
				res.send(body)
			} else {
				res.send('error with db.get : ' + err)
			}
		});
	}
};
exports.postFeedback = function(req,res){
	//console.log(req)
	console.log('postFeedback');
	var email = req.body.email
	var message = req.body.text
	console.log(email)
	// send email to myself
	// create reusable transport method (opens pool of SMTP connections)
	var smtpTransport = nodemailer.createTransport({
		    // service: "Gmail",
		    // auth: {
		    //     user: "epicsepics@gmail.com",
		    //     pass: "epicspass"
		    // }
		    service: "Mailgun",
		    auth: {
		        user: "postmaster@sandboxefbd444a4c6c467c8c06c5b70faf2511.mailgun.org",
		        pass: "90b0a509a29a6970a23a7f88afa7fc63"
		    }
		});

	// setup e-mail data with unicode symbols
	var mailOptions = {
		    from: email, // sender address
		    //to: "bar@blurdybloop.com, baz@blurdybloop.com", // list of receivers
		    to: "sylvaincau31@gmail.com",
		    subject: "my Outdoor Cinema - feedback", // Subject line
		    text: message, // plaintext body
		    // html: 	"Hey "+ firstname + "! <br> <br> Sorry to hear you lost your password. <br> <br>" + 
		    // 		"<b> your password is: " + pwd + "</b> <br>" +
		    // 		"<br>" + 
		    // 		"Thanks for using Epics! <br>" +
		    // 		"<br> The Epics Team" +
		    // 		"" // html body

		}

	// send mail with defined transport object
	smtpTransport.sendMail(mailOptions, function(error, info){
		    if(error){
		        console.log(error);
		        res.send('{"error":1, "message":"Sorry there was an issue. please try again later"}');

		    } else {
		        console.log("Message sent"); //JSON.stringify(info));
		    	res.send('{"error":0, "message":"ok"}');
		    	//res.send('{"error":0}');

		    }

		    // if you don't want to use this transport object anymore, uncomment following line
		    smtpTransport.close(); // shut down the connection pool, no more messages
	});
};

// initCache(function(result){
// 		console.log('InitCache : number of Movies ' + result);
// });
exports.initCache = function(req,res){
	initCache(function(result){
		res.send('result: ' + result)
	});
	//res.send('Init Cache server...')
};

exports.initCacheRestart = function(){
	initCache(function(result){
		console.log('result: ' + result)
	});
	//res.send('Init Cache server...')
};

function initCache(callback){
	var d = new Date();
	var callbackStatus = [];
	var total_callbackStatus = 3; //nombre de request
	//reset Reduced collection
	allMovies_Reduced = {rows:[]};
	allShows_Reduced = {rows:[]};
	
	//Request Shows that are in the future:
	ocdb.view('shows', 'all', function(err, body) {
		if (!err) {
			allShows = body
			console.log('cacheShows: ' + d)

			//select shows in the future:  // Should be done at the database level ... TODO
			allShows.rows.forEach(function(show){
				var showEndDay = new Date(show.value.endDay)
				showEndDay.setDate(showEndDay.getDate() + 7)
				//console.log(d + ' ==== ' + show.value.endDay)
				if (showEndDay > d){
					allShows_Reduced.rows.push(show)
				}
			});
			console.log('allShows_Reduced.rows.length' + allShows_Reduced.rows.length)

			callbackStatus.push({status: "OK - allShows_Reduced.rows.length", bdy : '' })
			final();
		} else {
			//res.send('error with db.get : ' + err)
		}
	});

	//request all movies
	ocdb.view('movies', 'byTitle', function(err, body) {
		if (!err) {
			allMovies = body;
			console.log('cacheMovies: ' + d);
			callbackStatus.push({status: "OK", bdy : '' })
			final();
		} else {
			//res.send('error with db.get : ' + err)
		}
	});

	//request all venues
	ocdb.view('venues', 'byRef', function(err, body) {
		if (!err) {
		    allVenues = body;
			console.log('cacheVenues: ' + d);
			callbackStatus.push({status: "OK", bdy : '' })
			final();
		} else {
			//res.send('error with db.get : ' + err)
		}
	});

	function final(err){  // NOK status is not treated ......
			// console
	  // 		//check if one is not ok
			// var obj = callbackStatus.filter(function ( obj ) {
			//     return obj.status === "NOK";
			// })[0];

			// if (obj == undefined) {
			// 	final(callbackStatus)
			// } else{
			// 	console.log(index)
			// }
		if (!err && (callbackStatus.length == total_callbackStatus) ){
			console.log('ok')

			//setup allMovies_Reduced
			allMovies_Reduced = {rows:[]};
			//go through shows and select movies:
			allShows_Reduced.rows.forEach(function(show){
				var movieRef = show.value.movieRef;
				//console.log('? ' +movieRef )
				//check if movie already in allMovies_Reduced collection
				var filt = allMovies_Reduced.rows.filter(function(e) { 
					return e.value.movieRef == movieRef; })[0];
				//console.log('?filt ' + filt )

				if (filt != undefined && filt.hasOwnProperty("value")){
					// already in the new collection // do nothing
				} else {
					// add to new database
					//console.log('add2databse: '+ movieRef)
					//find movie in allMovies
					var movie = allMovies.rows.filter(function(e) { 
						return e.value.movieRef == movieRef; })[0];

					//clean movie
					delete movie.Poster;
					delete movie.imdbID;
					delete movie.imdbRating;
					delete movie.imdbVotes;
					delete movie.Type;
					delete movie.Response;

					//and add movie to collection
					allMovies_Reduced.rows.push(movie) 
				}
			});
			console.log(allMovies_Reduced.rows.length)
			allMovies_Reduced.total_rows = allMovies_Reduced.rows.length;
			callback(allMovies_Reduced.rows.length);

		} else if (err){
			callback(err)
		} else {

		}
	};
};

function c(val){
    if (typeof val == "String"){
        console.log(val)
    } else{
        console.log(JSON.stringify(val))
    }
};

//====================================================
// Fiddle 
//====================================
exports.test = function(req, res) {
 //    c("test");
	// child_process.spawnSync(initCache())
	// child_process.spawnSync(console.log('toto-----------'))	
	ocdb.view('movies', 'byRef', function(err, body) {
			if (!err) {
				//allMovies = body;
				
				res.send(body.rows)
			} else {
				c('error with db.get : ' + err)
			}
		});
};


function complex(){
		initCache(function(result){
			console.log('InitCache -------------- ' + result);
		});	
		c('tat===========')
}
function stop(){ //need  callback
  // var rl = readline.createInterface({
  //   input: process.stdin,
  //   output: process.stdout
  // });

  // rl.question("Continue ?", function(answer) {
  //   if (answer == "no"){
  //   	return;
  //   } else {
  //   	c(" ==> continue")
  //   }
  //   rl.close();
  // });
}