var express = require('express')
  , connect = require('connect')
  , request = require("request")
  , async = require('async')
  , bcrypt = require('bcryptjs')
  , logger = require('morgan')
  , bodyParser = require('body-parser')
  , mongoose = require('mongoose')
  , nodemailer = require("nodemailer")
  , crypto = require('crypto')
  ;  

var fs = require('fs');
var jwt = require('jwt-simple');
var moment = require('moment');
//var path = require('path');

//** IMPORT CONSTANT 
var config = require('../config');

//Import Middelware
var ensureAuthenticated = require('./mw_ensureAuthenticated')

// Import MODEL
var modelUser = require('../model/User');
var User = modelUser.User;

module.exports = (function() {
    'use strict';
    var api = express.Router();

    /*
     |--------------------------------------------------------------------------
     | Login Required Middleware
     |--------------------------------------------------------------------------
     */
    // function ensureAuthenticated(req, res, next) {
    //   if (!req.headers.authorization) {
    //     return res.status(401).send({ message: 'Please make sure your request has an Authorization header' });
    //   }
    //   var token = req.headers.authorization.split(' ')[1];
    //   var payload = jwt.decode(token, config.TOKEN_SECRET);
    //   if (payload.exp <= moment().unix()) {
    //     return res.status(401).send({ message: 'Token has expired' });
    //   }
    //   req.user = payload.sub;
    //   next();
    // };

    /*
     |--------------------------------------------------------------------------
     | Generate JSON Web Token
     |--------------------------------------------------------------------------
     */
    function createJWT(user) {
      var payload = {
        sub: user._id,
        iat: moment().unix(),
        exp: moment().add(14, 'days').unix()
      };
      return jwt.encode(payload, config.TOKEN_SECRET);
    };

    /*
     |--------------------------------------------------------------------------
     | Send PWD in an email
     |--------------------------------------------------------------------------
     */
     function sendPWD(token, user, done){
        console.log('sendPWD to ' + user.email);

        // create reusable transport method (opens pool of SMTP connections)
        var smtpTransport = nodemailer.createTransport({
              service: "Mailgun",
              auth: {
                  user: "postmaster@sandboxefbd444a4c6c467c8c06c5b70faf2511.mailgun.org",
                  pass: "90b0a509a29a6970a23a7f88afa7fc63"
              }
          });

        var message = 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
          'http://' + req.headers.host + '/reset/' + token + '\n\n' +
          'If you did not request this, please ignore this email and your password will remain unchanged.\n'

        // setup e-mail data with unicode symbols
        var mailOptions = {
              from: "noreply@myoutdoorcinema.com.au", // sender address
              //to: "bar@blurdybloop.com, baz@blurdybloop.com", // list of receivers
              to: email,
              subject: "my Outdoor Cinema - login", // Subject line
              text: message 
          }

        // send mail with defined transport object
        smtpTransport.sendMail(mailOptions, function(error, info){
              if(error){
                  console.log(error);
                  return res.status(404).send({msg:"Sorry there was an issue. please try again later"});

              } else {
                  console.log("Message sent"); //JSON.stringify(info));
                  done(err, 'done');
              }
              // if you don't want to use this transport object anymore, uncomment following line
              smtpTransport.close(); // shut down the connection pool, no more messages
        });
     };

    /*
     |--------------------------------------------------------------------------
     | GET /api/me
     |--------------------------------------------------------------------------
     */
    api.get('/api/me', ensureAuthenticated, function(req, res) {
      console.log("search for user: " + JSON.stringify(req.user))
      User.findById(req.user, function(err, user) {
        res.send(user);
      });
    });

    /*
     |--------------------------------------------------------------------------
     | PUT /api/me
     |--------------------------------------------------------------------------
     */
    api.put('/api/me', ensureAuthenticated, function(req, res) {
      User.findById(req.user, function(err, user) {
        if (!user) {
          return res.status(400).send({ message: 'User not found' });
        }
        user.displayName = req.body.displayName || user.displayName;
        user.email = req.body.email || user.email;
        user.save(function(err) {
          res.status(200).end();
        });
      });
    });


    /*
     |--------------------------------------------------------------------------
     | Log in with Email
     |--------------------------------------------------------------------------
     */
    api.post('/auth/login', function(req, res) {
      User.findOne({ email: req.body.email }, '+password', function(err, user) {
        if (!user) {
          return res.status(401).send({ message: 'Invalid email and/or password' });
        }
        user.comparePassword(req.body.password, function(err, isMatch) {
          if (!isMatch) {
            return res.status(401).send({ message: 'Invalid email and/or password' });
          }
          res.send({ token: createJWT(user) });
        });
      });
    });

    /*
     |--------------------------------------------------------------------------
     | Create Email and Password Account
     |--------------------------------------------------------------------------
     */
    api.post('/auth/signup', function(req, res) {
      User.findOne({ email: req.body.email }, function(err, existingUser) {
        if (existingUser) {
          return res.status(409).send({ message: 'Email is already taken - you might be already logged in with Facebook or Google+' });
          // TO DO Client to handle this.
        }
        var user = new User({
          displayName: req.body.displayName,
          email: req.body.email,
          password: req.body.password
        });
        user.save(function() {
          res.send({ token: createJWT(user) });
        });
      });
    });


    /*
     |--------------------------------------------------------------------------
     | Retrieve PWD /api/fwd
     |--------------------------------------------------------------------------
     */
    api.post('/api/fpwd', function(req, res) {
      var token;
      async.waterfall([
        function(done) {
          crypto.randomBytes(20, function(err, buf) {
            token = buf.toString('hex');
            done(err, token);
          });
        },
        function(token, done) {
          User.findOne({ email: req.body.email }, function(err, user) {
            if (!user) {
              var msg = 'User does not exist'
              //console.log(msg)
              return res.status(404).send({msg: msg})
            }

            user.resetPasswordToken = token;
            user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

            user.save(function(err) {
              done(err, token, user);
            });
          });
        },
        //sendPWD(token, user, done)
        function(token, user, done) {
          console.log('sendPWD to ' + user.email);

          // create reusable transport method (opens pool of SMTP connections)
          var smtpTransport = nodemailer.createTransport({
                service: "Mailgun",
                auth: {
                    user: "postmaster@sandboxefbd444a4c6c467c8c06c5b70faf2511.mailgun.org",
                    pass: "90b0a509a29a6970a23a7f88afa7fc63"
                }
          });

          var message = 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
            'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
            'http://' + req.headers.host + '/reset/' + token + '\n\n' +
            'If you did not request this, please ignore this email and your password will remain unchanged.\n' + 
            'This link will expire in 1 hour'

          // setup e-mail data with unicode symbols
          var mailOptions = {
                from: "noreply@myoutdoorcinema.com.au", // sender address
                to: user.email,
                subject: "my Outdoor Cinema - login", // Subject line
                text: message 
          }

          // send mail with defined transport object
          smtpTransport.sendMail(mailOptions, function(err, info){
                if(err){
                    console.log(err);
                    res.status(404).send({msg:"Sorry there was an issue. please try again later"});

                } else {
                    console.log("Message sent"); //JSON.stringify(info));
                    done(err, 'done');
                }
                // if you don't want to use this transport object anymore, uncomment following line
                smtpTransport.close(); // shut down the connection pool, no more messages
          });
        }
        ], function(err) {
          if (err) return res.status(500).send({msg:"Sorry there was an issue. please try again later"});
			 
          var msg = 'User identified. An email has been sent to you. Please check your mailbox.'
          return res.status(200).send({msg: msg});
      });
    });

    api.put('/api/resetPwd', function(req, res) {
      console.log('resetPwd '+ req.body.token )
      async.waterfall([
          function(done) {
            User.findOne({ resetPasswordToken: req.body.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
              if (!user) {
                var msg = 'Password reset token is invalid or has expired.';
                return res.status(150).send({msg: msg});
              }

              user.password = req.body.password;
              user.resetPasswordToken = undefined;
              user.resetPasswordExpires = undefined;

              user.save(function(err) {
                done(err, user);
              });
            });
          },
          function(user, done) {
            var smtpTransport = nodemailer.createTransport({
                  service: "Mailgun",
                  auth: {
                      user: "postmaster@sandboxefbd444a4c6c467c8c06c5b70faf2511.mailgun.org",
                      pass: "90b0a509a29a6970a23a7f88afa7fc63"
                  }
            });

            var mailOptions = {
              to: user.email,
              from: 'noreply@myoutdoorcinema.com.au',
              subject: 'Your password has been changed',
              text: 'Hello,\n\n' +
                'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
            };
            smtpTransport.sendMail(mailOptions, function(err) {
              var msg = 'Success! Your password has been changed.';
              done(err);
            });
          }
        ], function(err) {
          if (err) { return res.status(404).send(err); }

          var msg = 'Success! Your password has been changed.';
          return res.status(200).send({msg: msg});

        });
    });

    /*
     |--------------------------------------------------------------------------
     | Login with Google
     |--------------------------------------------------------------------------
     */
    api.post('/auth/google', function(req, res) {
      var accessTokenUrl = 'https://accounts.google.com/o/oauth2/token';
      var peopleApiUrl = 'https://www.googleapis.com/plus/v1/people/me/openIdConnect';
      var params = {
        code: req.body.code,
        client_id: req.body.clientId,
        client_secret: config.GOOGLE_SECRET,
        redirect_uri: req.body.redirectUri,
        grant_type: 'authorization_code'
      };

      // Step 1. Exchange authorization code for access token.
      request.post(accessTokenUrl, { json: true, form: params }, function(err, response, token) {
        var accessToken = token.access_token;
        var headers = { Authorization: 'Bearer ' + accessToken };

        // Step 2. Retrieve profile information about the current user.
        request.get({ url: peopleApiUrl, headers: headers, json: true }, function(err, response, profile) {

          //console.log(JSON.stringify(response))
          //console.log(JSON.stringify("======================"))
          //console.log(JSON.stringify(profile))

          // Step 3a. Link user accounts.
          if (req.headers.authorization) {

            User.findOne({ google: profile.sub }, function(err, existingUser) {
              if (existingUser) {
                return res.status(409).send({ message: 'There is already a Google account that belongs to you' });
              }
              var token = req.headers.authorization.split(' ')[1];
              var payload = jwt.decode(token, config.TOKEN_SECRET);
              User.findById(payload.sub, function(err, user) {
                if (!user) {
                  return res.status(400).send({ message: 'User not found' });
                }
                user.google = profile.sub;
                user.picture = user.picture || profile.picture.replace('sz=50', 'sz=200');
                user.displayName = user.displayName || profile.name;
                user.email = profile.email;
                user.save(function(err) {
                  if (err){console.log("Google1: error saving new user: " + err ); return res.status(400).send({ message: 'Sorry an error occured. PLease try agin later'});}
                  var token = createJWT(user);
                  res.send({ token: token });
                });
              });
            });
          } else {
            // Step 3b. Create a new user account or return an existing one.
            User.findOne({ google: profile.sub }, function(err, existingUser) {
              if (existingUser) {  
                return res.send({ token: createJWT(existingUser) });
              }

              User.findOneAndUpdate(query, update, options, function(err, existingUser2) {  
                if (existingUser2) {
                  var token = createJWT(existingUser2);
                  return res.send({ token: token });
                }

                var user = new User();
                user.google = profile.sub;
                user.picture = profile.picture.replace('sz=50', 'sz=200');
                user.displayName = profile.name;
                user.email = profile.email;
					 user.save(function(err) {
                  if (err){
							console.log("Google2: error saving new user: " + err ); 
							return res.status(500).send({ message: 'Sorry an error occured. PLease try agin later'});
						}
                  var token = createJWT(user);
                  res.send({ token: token });
                });
              });
            });
          }
        });
      });
    });


    /*
     |--------------------------------------------------------------------------
     | Login with Facebook
     |--------------------------------------------------------------------------
     */
    api.post('/auth/facebook', function(req, res) {
      var accessTokenUrl = 'https://graph.facebook.com/v2.3/oauth/access_token';
      var graphApiUrl = 'https://graph.facebook.com/v2.3/me';
      var params = {
        code: req.body.code,
        client_id: req.body.clientId,
        client_secret: config.FACEBOOK_SECRET,
        redirect_uri: req.body.redirectUri
      };

      // Step 1. Exchange authorization code for access token.
      request.get({ url: accessTokenUrl, qs: params, json: true }, function(err, response, accessToken) {
        if (response.statusCode !== 200) {
          return res.status(500).send({ message: accessToken.error.message });
        }

        // Step 2. Retrieve profile information about the current user.
        request.get({ url: graphApiUrl, qs: accessToken, json: true }, function(err, response, profile) {
          if (response.statusCode !== 200) {
            return res.status(500).send({ message: profile.error.message });
          }
          //console.log("facebook profile: "+ JSON.stringify(profile))

          if (req.headers.authorization) { //If user has send headers - means it is a returning user.
            console.log("ok1")
            User.findOne({ facebook: profile.id }, function(err, existingUser) { //check user in database to check if already registered.
              if (existingUser) {
                console.log("find existing user")
                return res.status(409).send({ message: 'There is already a Facebook account that belongs to you.' });
              }
              var token = req.headers.authorization.split(' ')[1];
              var payload = jwt.decode(token, config.TOKEN_SECRET);
              User.findById(payload.sub, function(err, user) {
                            console.log("ok2")

                if (!user) {
                  return res.status(400).send({ message: 'User not found' });
                }
                user.facebook = profile.id;
                user.picture = user.picture || 'https://graph.facebook.com/v2.3/' + profile.id + '/picture?type=large';
                user.displayName = user.displayName || profile.name;
                user.email = profile.email;

                console.log(user.email + "||" + profile.email);

                user.save(function(err) {
                  if (err){
							console.log("facebook login1 : " + err)
							return res.status(500).send({ message: 'Sorry an error occured. PLease try agin later'})
						}
                  var token = createJWT(user);
                  res.send({ token: token });
                });
              });
            });
          } else {
            // Step 3b. Create a new user account or return an existing one.
            User.findOne({ facebook: profile.id }, function(err, existingUser) {
              //console.log("User.findOne facebook=profile.id")
              if (existingUser) {
                var token = createJWT(existingUser);
                return res.send({ token: token });
              }
              //search for similar email and merge account
              var query = {email: profile.email};
              var pict = 'https://graph.facebook.com/' + profile.id + '/picture?type=large';
              var update = {facebook: profile.id, displayName: profile.name, picture : pict};
              var options = {new: true};
              //console.log(update)
              User.findOneAndUpdate(query, update, options, function(err, existingUser2) {  
                if (existingUser2) {
                  var token = createJWT(existingUser2);
                  return res.send({ token: token });
                }

                var user = new User();
                user.facebook = profile.id;
                user.picture = 'https://graph.facebook.com/' + profile.id + '/picture?type=large';
                user.displayName = profile.name;
                user.email = profile.email;
                console.log(user.email + "||" + profile.email);

                user.save(function(err) {
                  if (err){
							console.log("facebook login2 : " +err);
							return res.status(500).send({ message: 'Sorry an error occured. PLease try agin later'});
						}
                  var token = createJWT(user);
                  res.send({ token: token });
                });
              });
            });
          }
        });
      });
    });

    /*
     |--------------------------------------------------------------------------
     | Login with Windows Live
     |--------------------------------------------------------------------------
     */
    api.post('/auth/live', function(req, res) {
      async.waterfall([
        // Step 1. Exchange authorization code for access token.
        function(done) {
          var accessTokenUrl = 'https://login.live.com/oauth20_token.srf';
          var params = {
            code: req.body.code,
            client_id: req.body.clientId,
            client_secret: config.WINDOWS_LIVE_SECRET,
            redirect_uri: req.body.redirectUri,
            grant_type: 'authorization_code'
          };
          request.post(accessTokenUrl, { form: params, json: true }, function(err, response, accessToken) {
            done(null, accessToken);
          });
        },
        // Step 2. Retrieve profile information about the current user.
        function(accessToken, done) {
          var profileUrl = 'https://apis.live.net/v5.0/me?access_token=' + accessToken.access_token;
          request.get({ url: profileUrl, json: true }, function(err, response, profile) {
            done(err, profile);
          });
        },
        function(profile) {
          console.log(JSON.stringify(profile))
          // Step 3a. Link user accounts.
          if (!req.headers.authorization) {
            User.findOne({ live: profile.id }, function(err, user) {
              if (user) {
                return res.send({ token: createJWT(user) });
              }
              var newUser = new User();
              newUser.live = profile.id;
              newUser.displayName = profile.name;
              newUser.email = profile.email;
              newUser.save(function() {
                var token = createJWT(newUser);
                res.send({ token: token });
              });
            });
          } else {
            // Step 3b. Create a new user or return an existing account.
            User.findOne({ live: profile.id }, function(err, user) {
              if (user) {
                return res.status(409).send({ message: 'There is already a Windows Live account that belongs to you' });
              }
              var token = req.headers.authorization.split(' ')[1];
              var payload = jwt.decode(token, config.TOKEN_SECRET);
              User.findById(payload.sub, function(err, existingUser) {
                if (!existingUser) {
                  return res.status(400).send({ message: 'User not found' });
                }
                existingUser.live = profile.id;
                existingUser.displayName = existingUser.name;
                existingUser.email = profile.email;
                existingUser.save(function() {
                  var token = createJWT(existingUser);
                  res.send({ token: token });
                });
              });
            });
          }
        }
      ]);
    });


    /*
     |--------------------------------------------------------------------------
     | Unlink Provider
     |--------------------------------------------------------------------------
     */
    api.get('/auth/unlink/:provider', ensureAuthenticated, function (req, res) {
      var provider = req.params.provider;
      User.findById(req.user, function(err, user) {
        if (!user) {
          return res.status(400).send({ message: 'User not found' });
        }
        user[provider] = undefined;
        user.save(function() {
          res.status(200).end();
        });
      });
    });
    
		
    return api;
})();