"use strict";

//********************
// Import MODEL
var User = require('../model/User');
// var modelUser = require('../model/User');
// var User = modelUser.User;
// var Reminder = modelUser.Reminder;

var model = require('../model/ShowMoviesVenues');
var Venue = model.Venue;
var Movie = model.Movie;
var Show = model.Show;

var modelC = require('../model/Comments');
var Comment = modelC.Comment;


// function movieWithAllComments(id) {
//     var movie = Movie.findOne({_id: id})
//     movie.comments = Comment.find({postId: id}).toArray()
//     return movie
// }

// function addComment(postId, comment) {
//     comment.created = Date.now()
//     comment.postId = postId
//     db.comments.save(comment)
// }

exports.addComment = function(req, res){
	console.log(req.body.parentId + " // " + req.body.userId + " // " + req.body.text )
	console.log(JSON.stringify(req.body) )

	var comment = new Comment({
  		parentId: req.body.parentId,
  		text: req.body.text,
  		user: req.body.userId,
  		created: Date.now()
	});

	comment.save(function(err) {
		if (err) {
			return res.status(500).send(err);
		} else {
		   res.send({message: "Comment saved!"});	
		}

    });
}


exports.saveComment = function(req, res){
	console.log(req.body.parentId + " // " + req.body.userId + " // " + req.body.text )
	//console.log(JSON.stringify(req.body) )

	Comment.findByIdAndUpdate(req.body._id, { text: req.body.text }, function(err) {
		if (err) {
			return res.status(500).send(err);
		} else {
			console.log("ok")
		   res.send({message: "Comment saved!"});	
		}

    });
}

exports.deleteComment = function(req, res){
	//console.log(req.body.parentId + " // " + req.body.userId + " // " + req.body.text )
	//console.log(JSON.stringify(req.body) )
	console.log("deleteComment: " + req.body._id)

	Comment.findByIdAndRemove(req.body._id, function(err) {
		if (err) {
			console.log("deleteComment: " + err)
			return res.status(500).send(err);
		} else {
		   res.send({message: "Comment deleted!"});		
		}

    });
}

exports.getComments = function(req, res){
	//console.log(" getComments: parentId: " + req)
    Comment.find({parentId: req.query.parentId})
    		.populate({path: 'user', model: 'User', options: { sort: { created: -1 }} })
    		.exec(function(err, comments) {
				if (err){
					console.log(" getComments: parentId: " + req.query.parentId + err)
					return res.status(500).send(err);
				}
        		res.send({data:comments});
    });
}

exports.getMyComments = function(req, res){
	//console.log(" getMyComments: userId:" + req.query.userId)
    Comment.find({user: req.query.userId})
    		.populate({path: 'parentId', model: 'Movie', options: { sort: { created: -1 }} })
    		.exec(function(err, comments) {
				if (err){
					console.log(" getComments: parentId: " + req.query.parentId + err)
					return res.status(500).send(err);
				}
        		res.send({data:comments});
    });
}

//============================================================================
// Get All Docs
//=============================================================================


//====================================================
// Fiddle 
//====================================



// You can add the numerical milliseconds version of timestamp as a virtual attribute on the schema:

// schema.virtual('timestamp_ms').get(function() {
//   return this.timestamp.getTime();
// });
// Then you can enable the virtual field's inclusion in toObject calls on model instances via an option on your schema:

// var schema = new Schema({
//   timestamp: Date
// }, {
//   toObject: { getters: true }
// });