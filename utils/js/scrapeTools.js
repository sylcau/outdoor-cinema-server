//scrap website
"use strict";
var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var rp = require('request-promise');

//Done Kings Park
// exports.scrapeKingsPark = function(req, res){

// 	var url = 'http://www.moonlight.com.au/perth/program/';

// 	request(url, function(error, response, html){
// 	    if(!error){
// 	        var $ = cheerio.load(html);

		    
// 		    var json = { title : "", date : "", rating : ""};
// 		    var array= [];

// 		    $('article').each(function(){
		    	
// 		    	var title, date, rating;
// 		    	var data = $(this);
// 		    	date = data.children().first().text().trim() + " 2015"
		    	
// 		    	title = data.children(".desc").children('h3').contents().filter(function() { 
//                       //return !!$.trim(this.innerHTML||this.data);
//                       return this.nodeType === 3;
//                 }).first().text().trim();
		    	
// 		    	rating = data.find("span.rating").text()
		    	
// 		    	if(new Date(date) >= new Date("2015-03-01")){
// 		    		start = '7pm'
// 		    	} else{
// 		    		start = "8pm"
// 		    	}

// 		    	array.push({ movie : title, startDay : date, endDay: date,  rating: rating, venueId: "MooKin", gateOpen: "6pm", movieStart: start, language: "", subtitle: ""}); 
// 				console.log(title + " / " + rating + " / " + date);
// 				console.log('===============================')
// 		    })
// 		};

// 		// To write to the system we will use the built in 'fs' library.
// 		// In this example we will pass 3 parameters to the writeFile function
// 		// Parameter 1 :  output.json - this is what the created filename will be called
// 		// Parameter 2 :  JSON.stringify(json, null, 4) - the data to write, here we do an extra step by calling JSON.stringify to make our JSON easier to read
// 		// Parameter 3 :  callback function - a callback function to let us know the status of our function

// 		fs.writeFile('./utils/scrapedWeb-JSON/scrape.json', JSON.stringify(array, null, 4), function(err){

// 		    console.log('File successfully written! - Check your project directory for the output.json file');

// 		})

// 		// Finally, we'll just send out a message to the browser reminding you that this app does not have a UI.
// 		res.send('File successfully written! - Check your project directory for the output.json file')
// 	});
// };

// // Done Luna Leederville (LunaLee) & Camelot Mosman Park CamMos 20/03
exports.scrapeLuna = function(req, res){

	//var url = 'http://outdoor.lunapalace.com.au/';
	//var venue = "LunLee";

	var url = 'http://camelot.lunapalace.com.au/';
	var venue = "CamMos"

	var gateOpen = "";

	request(url, function(error, response, html){
	    if(!error){
	        var $ = cheerio.load(html);

		    
		    var json = { title : "", date : "", rating : ""};
		    var array = [];

		    // $("img").each(function(){
		    // 	title = data.attr("title")
		    // };

		    $('a[href^="select-film"]:not(:has(>img)) span').
		    	// filter(function(index){
		    	// 	return ('a[href^="select-film"]:has(>img)').
		    		each(function(){
		    	// var attr = $(this).prev().attr("title")
		    	// if (typeof attr !== typeof undefined && attr !== false) {
		    	// 	console.log("ok")
		    	// } else{
		  		 //    console.log("NNNNNNok")
  		
		    	// }
		    	console.log("ok")
		    	var title, date;
		    	var rating = "";
		    	var regExp, start;
		    	var data = $(this);

		    	//var href;
		    	//href = data.

		    	var tmp = data.text().trim();
		    	console.log(tmp)

		    	var select;

		    	regExp = /([^(]+)\(/;
		    	select = regExp.exec(tmp) //find "(" and select what is before
		    	if (select) {title = select[1].trim()} else {title = tmp }

		    	//rating
		    	regExp = /\(([^)]+)\)/; // find "(" and selct what is after until ")"
				select = regExp.exec(tmp);
		    	if (select) {rating = select[1].trim()}

		    	tmp = data.parent().parent().prev().prev().contents().text().trim()
		    	console.log(tmp)

		    	//select date
		    	regExp = /([^,]+)\,/; 
				select = regExp.exec(tmp);
				if (select){date = select[1].trim() + " 2015"}
		    	
		    	 
		    	regExp = /\,([^,]+)/;
				select = regExp.exec(tmp);
		    	if (select){start = select[1].trim()}


		    	// Doors open at 6:15 PM. From 19 March doors open 6PM.
		    	if(venue == "CamMos") {
		    		if (new Date(date) >= new Date("2015-03-19")) {
			    		gateOpen = '6:15pm'
			    	} else{
			    		gateOpen = "6pm"
			    	};
		    	};

		    	array.push({ movie : title, startDay : date, endDay: date,  rating: rating, venueId: venue, gateOpen: gateOpen, movieStart: start, language: "", subtitle: ""}); 
				console.log(title + " / " + rating + " / " + date);
				console.log('===============================')
		    })
		};

		// To write to the system we will use the built in 'fs' library.
		// In this example we will pass 3 parameters to the writeFile function
		// Parameter 1 :  output.json - this is what the created filename will be called
		// Parameter 2 :  JSON.stringify(json, null, 4) - the data to write, here we do an extra step by calling JSON.stringify to make our JSON easier to read
		// Parameter 3 :  callback function - a callback function to let us know the status of our function

		fs.writeFile('./utils/scrapedWeb-JSON/scrapeLuna.json', JSON.stringify(array, null, 4), function(err){

		    console.log('File successfully written! - Check your project directory for the output.json file');
		})

		// Finally, we'll just send out a message to the browser reminding you that this app does not have a UI.
		res.send('Check your console!')
	});
};

// //
// exports.scrapeRoofTop = function(req, res){
// 	console.log("scrape...")
// 	var url = 'https://www.rooftopmovies.com.au/program/';
// 	//var venue = "RooNor"
// 	var venueId = "554ed6bed617526c16816cf1"

// 	//var gateOpen = "6pm";

// 	request(url, function(error, response, html){
// 	    if(!error){
// 	        var $ = cheerio.load(html);

		    
// 		    var json = { title : "", date : "", rating : ""};
// 		    var array = [];
// 		    var k = $('#content').children().length;
// 		    $('#content').children().each(function(){

// 		    	console.log("ok")
// 		    	var title, date,gateOpen;
// 		    	var rating = "";
// 		    	var regExp, start;
// 		    	var data = $(this);
// 		    	var url1;
// 		    	start = "8:00 PM";

// 				date = data.children('h3').contents('a').text().trim() + " 2015";
// 		    	date.trim();

// 		    	title = data.children('h4').contents('a').text().trim();
// 		    	title.trim();
//  				if (title.indexOf("(Encore Screening)") > -1) {
//  					title = title.slice(0,-18);
//  				};
// 		    	url1 = data.children('.event_image').attr('href');
// 				console.log(title + " / " + rating + " / " + date + " / " + url1 );


// 				// Doors open at xx PM. 
// 		    	//if(venue == "RooNor") {
// 		    		if (new Date(date) >= new Date("2015-03-19")) {
// 			    		gateOpen = "6:00 PM"
// 			    	} else{
// 			    		gateOpen = "6:00 PM"
// 			    	};
// 		    	//};

//     			array.push({ movie : title, startDay : date, endDay: date,  rating: rating, venueId: venueId, gateOpen: gateOpen, movieStart: start, language: "", subtitle: ""}); 
//     			console.log(title + " / " + rating + " / " + date );
// 				console.log('===============================');


// 				final(array,k);

// 				//need to use PhantomJS
// 		    // 	request(url1, function(error, response, html1){
// 		    // 		if(!error){
// 		    // 			var $ = cheerio.load(html1);
// 		    // 			var dat = $(this);
// 						// var title, date;
// 		    // 			var rating = "";
// 		    // 			var regExp;
// 		    // 			var start,gateOpen,trailer;


// 		    // 			console.log($('#resultsSS').length)
// 						// fs.writeFile('./utils/tmp.json', JSON.stringify($.html(), null, 4), function(err){
// 				  //   		console.log('////');
// 						// })
// 		    // 			//console.log("length  == " + dat.length)
// 		    // 			title = dat.children(':nth-child(2)').text().trim();
// 		    // 			//date = dat.children(".one_col.last_col:nth-child(2)").children(":nth-child(2)").content().text().trim()
// 		    // 			date = dat.find('div[id^="dates_"]').text().trim();
// 		    // 			//dates_0f144d6f-a09e-4571-a9b2-005bb46467a9
// 		    // 			gateOpen = dat.find('div[id^="doors_open"]').text().trim() || "6pm" ;
// 		    // 			start = dat.find('div[id^="time_"]').text().trim();
// 		    // 			trailer = dat.find(".yt").attr("href");

// 				  //      	// Doors open at xx PM. 
// 				  //   	if(venue == "RooNor") {
// 				  //   		if (new Date(date) >= new Date("2015-03-19")) {
// 					 //    		gateOpen = '6pm'
// 					 //    	} else{
// 					 //    		gateOpen = "6pm"
// 					 //    	};
// 				  //   	};

// 		    // 			array.push({ movie : title, startDay : date, endDay: date,  rating: rating, venueId: venue, gateOpen: gateOpen, movieStart: start, language: "", subtitle: ""}); 
// 		    // 			console.log(title + " / " + rating + " / " + date );
// 						// console.log('===============================');
// 						// final(array,k)

// 		    // 		}
// 		    // 	});

// 		    });
// 		};

		
// 		function final(array,k){
// 			if (array.length == k){
// 				fs.writeFile('./utils/scrapedWeb-JSON/scrapeRoofTop.json', JSON.stringify(array, null, 4), function(err){

// 				    console.log('File successfully written! - Check your project directory for the scrape.json file');
// 				})

// 				// Finally, we'll just send out a message to the browser reminding you that this app does not have a UI.
// 				res.send('Check your console!')
// 			}
// 		}

// 	});
// }

// // Mcdonalds Cinemas
// // get all shows from http://www.communitycinemas.com.au/Page/Print-Sessions and create simplify JSON
// exports.scrapeMcdo = function(req, res){
// 	console.log('scrapeMcdo....')
// 	//this is not a real scrape. from the json manually done, reformat file to macth scrape.json
// 	var mcDoCinemaProgram = JSON.parse(fs.readFileSync('./utils/scrapedWeb-JSON/McDoCinemaProgram.json', 'utf8'));
// 	var array=[];
	
// 	mcDoCinemaProgram.forEach(function(sh){
// 		var regExp, select;
// 		var show = {
// 	        "movie": "",
// 	        "startDay": "",
// 	        "endDay": "",
// 	        "rating": "",
// 	        "venueId": "",
// 	        "gateOpen": "",
// 	        "movieStart": "",
// 	        "language": "",
// 	        "subtitle": ""
//     	}

//   		//Separate movie title from rating:
//     	regExp = /([^(]+)\(/; //find "(" and select what is before 
//     	select = regExp.exec(sh.movie) 
//     	show.movie = select[1].trim() || "";
    	
//     	//Date => startDay = endDay
//     	show.startDay = sh.date.slice(0,-7).trim() + " 2015";
//     	show.endDay = show.startDay

//     	//rating
//     	regExp = /\(([^,]+)\,/; // find "(" and select what is after until ")"
// 		select = regExp.exec(sh.movie);
//     	if (select) {show.rating = select[1].trim()}

//     	//
//   		show.venueId = sh.venue;

//   		show.gateOpen = ""

// 		// start time
//   		show.movieStart = sh.date.slice(-7).trim();

// 		console.log(JSON.stringify(show))
// 		array.push(show)
// 	});
	
// 	fs.writeFile('./utils/scrapedWeb-JSON/scrape.json', JSON.stringify(array, null, 4), function(err){
// 		var msg = 'File successfully written! - Check your project directory for the output.json file'
// 	    console.log(msg);
// 		// Finally, we'll just send out a message to the browser reminding you that this app does not have a UI.
// 		res.send(msg);
// 	})	
// };

// exports.scrapeOpenAir = function(req, res){
// 	console.log("scrape...")
// 	var url = 'http://openaircinemas.com.au/perth/home';
// 	var venue = "OpeAir"
// 	//This is obtain from ajax request.

// 	var events = JSON.parse(fs.readFileSync('./utils/scrapedWeb-JSON/OpenAirJson.json', 'utf8'));
// 	var array=[];
// 	console.log(events.events.length)
// 	events.events.forEach(function(ev){
// 		if(!ev.Event.is_music){
// 			var title, date, rating, gateOpen, start, trailer;
// 			title = ev.Event.name;
// 			console.log (title)
// 			if (title.slice(0,20) == "Perth Now presents: "){
// 				title = title.slice(20)
// 			} else if (title.slice(0,16) == "Sunday Session: "){
// 				title = title.slice(16)
// 			} else if (title.slice(0,16) == "SBS 2 presents: "){
// 				title = title.slice(16)	
// 			} else if( title.slice(0,11) == "ANZAC Day: " ){
// 				title = title.slice(11)	
// 			} else if( title.slice(0,30) == "Closing Night Sunday Session: "){
// 				title = title.slice(30)	
// 			} else if( title.slice(0,17) == "April Fools' Day:"){
// 				title = title.slice(17)	

// 			} else if( title.slice(0,15) == "Yelp Presents: "){
// 				title = title.slice(15)	

// 			}
// 			console.log (title)

// 			date = ev.Event.start_time_formatted + " 2015"
// 			rating = ev.Event.rating
// 			start = ev.Event.start_time.slice(-8,-3);
// 			gateOpen = ev.Event.open_time.slice(-8,-3);
// 			trailer = ev.Event.trailer_youtube_id;
// 			array.push({ movie : title, startDay : date, endDay: date,  rating: rating, venueId: venue, gateOpen: gateOpen, movieStart: start, language: "", subtitle: "", trailer: trailer}); 
// 			console.log('===============================')
// 		}
// 	});

// 	fs.writeFile('./utils/scrapedWeb-JSON/scrape.json', JSON.stringify(array, null, 4), function(err){
// 		var msg = 'File successfully written! - Check your project directory for the output.json file'
// 	    console.log(msg);
// 		// Finally, we'll just send out a message to the browser reminding you that this app does not have a UI.
// 		res.send(msg);		
// 	})
// };

// exports.scrapeRoofTop_Single = function(req, res){
// 	console.log("scrape...")
// 	var url = 'https://www.rooftopmovies.com.au/program/event/8556a8d4-aac4-4a39-80e9-3a5d3dd29c6d';
// 	//var venue = "RooNor"
// 	var venueId = "554ed6bed617526c16816cf1"

// 	//var gateOpen = "6pm";

// 	request(url, function(error, response, html1){
// 		if(!error){
// 			var $ = cheerio.load(html1);
// 			var dat = $(this);
// 			var title, date;
// 			var rating = "";
// 			var regExp;
// 			var start,gateOpen,trailer;
// 			var array = []
// 			var k=1;

// 			console.log($('#resultsSS').length)
// 			fs.writeFile('./utils/tmp.json', JSON.stringify($.html(), null, 4), function(err){
// 	    		console.log('////');
// 			})
// 			//console.log("length  == " + dat.length)
// 			title = dat.children(':nth-child(2)').text().trim();
// 			//date = dat.children(".one_col.last_col:nth-child(2)").children(":nth-child(2)").content().text().trim()
// 			date = dat.find('div[id^="dates_"]').text().trim();
// 			//dates_0f144d6f-a09e-4571-a9b2-005bb46467a9
// 			gateOpen = dat.find('div[id^="doors_open"]').text().trim() || "6pm" ;
// 			start = dat.find('div[id^="time_"]').text().trim();
// 			trailer = dat.find(".yt").attr("href");

// 	       	// Doors open at xx PM. 
// 	    	//if(venue == "RooNor") {
// 	    		if (new Date(date) >= new Date("2015-03-19")) {
// 		    		gateOpen = "6:00 PM"
// 		    	} else{
// 		    		gateOpen = "6:00 PM"
// 		    	};
// 	    	//};

// 			array.push({ movie : title, startDay : date, endDay: date,  rating: rating, venueId: venueId, gateOpen: gateOpen, movieStart: start, language: "", subtitle: ""}); 
// 			console.log(title + " / " + rating + " / " + date );
// 			console.log('===============================');
// 			final(array,k)

// 		}
// 	});
		
// 	function final(array,k){
// 		if (true){//(array.length == k){
// 			fs.writeFile('./utils/scrapedWeb-JSON/scrapeRoofTop.json', JSON.stringify(array, null, 4), function(err){

// 			    console.log('File successfully written! - Check your project directory for the scrape.json file');
// 			})

// 			// Finally, we'll just send out a message to the browser reminding you that this app does not have a UI.
// 			res.send('Check your console!')
// 		}
// 	}
// };

module.exports = (function() {
  	'use strict';
  	var api = express.Router();
  
	api.get('/scrapeRoofTop', function(req, res) {
		console.log("scrape...")
		var url = 'https://www.rooftopmovies.com.au/program/';
		//var venue = "RooNor"
		var venueId = "554ed6bed617526c16816cf1"

		//var gateOpen = "6pm";

		request(url, function(error, response, html){
		if(!error){
		    var $ = cheerio.load(html);

		    
		    var json = { title : "", date : "", rating : ""};
		    var array = [];
		    var k = $('#content').children().length;
		    $('#content').children().each(function(){

		    	console.log("ok")
		    	var title, date,gateOpen;
		    	var rating = "";
		    	var regExp, start;
		    	var data = $(this);
		    	var url1;
		    	start = "8:00 PM";

				date = data.children('h3').contents('a').text().trim() + " 2015";
		    	date.trim();

		    	title = data.children('h4').contents('a').text().trim();
		    	title.trim();
					if (title.indexOf("(Encore Screening)") > -1) {
						title = title.slice(0,-18);
					};
		    	url1 = data.children('.event_image').attr('href');
				console.log(title + " / " + rating + " / " + date + " / " + url1 );


				// Doors open at xx PM. 
		    	//if(venue == "RooNor") {
		    		if (new Date(date) >= new Date("2015-03-19")) {
			    		gateOpen = "6:00 PM"
			    	} else{
			    		gateOpen = "6:00 PM"
			    	};
		    	//};

				array.push({ movie : title, startDay : date, endDay: date,  rating: rating, venueId: venueId, gateOpen: gateOpen, movieStart: start, language: "", subtitle: ""}); 
				console.log(title + " / " + rating + " / " + date );
				console.log('===============================');


				final(array,k);

		    });
		};


		function final(array,k){
			if (array.length == k){
				fs.writeFile('./utils/scrapedWeb-JSON/scrapeRoofTop.json', JSON.stringify(array, null, 4), function(err){

				    console.log('File successfully written! - Check your project directory for the scrape.json file');
				})

				// Finally, we'll just send out a message to the browser reminding you that this app does not have a UI.
				res.send('Check your console!')
			}
		}

		});
	})

	api.get('/somerville', function(req, res){
		var log=[];
		console.log('somerville');

		// read array

		var url = 'https://perthfestival.com.au/lotterywest-festival-films/whats-on/2016/a-perfect-day/';
		//var venue = "RooNor"
		//var venueId = ""

		//var gateOpen = "6pm";

		request(url, function(error, response, html){
			if(!error){
			    var $ = cheerio.load(html);

			    var json = { title : "", date : "", rating : ""};
			    var array = [];
			    var data = $(this)
			    //data.children('h3').contents('a').text().trim() 
			    var text = $.html('.sticky-footer__event')
			    res.send(text)
			}
		});
	})

	api.get('/luna', function(req, res){

		var url = 'http://outdoor.lunapalace.com.au/';
		var venue = "LunLee";
		var venueId = "554ed6bcd617526c16816ce9"

		var gateOpen = "7:00 PM";
		var toto = [];
		var allPromises = [];

		request(url, function(error, response, html){
		    if(!error){
		        var $ = cheerio.load(html);

			    
			    var json = { title : "", date : "", rating : ""};
			    var array = [];

			    $('a[href^="select-film"]:not(:has(>img)) span').each(function(){
			    	console.log("ok")
			    	var title, date;
			    	var rating = "";
			    	var regExp, start;
			    	var data = $(this);

			    	//toto.push(data)
			    	var href;
					href = data.parent().attr("href")
					console.log(href)

			    	var tmp = data.text().trim();
			    	console.log(tmp)

			    	var select;

			    	regExp = /([^(]+)\(/;
			    	select = regExp.exec(tmp) //find "(" and select what is before
			    	if (select) {title = select[1].trim()} else {title = tmp }

			    	//rating
			    	regExp = /\(([^)]+)\)/; // find "(" and selct what is after until ")"
					select = regExp.exec(tmp);
			    	if (select) {rating = select[1].trim()}

			    	tmp = data.parent().parent().prev().prev().contents().text().trim()
			    	console.log(tmp)

			    	//select date
			    	regExp = /([^,]+)\,/; 
					select = regExp.exec(tmp);
					if (select){date = select[1].trim() + " 2015"}
			    	
			    	 
			    	regExp = /\,([^,]+)/;
					select = regExp.exec(tmp);
			    	if (select){start = select[1].trim()}


		    		array.push({ 
			    		movieTitle : title, 
			    		startDay : date, 
			    		endDay: date,  
			    		parentalRating: rating, 
			    		venueId: venueId, 
			    		gateOpen: gateOpen, 
			    		movieStart: start, 
			    		language: "", 
			    		subtitle: "",
			    		link: "http://outdoor.lunapalace.com.au/" + href
			    		}
		    		); 

					console.log(title + " / " + rating + " / " + date);
					console.log('===============================');
			    })
			};

			//For each selection go to page and download trailer link:
			array.forEach(function(show){
				var promise = rp({uri: show.link , json:true}).then(function(html){
			    		var movie = cheerio.load(html)
			    		var trailer = movie('a.video').attr('href')
			    		console.log(trailer)
			    		show.trailer = trailer
			    		return show
			    	})
					.catch(function(err){
						console.log("error " + err)
					})

			    allPromises.push(promise)
			})
			
			fs.writeFile('./utils/logtest.json', JSON.stringify(array, null, 4), function(err){
			    console.log('File successfully written! - Check your project directory for the output.json file');
			})

			Promise.all(allPromises).then(function(result){
				fs.writeFile( './utils/scrapedWeb-JSON/scrapeLUNA.json', JSON.stringify(result, null, 4), function(err){
				    console.log('File successfully written! - Check your project directory for the output.json file');
				})

				// Finally, we'll just send out a message to the browser reminding you that this app does not have a UI.
				res.send('Check your console!')

			})

			// To write to the system we will use the built in 'fs' library.
			// In this example we will pass 3 parameters to the writeFile function
			// Parameter 1 :  output.json - this is what the created filename will be called
			// Parameter 2 :  JSON.stringify(json, null, 4) - the data to write, here we do an extra step by calling JSON.stringify to make our JSON easier to read
			// Parameter 3 :  callback function - a callback function to let us know the status of our function




		});
	});

	api.get('/came', function(req, res){
		console.log("came")
		var url = 'http://camelot.lunapalace.com.au/';
		var venue = "CamMos"
		var venueId = "554ed6bbd617526c16816ce7"

		var gateOpen =""
		var toto = [];
		var allPromises = [];

		request(url, function(error, response, html){
		    if(!error){
		        var $ = cheerio.load(html);

			    
			    var json = { title : "", date : "", rating : ""};
			    var array = [];

			    $('div.programme-tile').each(function(index){
	
			    	console.log("ok")
			    	var title, date;
			    	var rating = "";
			    	var regExp, start;
			    	var data = $(this);

			    	var href;
					href = data.children('a').attr("href")
					console.log(href)


					//TITLE & Rating
			    	var tmp, select;

			    	tmp  = data.find('div h2 a').text().trim();
			    	console.log(tmp)

			    	regExp = /([^(]+)\(/;
			    	select = regExp.exec(tmp) //find "(" and select what is before
			    	if (select) {title = select[1].trim()} else {title = tmp }

			    	////rating
			    	var rating;
			    	regExp = /\(([^)]+)\)/; // find "(" and selct what is after until ")"
					select = regExp.exec(tmp);
			    	if (select) {rating = select[1].trim()}


			    	//DATE
			    	tmp = data.find('span.date').text().trim()
			    	console.log(tmp)

			    	//select date
			    	//regExp = /([^,]+)\,/; 
					//select = regExp.exec(tmp);
					var year;
					if (index < 28) { year = " 2015"} else { year = " 2016"}
					date = tmp + year
			    	console.log(date + "WARNING YEAR might be INCORRECT!")
			    	 
			    	tmp = data.find('span.time').text().trim()
			    	//regExp = /\,([^,]+)/;
					//select = regExp.exec(tmp);
			    	start = tmp
			    	console.log(start)

					// Doors open at 6:15 PM. From 19 March doors open 6PM.
					if (new Date(date) >= new Date("2015-03-19")) {
			    		gateOpen = '6:15 PM'
			    	} else{
			    		gateOpen = "6:00 PM"
			    	};

			    	array.push({ 
			    		movieTitle : title, 
			    		startDay : date, 
			    		endDay: date,  
			    		parentalRating: rating, 
			    		venueId: venueId, 
			    		gateOpen: gateOpen, 
			    		movieStart: start, 
			    		language: "", 
			    		subtitle: "",
			    		link: "http://camelot.lunapalace.com.au/" + href
			    	}); 

					console.log(title + " / " + rating + " / " + date + " " + start);
					console.log('===============================')
			    })
			};

			//For each selection go to page and download trailer link:
			array.forEach(function(show){
				console.log(show.movieTitle)
				var promise = rp({uri: show.link , json:true}).then(function(html){
			    		var movie = cheerio.load(html)
			    		var trailer = movie('a.video').attr('href')
			    		console.log(trailer)
			    		show.trailer = trailer
			    		return show
			    	})
					.catch(function(err){
						console.log("error " + err)
					})

			    allPromises.push(promise)
			})
			
			fs.writeFile('./utils/logtest.json', JSON.stringify(array, null, 4), function(err){
			    console.log('File successfully written! - Check your project directory for the output.json file');
			})

			Promise.all(allPromises).then(function(result){
				fs.writeFile( './utils/scrapedWeb-JSON/scrapeCAME.json', JSON.stringify(result, null, 4), function(err){
				    console.log('File successfully written! - Check your project directory for the output.json file');
				})

				// Finally, we'll just send out a message to the browser reminding you that this app does not have a UI.
				res.send('Check your console!')

			})
			.catch(function(err){
				console.log(err)
			})
		});
	});


  return api;
})();