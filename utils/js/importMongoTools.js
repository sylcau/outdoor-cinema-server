var express = require('express')
  , connect = require('connect')
  , request = require("request")
  , async = require('async')
  , bcrypt = require('bcryptjs')
  , logger = require('morgan')
  , bodyParser = require('body-parser')
  , mongoose = require('mongoose')
  ;  

var fs = require('fs');
var jwt = require('jwt-simple');
var moment = require('moment');
//var path = require('path');

//** IMPORT CONSTANT 
var config = require('../../config');

// Import MODEL
var User = require('../../model/User');
var model = require('../../model/ShowMoviesVenues');

var Venue = model.Venue;
var Movie = model.Movie;
var Show = model.Show;

module.exports = (function() {
  'use strict';
  var api = express.Router();

  // Check that all movies are present
  function verifyMovies (show, log){ 
    
  };

  function testShowDuplicate(show, log){

  }; 

  function maxMovieRef(movieArray){ //mvRef: "OC2001" --> return 2001
      movieArray.sort(function(a, b){return parseInt(b.movieRef.slice(-4)) - parseInt(a.movieRef.slice(-4))});
      return parseInt(movieArray[0].movieRef.slice(-4)) //first value as a number 
  };

  function maxShowRef(showArray){ //idRef: "101" --> return 101
      showArray.sort(function(a, b){ return parseInt(b.idRef) - parseInt(a.idRef) } );
      return parseInt(showArray[0].idRef) //first value as a number 
  };

  api.get('/checkShows', function (req, res) {
    console.log(" ==== checkShows ==== ")
    var jsonFile = req.query.file
    if (typeof jsonFile == "undefined") {
      return res.send("missing param file = ....json")

    }
    var addFile = false;
    if (req.query.add) { addFile = true };

    //check if Movie.OMDB // Movie identify in the DB
    // check if Movie.imdbId // movie identified manually.
    // If Not, 
    //   check for title in mongoose
    //   if found => get movieId and add to Show
    // else check for title in MovieDB
    //   if found => get movie details and add to Show.movieIMDB + write in logFile
    //   else write in Log file Not found.

    var shows2Upload = []
    var movies2Upload = []
    var log = [];
    var logExtraDetails = [];

    function daydiff(first, second) {
      return Math.round((second-first)/(1000*60*60*24));
    }

    //read ShowsJSON after a scrape, copy manually data in this file.
    var jsonFileFull = "./utils/scrapedWeb-JSON/" + jsonFile + ".json"
    console.log("reading " + jsonFileFull)
    var ShowsJson = JSON.parse(fs.readFileSync(jsonFileFull, 'utf8'));

    if (addFile) {
      var addFileFull = "./utils/scrapedWeb-JSON/" + jsonFile + "_extra.json"
      var extraDetails = JSON.parse(fs.readFileSync(addFileFull, 'utf8'));
      console.log("reading " + addFileFull + " | length: " + extraDetails.length)

    }

    //check if MovieId //Movie already in database
    async.eachSeries(ShowsJson, function(show, callback){
      console.log("==== ")
      console.log("movie: " + show.movieTitle)

      //test case for fucking -- F**king - replace * with --
      show.movieTitle = show.movieTitle.replace(/\*/gi, '-')

      //search extra detaisl for more infos:
      if(addFile) {
        var index = extraDetails.map(function(e) { return e.movieTitle; }).indexOf(show.movieTitle)
        if ( index > -1 ) {
          show.movieId = extraDetails[index].movieId;
          show.movieIMDB = extraDetails[index].movieIMDB;
          console.log("added details : " + show.movieId + " | " + show.movieIMDB)
        };
      } 
      console.log("movie: " + show.movieTitle)

      //check if show is already in Database:
      var showObjTmp = new Show(show)
      // Test if Duplicate SHows in dates. Assume 1 movie per venue.
      showObjTmp.startDay.setHours(0)
      showObjTmp.endDay = new Date(show.startDay)
      showObjTmp.endDay.setHours(23)
      console.log("startDay: " + new Date(show.startDay))
      console.log("startDay: " + showObjTmp.startDay)
      console.log("endDay: " + showObjTmp.endDay)

      Show.find({startDay: {"$gte": showObjTmp.startDay , "$lt": showObjTmp.endDay}, venueId: showObjTmp.venueId})
      .exec(function(err, res){
        if (err) {
          var msg = "error = " + err
          log.push(msg)
          console.log(msg)
          return callback()

        } else if (res.length == 0) {
          console.log("ok")
        } else {
          var msg = "Duplicate Show: " + show.movieTitle + " - " + show.startDay
          show.duplicate = true
          log.push(msg)
          console.log(msg)
          
        }
      }).then(function(){
              console.log("movie: then " )

        if (show.duplicate) { 
          console.log("duplicate")
          return callback() 
        }
        if (!show.movieId){
          if (typeof show.movieDirector =="undefined") { show.movieDirector = "-" }
          if (typeof show.parentalRating =="undefined") { show.parentalRating = "" }
          if (typeof show.endDay =="undefined") { show.endDay = show.startDay }

          var criteria1 = new RegExp("^"+ show.movieTitle +"$","i")
          var criteria2 = "cantmacthAnything"; 
          if (typeof show.movieIMDB != "undefined") {criteria2 = show.movieIMDB };
          console.log(criteria1 + "  " +  criteria2)
          Movie.find( {"$or": [ {Title: criteria1}, { movieTitle: criteria1}, {imdbID: criteria2} ] } )
            .exec(function(err, mov){
              if (err){ 
                console.log(" -error with " + show.movieTitle + " => " + err)
                log.push(" -error with " + show.movieTitle + " => " + err)
                logExtraDetails.push({movieTitle: show.movieTitle , movieIMDB: "", movieId: ""})              

                return callback();
              }
              if (mov.length == 1) { //find only one = good.!!
                show.movieId = mov[0]._id 
                
                console.log(" -found: "+ mov[0].movieTitle + "  - " +  show.movieId)

                var first = new Date(show.startDay)
                var second = new Date(show.endDay)

                //Multiple Shows if start date diff de end date
                var diff = daydiff(first, second)
                console.log(diff + " | " + first + " | "+ second)
                for (var i = 0; i <= diff; i++) {
                  //var tmp = JSON.parse(JSON.stringify(show));
                  var newfirst = new Date(first.toGMTString())
                  //console.log("===*** "+ newfirst + "  " + first.getDate() + " " + i)
                  newfirst.setDate(first.getDate() + i)
                  newfirst.setHours(19)
                  //console.log("===*** "+ newfirst + "  " + first.getDate() + " " + i)
                  show.startDay = newfirst.toString()
                  show.endDay = show.startDay
                  console.log("==> "+ show.startDay + " / "+ show.endDay)
                  shows2Upload.push(JSON.parse(JSON.stringify(show)))
                }


                return callback();

              } else if (mov.length > 1) { //find 2 possible issue
                console.log(" -Find Duplicate match for: " + show.movieTitle + " -- movie Director: " + show.movieDirector) 
                for (var i=0; i < mov.length; i++) {
                  if (i == 0) (log.push( { "$oid": mov[i]._id }))
                  console.log(" --" + mov.length +  " | "+ i + " " + mov[i].movieTitle + " | " + mov[i]._id)
                  if (mov[i].Director == show.movieDirector){
                    console.log("identify with Director: " + mov[i]._id )
                  }

                };
                return callback();

              } else { //didn't find any.              
                if (typeof show.movieIMDB != "undefined") { // manually identified.
                  var url1 = 'http://www.omdbapi.com/?i=' + show.movieIMDB +'&y=&plot=full&r=json'
                } else {
                  var url1 = 'http://www.omdbapi.com/?t=' + encodeURIComponent(show.movieTitle)+'&y=&plot=full&r=json'
                }
                request(url1 , function (error, response, json) {
                  console.log('start request OMDBAPI: ' + url1);
                  if (!error){
                        var obj = JSON.parse(json)
                        if (obj.Error){
                          var msg = 'OMDB: error movie getting info for: '+ show.movieTitle
                          console.log(msg)
                          log.push(msg)
                          logExtraDetails.push({movieTitle: show.movieTitle , movieIMDB: "", movieId: ""})
                          callback();

                        } else {
                          if ( typeof show.movieIMDB == "undefined" && (obj.Director.toLowerCase() != show.movieDirector.toLowerCase()) && (show.movieDirector.toLowerCase() !="-")) {
                            var msg = "Warning Director don't match: " + obj.Director.toLowerCase()  + " | It should be: " + show.movieDirector.toLowerCase() + " | movie: " + show.movieTitle
                            console.log(msg)
                            log.push(msg)
                            logExtraDetails.push({movieTitle: show.movieTitle , movieIMDB: "", movieId: ""})

                            if ( obj.Title.toLowerCase() != show.movieTitle.toLowerCase()) {
                              var msg = "title don't match " + obj.Title + ":  ||| " + show.movieTitle +":"
                              console.log(msg)
                              log.push(msg)
                              logExtraDetails.push({movieTitle: show.movieTitle , movieIMDB: "", movieId: ""})

                              //return callback()
                            }
                            return callback();
                          }
                          obj.movieTitle = show.movieTitle;
                          obj.parentalRating = show.parentalRating;
                          obj.trailer = show.trailer;
                          movies2Upload.push(obj)
                          console.log("OMDB done")
                          callback();
                        }
                  } else {
                    console.log('OMDB: error movie getting info for: '+ show.movieTitle)
                    logExtraDetails.push({movieTitle: show.movieTitle , movieIMDB: "", movieId: ""})
                    callback();
                  }
                });
              }
            })
        } else {
          // create show

          console.log(" -found: "+ show.movieId)

          var first = new Date(show.startDay)
          var second = new Date(show.endDay)

          //Multiple Shows if start date diff de end date
          var diff = daydiff(first, second)
          console.log(diff + " | " + first + " | "+ second)
          for (var i = 0; i <= diff; i++) {
            //var tmp = JSON.parse(JSON.stringify(show));
            var newfirst = new Date(first.toGMTString())
            //console.log("===*** "+ newfirst + "  " + first.getDate() + " " + i)
            newfirst.setDate(first.getDate() + i)
            newfirst.setHours(19)
            //console.log("===*** "+ newfirst + "  " + first.getDate() + " " + i)
            show.startDay = newfirst.toString()
            show.endDay = show.startDay
            console.log("==> "+ show.startDay + " / "+ show.endDay)
            shows2Upload.push(JSON.parse(JSON.stringify(show)))
          }
          callback();
        }
      })

    }, function(){

      console.log( " - " )
      var msg = [];
      msg.push('Error recorded (log.json): ' + log.length);
      msg.push('movies to Upload (movies2Upload.json): ' + movies2Upload.length)
      msg.push('shows2Upload.json: ' + shows2Upload.length)
      msg.push ("shows scraped: "+ ShowsJson.length)

      fs.writeFile('./utils/log.json', JSON.stringify(log, null, 4), function(err){
        console.log('Error recorded (log.json): ' + log.length);
      });
      fs.writeFile('./utils/scrapedWeb-json/logExtraDetails.json', JSON.stringify(logExtraDetails, null, 4), function(err){
        console.log('created logExtraDetails.json ' + logExtraDetails.length);
      });

      fs.writeFile('./utils/movies2Upload.json', JSON.stringify(movies2Upload, null, 4), function(err){ 
        console.log('movies to Upload (movies2Upload.json): ' + movies2Upload.length);
      });

      // write updated file.
      fs.writeFile('./utils/shows2Upload.json', JSON.stringify(shows2Upload, null, 4), function(err){
        console.log('shows2Upload.json: ' + shows2Upload.length );
      });
      console.log("shows scraped: "+ ShowsJson.length )
      // Finally, we'll just send out a message to the browser reminding you that this app does not have a UI.
      res.send(msg)

    })
  });

  /////////////////////////////////////////////////////////////////////////////////
  // UPLOAD
  api.get('/uploadShows', function (req, res) {
    // read JSON of SHOWS to Import
    var shows2Upload = JSON.parse(fs.readFileSync('./utils/shows2Upload.json', 'utf8'));
    //var shows2Upload = JSON.parse(fs.readFileSync('./utils/scrapedWeb-JSON/scrapeLotterywest_Shows2Upload_add.json', 'utf8'));
   
    console.log("----------" + shows2Upload.length)


    // CHECK that movies and venues are defined correctly.
    var Shows = [];
    var log = [];
    //var  = 0;
    var maxIdRef;
    Show.find({}).select("idRef").exec()
      .then(function(data){
        console.log(data)
        maxIdRef = maxShowRef(data)
        console.log(maxIdRef)
        return (maxIdRef)
      })
      .then(function(maxIdRef){
        console.log("maxIdRef: " + maxIdRef)
        async.eachSeries(shows2Upload, function(show, callback){

          // Check if all details are there.
          if (typeof show.movieId == "undefined" && show.movieId == "") {
            var msg = "error movie id missing in show " + show.idRef
            console.log(msg)
            log.push(msg)
            return callback()

          }
          if (typeof show.venueId == "undefined" && show.venueId == "") {
            var msg = "error venue id missing in show " + show.idRef
            console.log(msg)
            log.push(msg)
            return callback()
          }

          var showObj = new Show(show)
          // Test if Duplicate SHows. Assume 1 movie per venue.
          Show.find( {"$or": [ {startDay: showObj.startDay, venueId: showObj.venueId}, {idRef: showObj.idRef} ] })
          .exec(function(err, res){
            if (err) {
              var msg = "error = " + err
              log.push(msg)
              console.log(msg)
              callback()

            } else if (res.length == 0) {
              //Shows.push(show)
              
              maxIdRef = maxIdRef + 1
              showObj.idRef = maxIdRef;
              console.log("no duplicate - show added: " + show.startDay + " :" + show.movieTitle + " : " +showObj.idRef )

              showObj.save(function (err, sh) {
                if (err) {return console.error(err)};
                console.log('==>> Show saved: ' + sh.startDay + " : " + sh.movieId)
                callback();
              });

              //callback()

            } else {
              var msg = "Duplicate error: " + show.movieId + " - " + show.startDay
              log.push(msg)
              console.log(msg)
              callback()

            }
          })
        }, function (){
          
          if (log.length !== 0){
            fs.writeFile('./utils/uploadShowsError.json', JSON.stringify(log, null, 4), function(err){
              console.log('File successfully written! - Check your project directory for the output.json file');
            });
            return res.send("some Issues")
          }

          console.log("SHows uploaded - job done!!")
          return res.send("job done!!")
        });         
      });
  });

  api.get('/uploadNewMovies', function (req, res) {
    // read JSON - create table
    var movieFile = JSON.parse(fs.readFileSync('./utils/movies2Upload.json', 'utf8'));
    console.log("----------" + movieFile.length)
    var Movies = [];
    var log = [];

    //for Each > check if duplicate // then save 
    var nbMovie;

    Movie.find({})
    .select("movieRef")
    .exec(function (err, movies){
      if (err) {
        //console.log('there are %d jungle adventures', count);
        return res.send("error")
      } else {
        nbMovie = maxMovieRef(movies);
        console.log("nb movie: " + movies.length + " " + maxMovieRef(movies))
      }
    })
    .then(function(){
      async.eachSeries(movieFile, function(movie, callback){
        console.log("  ")
        console.log(" *************** ")

        //check if duplicate!!!!
        var arr = [];
        if (typeof movie.imdbID != "undefined") { arr.push({imdbID: movie.imdbID}) }
        if ((typeof movie.Title != "undefined") && (typeof movie.Year != "undefined"))  { arr.push({'Title': movie.Title, 'Year': movie.Year }) }

        if (arr.length == 0){
          console.log("issue: movie with not enough infos.")
          return false
        };
        
        Movie.find({"$or": arr})
        .exec(function (err, mv){
          if (err) {
            var mess = "Duplicate error: " + err
            log.push(mess)
            console.log(mess)
            callback();

          } else if (mv.length == 0) {
            //search duplicate in MoviesDb
            var mess = "No Duplicate for: " + JSON.stringify(arr)
            log.push(mess)
            console.log(mess)
            //Movies.push(movie);

            //
            var movieObj = new Movie(movie)
            nbMovie = nbMovie + 1
            if (typeof movieObj.movieTitle == "undefined"){ movieObj.movieTitle = movieObj.Title };
            if (typeof movieObj.movieRef == "undefined"){ movieObj.movieRef = "OC" + nbMovie };
            
            console.log('movie Model: ' + movieObj.movieRef + "  = " + movieObj.movieTitle + "  = " + movieObj.Title )


            movieObj.save(function (err, movieDoc) {
              if (err) return console.error(err);
              console.log('==>movie saved: ' + movieDoc.movieRef + "  = " + movieDoc.movieTitle)
              callback();
            })
            //callback()

          } else {
            var mess = "find duplicate for " + mv[0].Title
            log.push(mess)
            console.log(mess)
            callback();

          }
        });

      }, function(){
        //console.log("duplicate check done: " + Movies.length)
        console.log("job done!!")
        res.send("job done!!")
        
        //save each raw. 
        // get total number of Movies: 
        // Movie.find({})
        //   .select("movieRef")
        //   .exec(function (err, movies){
        //   if (err) {
        //     //console.log('there are %d jungle adventures', count);
        //     return res.send("error")
        //   } else {
        //     var nbMovie = maxMovieRef(movies);
        //     console.log("nb movie: " + movies.length + " " + maxMovieRef(movies))

        //     async.eachSeries(Movies, 
        //       function(val, callback){
                
        //         });
        //       }, function(){
                
        //     });
        //   };
        // });
      });
    });
  });
  ////////////////////////////////////////////////////////////////////////////////

  // fiddle
  api.get('/toto', function (req, res){
    console.log("searching!!")
    var ShowsJson = JSON.parse(fs.readFileSync('./utils/scrapedWeb-JSON/scrapeLotterywest_Shows2Upload.json', 'utf8'));
    var log = []
    //check if MovieId //Movie already in database
    async.eachSeries(ShowsJson, function(show, callback){
      var date = new Date (show.startDay)
      var newDate = new Date (show.startDay)
      if (date.getDay() == 6){
        newDate.setDate(newDate.getDate() + 1)
        show.startDay = newDate.toGMTString()
        show.endDay = show.startDay
        console.log('==> '+ show.startDay)
        log.push(show)
      }
      callback()
    }, function(){
      fs.writeFile('./utils/scrapedWeb-JSON/scrapeLotterywest_Shows2Upload_add.json', JSON.stringify(log, null, 4), function(err){
          console.log('File successfully written! - Check your project directory for the log.json file');
      });
      res.send("ok")
    })
  });

  // NOT USED ////////////////////////////////////////////////////////////////
  api.get('/uploadVenues-Done', function (req, res) {
    // read JSON - create table
    var V = JSON.parse(fs.readFileSync('./utils/Venues.json', 'utf8'));
    console.log("----------" + V.total_rows)
    var Venues = [];
    V.rows.forEach(function(val){
      delete val.value._id;
      delete val.value.rev;

      Venues.push(val.value)
    })

    // save each raw. 
    async.eachSeries(Venues, function(val,callback){
      var venue = new Venue(val)
      venue.save(function (err, venue) {
        if (err) return console.error(err);
        console.log('venue saved: ' + venue.cinema)
        callback();
      });
    }, function(){
      console.log("job done!!")
      res.send("job done!!")
    });
  });


  return api;
})();