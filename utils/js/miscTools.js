var express = require('express')
  , connect = require('connect')
  , request = require("request")
  , async = require('async')
  , bcrypt = require('bcryptjs')
  , logger = require('morgan')
  , bodyParser = require('body-parser')
  , mongoose = require('mongoose')
  ;  

var fs = require('fs');
var jwt = require('jwt-simple');
var moment = require('moment');
//var path = require('path');

var YouTube = require('youtube-node');
var youTube = new YouTube();
youTube.setKey('AIzaSyDOiSDN15L8bKSpl3pqBFgtVcYyvwiDPdI');

var rp = require('request-promise');

//** IMPORT CONSTANT 
var config = require('../../config');

// Import MODEL
var User = require('../../model/User');
var model = require('../../model/ShowMoviesVenues');

var Venue = model.Venue;
var Movie = model.Movie;
var Show = model.Show;

function findTrailer(url, doc) {
  rp(url)
    .then(function(data){
      if (typeof data.results == "undefined") { return {status: "error"} } 
      var trailer = data.results[0].key
      var title = data.results[0].name
      console.log(trailer + " " + title + " " + id)
      return {trailer: trailer, title: title, mov: doc}
    })
    .catch(handleError)
};

function handleError(err){
  console.log(err)
  res.send("err")
}

module.exports = (function() {
  'use strict';
  var api = express.Router();
  //route: /misc/ downloadImg 

  //search for missing poster, download them, add them.
  api.get('/downloadImg', function (req,res){
    console.log("searching!!")


    //Download Images: (Need to have the Poster field filled. from OMDB)
    var download = function(uri, filename, title, callback){
      if (typeof uri == "undefined" || uri == "N/A") {
        console.log("error: "+ title)
        //uri = "./public/img/movies/" + mov.movieRef + ".jpg" 
        return callback
      }
      request.head(uri, function(err, res, body){
        if (err){ 
          console.log("error Dw: " + err)
          return callback
        }
      //console.log('content-type:', res.headers['content-type']);
      //console.log('content-length:', res.headers['content-length']);

        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
      });
    };
    function final(array, k){
      //var completed = array.filter(function(e) { return e.imdbID != undefined; })
      console.log(k + "|" + array.length)
      if ( k == array.length) {
        res.send(JSON.stringify(k))
      }
    };

    //get list of movies:
    Movie.find({}, function(err, movies) {
      if (!err) {
        console.log("got movies")

        var k = 0;
        movies.forEach(function(mov){
          //var dest = "./public/img/movies/" + mov.movieRef + ".jpg"
          var dest = "../OutdoorCine/app/img/movies/" + mov.movieRef + ".jpg"
          //console.log(mov.Poster + "  |  " + dest)
          if (typeof mov.Poster != "undefined" && mov.Poster != "N/A" && mov.Poster != "undefined"){
            // check if file already exist:
            fs.exists(dest, function(exists) {
              if (exists) {
                k=k+1
                //console.log(mov.Poster + " : already exist")
                mov.img = "./img/movies/" + mov.movieRef + ".jpg"
                mov.save(function(){
                  final(movies, k);
                })
              } else {
                // download new file
                download(mov.Poster, dest, mov.movieTitle,function(){
                  console.log('done: ' + mov.movieRef);
                  k=k+1;
                  mov.img = "./img/movies/" + mov.movieRef + ".jpg"
                  mov.save(function(){
                    final(movies, k);
                  })
                });   
              };

            });
          } else {
            console.log('No URI Infos for: ' + mov.movieTitle)
            k = k + 1;
            final(movies, k);
          }
        })
        
      } else {
        res.send('error with db.get : ' + err)
      }
    });
  });
  
  api.get('/toto', function (req,res){
    console.log('toto')
    Movie.find({img: "./img/movies/undefined.jpg"}, function(err, movie){
      console.log(movie.length)
      async.eachSeries(movie, function(mov, callback){
        console.log(mov.movieRef)
        mov.img = "./img/movies/"+ mov.movieRef +".jpg"
        mov.save(callback) 

      }, function(){
        console.log("all movie saved")
        res.send("done")
      })

    })
  });


  api.get('/findTrailer-youtube', function(req, res){
    // find movie with no trailer and search Youtube and update movies.es
    var log=[];
    console.log('findTrailer');
    Movie.find({trailer: null}, function(err, movies){
      console.log("movies with no trailer: "+ movies.length)
      async.eachSeries(movies, function(movie,callback){

        youTube.search("trailer "+ movie.Title + " " + movie.Year + " " + movie.Director, 2, function(error, result) {
        //youTube.search(movie.Title + " Perth International Arts Festival",2,function(error,result){
          if (error) {
            console.log(error);
            callback();
          }
          else {
            var msg = result.items[0].id.videoId + " = " + result.items[0].snippet.title + " = " + movie.movieTitle + " = " + movie.Director + " = " + movie._id + "|" + result.items[0].snippet.description 
            //var msg = result
            console.log(msg);
            log.push(msg)
            movie.trailer = result.items[0].id.videoId
            movie.save(function(err){
              if (err) console.log("error save " + movie._id)
                callback()
            })
            //callback()
          }
        });
      }, function(){
          fs.writeFile('./utils/log.json', JSON.stringify(log, null, 4), function(err){
            console.log('File successfully written! - Check your project directory for the log.json file');
          });
          console.log("done")
          res.send("done")
      });
    })
  });

  api.get('/findTrailerTmdb', function(req, res){
    // find movie with no trailer and search tmdb and update movies.es
    console.log('findTrailerTmdb');

    var log=[];
    var allPromises = [];
    
    Movie.find({trailer: null}).exec()
      .then(function(movies){
        console.error("movies with no trailer: "+ movies.length)
        movies.forEach(function(mov){
          var url = "http://api.themoviedb.org/3/movie/" + mov.imdbID + "/videos?external_source=imdb_id&api_key=ba604f19ae9c593275b33b242ac4bb10"
          //console.log(url)
          var pr = rp(url)
                    .then(function(data){
                      console.log("=> " + url)
                      // if (typeof data.results == "undefined") { 
                      //   console.log(JSON.stringify(data))
                      //   return {status: "error"} 
                      // }
                      var dt = JSON.parse(data)
                      if (dt.results.length == 0) {
                        console.log("no trailer for: " + mov.imdbID  + " | " + mov.Title)
                        return {trailer: null, title: null, doc: mov} 
                      } 
                      var trailer = dt.results[0].key
                      var title = dt.results[0].name
                      
                      mov.trailer = dt.results[0].key
                      mov.save(function(){
                        console.log(trailer + " | " + title + " | " + mov._id + " | " + mov.imdbID)
                        console.log("^^^^^^^^^")
                        return {trailer: trailer, title: title, doc: mov}
                      })
                    })
                    .catch(function(err){
                      console.error(err)
                      console.log("^^^^^^^^^")
                      return {trailer: null, title: null, doc: mov}
                    })

            allPromises.push(pr) 
        })
      })
      .then(function(){

        Promise.all(allPromises).then(function(result){

          fs.writeFile('./utils/log.json', JSON.stringify(result, null, 4), function(err){
            console.log('File successfully written! - Check your project directory for the log.json file');
          });
          console.log("== All done")
          res.send("done")
        });
    })
  });


  //update doc with array of _id 
  api.get('/updateMisc', function(req,res){
    console.log("updateMisc")
    //var call = new Promises();
    var array = require('./sourceArray'); // array of object {id:"xx", parentalRating: "PG"}

    var allPromises = []
      
    array.changes.forEach(function(val){
      var promise = Movie.findByIdAndUpdate(val._id, {parentalRating: val.parentalRating } ).exec().then(function(doc){
        return console.log("doc saved: " + doc._id)
      })
      allPromises.push(promise)
    });
      
    Promise.all(allPromises).then(function(){
      console.log("== All done")
      res.send('ok')
    })
    .catch(function(err){
      console.log("err " + err)
      res.send(err)
    })
  });

  api.get('/correctdatesss', function(req,res){
    console.log("updateMisc")
    //var call = new Promises();
    //var array = require('./sourceArray'); // array of object {id:"xx", parentalRating: "PG"}

    var allPromises = []
    
    var arr = [];
    for (var i = 1283 ; i<= 1328; i++) {
      arr.push(i)
    }

    Show.find({"idRef" : {"$in":arr} }).exec().then(function(results){

      results.forEach(function(show){

        var date = new Date(show.startDay)
        date.setYear(2016)
        //console.log(date)

        show.startDay = date
        show.endDay = date
        console.log(show.startDay)

        var pr = show.save(function(){
          return console.log("==>  " + show.idRef + " " + show.startDay)
        })
        allPromises.push(pr)
      });
        
      Promise.all(allPromises).then(function(){
        console.log("== All done")
        res.send('ok')
      })
      .catch(function(err){
        console.log("err " + err)
        res.send(err)
      })



    })  
  });
  return api;
})();